#!/usr/bin/env bash
set -euo pipefail

# ========================================
# This script produces a single executable binary for each supported architecture.
# The binaries are produced by injecting a bundle produced by esbuild into a node.js binary
# as the SEA blob. For more details see the link below
# https://nodejs.org/docs/latest-v20.x/api/single-executable-applications.html
# ========================================

# ========================================
# Configuration
# ========================================
BIN_DIR="./bin"
TEMP_DIR="./tmp"
APP_NAME="gitlab-lsp"
SEA_CONFIG="sea-config.json"
SEA_BLOB="${BIN_DIR}/sea-prep.blob"
SEA_FUSE="NODE_SEA_FUSE_fce680ab2cc467b6e072b8b5df1996b2"
MACHO_SEGMENT="NODE_SEA"
NODE_VERSION="v20.18.1"
SUPPORTED_ARCHS="linux-x64 win-x64 darwin-x64 darwin-arm64"
OUT_DIR="./out"

# ========================================
# Helper Functions
# ========================================
log_step() {
    echo "===> $1"
}

ensure_directory() {
    if [[ ! -d "$1" ]]; then
        log_step "Creating directory: $1"
        mkdir -p "$1"
    fi
}

clean_directory() {
    local dir="$1"
    log_step "Cleaning directory: $dir"
    rm -rf "${dir}"/*
}

fetch_and_extract_node() {
    local arch="$1"
    local output_path="$2"
    log_step "Fetching Node.js ${NODE_VERSION} for ${arch}"

    # Ensure temp directories exist
    ensure_directory "$TEMP_DIR"
    ensure_directory "$NODE_EXTRACT_DIR"

    # Set URL and archive paths based on architecture
    local archive_extension="tar.gz"
    local node_exe_path="bin/node"
    local is_windows=false

    if [[ "$arch" == "win-x64" ]]; then
        # Prefer .zip over .7z for broader compatibility
        archive_extension="zip"
        node_exe_path="node.exe"
        is_windows=true
    fi

    local NODE_URL="https://nodejs.org/download/release/${NODE_VERSION}/node-${NODE_VERSION}-${arch}.${archive_extension}"
    local NODE_ARCHIVE="${TEMP_DIR}/node-${arch}.${archive_extension}"

    # Download Node.js archive
    log_step "Downloading Node.js from ${NODE_URL}"
    if ! curl -fsSL "$NODE_URL" -o "$NODE_ARCHIVE"; then
        echo "Error: Failed to download Node.js from ${NODE_URL}" >&2
        exit 1
    fi

    # Extract the archive based on type
    log_step "Extracting Node.js archive"
    if [[ "$is_windows" == true ]]; then
        # For zip files
        if command -v unzip &> /dev/null; then
            unzip -q "$NODE_ARCHIVE" -d "$NODE_EXTRACT_DIR"
        else
            echo "Error: 'unzip' command not found. Please install it to extract Windows Node.js packages." >&2
            exit 1
        fi

        # Windows zip structure includes a root directory with version in the name
        local root_dir=$(find "$NODE_EXTRACT_DIR" -mindepth 1 -maxdepth 1 -type d | head -n 1)

        # Copy node.exe to target location
        log_step "Copying Node.js executable to ${output_path}"
        cp "${root_dir}/${node_exe_path}" "$output_path"
    else
        # For tar.gz files
        if ! tar -xzf "$NODE_ARCHIVE" -C "$NODE_EXTRACT_DIR" --strip-components=1; then
            echo "Error: Failed to extract Node.js tarball" >&2
            exit 1
        fi

        # Copy node binary to target location
        log_step "Copying Node.js binary to ${output_path}"
        cp "${NODE_EXTRACT_DIR}/${node_exe_path}" "$output_path"
    fi

    if [[ ! -f "$output_path" ]]; then
        echo "Error: Failed to copy Node.js binary to $output_path" >&2
        exit 1
    fi

    # Make the binary executable (not needed for Windows .exe files, but doesn't hurt)
    chmod +x "$output_path"

    # Only attempt to remove signature for macOS binaries
    if [[ "$arch" == darwin-* && -x "$(command -v codesign)" ]]; then
        log_step "Removing signature from macOS binary"
        codesign --remove-signature "${output_path}" || true
    fi

    log_step "Node.js binary successfully prepared at $output_path"

    # Cleanup temporary files
    log_step "Cleaning up temporary files"
    rm -rf "$NODE_ARCHIVE" "$NODE_EXTRACT_DIR"/*
}

copy_assets() {
    log_step "Copying shared assets"

    # Copy tree-sitter.wasm files
    log_step "Copying tree-sitter WASM file"
    cp "${OUT_DIR}/node/tree-sitter.wasm" "${BIN_DIR}/"

    # Copy tree-sitter WASM grammar files
    log_step "Copying tree-sitter grammar WASM files"
    ensure_directory "${BIN_DIR}/vendor/grammars"
    cp ${OUT_DIR}/vendor/grammars/*.wasm "${BIN_DIR}/vendor/grammars/"

    # Copy webview assets
    log_step "Copying webviews assets"
    ensure_directory "${BIN_DIR}/webviews"
    cp -r ${OUT_DIR}/webviews/* "${BIN_DIR}/webviews/"

    log_step "Assets copied successfully"
}

generate_sea_blob() {
    # Generate SEA blob (only needs to be done once)
    log_step "Generating SEA blob"
    if [[ ! -f "$SEA_CONFIG" ]]; then
        echo "Error: SEA config file not found: $SEA_CONFIG" >&2
        exit 1
    fi
    node --experimental-sea-config "$SEA_CONFIG"
}

build_for_arch() {
    local arch="$1"
    log_step "Building for architecture: ${arch}"

    # Set executable extension based on target platform
    local exe_ext=""
    if [[ "$arch" == "win-x64" ]]; then
        exe_ext=".exe"
    fi

    # Set output binary path with appropriate naming
    local output_arch="$arch"
    # Replace "darwin" with "macos" in the output filename for backward compatibility
    if [[ "$arch" == darwin-* ]]; then
        output_arch="macos-${arch#darwin-}"
    fi

    local output_binary="${BIN_DIR}/${APP_NAME}-${output_arch}${exe_ext}"

    # Fetch and extract Node.js
    fetch_and_extract_node "$arch" "$output_binary"

    # Bundle SEA
    log_step "Bundling SEA into executable for ${arch}"
    if [[ ! -f "$SEA_BLOB" ]]; then
        echo "Error: SEA blob not found: $SEA_BLOB" >&2
        exit 1
    fi

    # --macho-segment-name argument is required of building darwin binaries
    # it is ignored for other platforms, so we can always provide it.
    # see https://github.com/nodejs/postject/blob/3c4f2080ee56025716c3add0f6c03b16e2af54ff/src/api.js#L44
    npx postject "$output_binary" "NODE_SEA_BLOB" "$SEA_BLOB" \
        --sentinel-fuse "$SEA_FUSE" \
        --macho-segment-name "$MACHO_SEGMENT"

    log_step "SEA bundle created successfully at $output_binary"
}

clean_up() {
    log_step "Cleaning up node SEA files"
    rm $SEA_BLOB
    log_step "Clean up completed"
}

# ========================================
# Main Script
# ========================================
main() {
    # Ensure bin directory exists
    ensure_directory "$BIN_DIR"

    # Clean bin folder before starting
    clean_directory "$BIN_DIR"

    # Define extract directory
    NODE_EXTRACT_DIR="${TEMP_DIR}/node-extract"
    ensure_directory "$NODE_EXTRACT_DIR"

    # Generate SEA blob first (common for all architectures)
    generate_sea_blob

    # Process each architecture
    for arch in $SUPPORTED_ARCHS; do
        build_for_arch "$arch"
    done

    # Copy assets once (shared by all binaries)
    copy_assets

    # Clean up node SEA files
    clean_up

    log_step "All builds completed successfully!"
}

if [[ "${NODE_SEA_BUILD:-false}" == "true" ]]; then
    main
else
    npm run package
fi
