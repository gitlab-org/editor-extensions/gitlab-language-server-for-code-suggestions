import { createInterfaceId } from '@gitlab/needle';
import { GitLabUser } from './types';

export interface UserService {
  readonly user?: GitLabUser;
}

export const UserService = createInterfaceId<UserService>('UserService');
