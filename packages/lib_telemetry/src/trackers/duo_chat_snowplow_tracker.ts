import { createInterfaceId } from '@gitlab/needle';
import { TelemetryService } from '../service';

export const DUO_CHAT_CATEGORY = 'ask_gitlab_chat';

export enum DUO_CHAT_EVENT {
  BTN_CLICK = 'click_button',
}

export interface TrackFeedbackContext {
  improveWhat: string | null;
  didWhat: string | null;
  feedbackChoices: string[] | null;
}

export const isTrackFeedbackContext = (obj: unknown): obj is TrackFeedbackContext => {
  return (
    typeof obj === 'object' &&
    obj !== null &&
    ('improveWhat' in obj ? !obj.improveWhat || typeof obj.improveWhat === 'string' : false) &&
    ('didWhat' in obj ? !obj.didWhat || typeof obj.didWhat === 'string' : false) &&
    ('feedbackChoices' in obj
      ? !obj.feedbackChoices ||
        (Array.isArray(obj.feedbackChoices) &&
          obj.feedbackChoices.every((choice) => typeof choice === 'string'))
      : false)
  );
};

export type DuoChatContext = TrackFeedbackContext;

export interface DuoChatSnowplowTracker
  extends TelemetryService<DUO_CHAT_EVENT, DuoChatContext, null> {}
export const DuoChatSnowplowTracker =
  createInterfaceId<DuoChatSnowplowTracker>('DuoChatSnowplowTracker');
