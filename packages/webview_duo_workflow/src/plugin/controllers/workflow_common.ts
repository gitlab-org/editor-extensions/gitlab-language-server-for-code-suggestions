import { WorkflowAPI } from '@gitlab-lsp/workflow-api';
import { Logger } from '@gitlab-org/logging';
import { ControllerResponse, ControllerData, WorkflowGraphqlPayloadClient } from './types';

export const initWorkflowCommonController = (workflowApi: WorkflowAPI, log: Logger) => {
  return {
    async getGraphqlData({
      eventName,
      query,
      variables,
      supportedSinceInstanceVersion,
    }: WorkflowGraphqlPayloadClient): Promise<ControllerResponse> {
      try {
        const response = await workflowApi.getGraphqlData({
          query,
          variables,
          supportedSinceInstanceVersion,
        });

        return {
          eventName,
          data: response,
        };
      } catch (e) {
        const error = e as Error;

        log.error(`Failed to get graphql data for query ${query}`, error);

        return {
          eventName: 'workflowError',
          data: error.message,
        };
      }
    },

    async getProjectPath(): Promise<ControllerResponse> {
      return {
        eventName: 'setProjectPath',
        data: await workflowApi.getProjectPath(),
      };
    },

    async pullDockerImage(image: string): Promise<ControllerResponse> {
      try {
        await workflowApi.prepareExecutor(image);
        return {
          eventName: 'pullDockerImageCompleted',
          data: {
            success: true,
          },
        };
      } catch (e) {
        const error = e as Error;

        log.error(`Failed to pull docker image ${image}`, error);
        return {
          eventName: 'pullDockerImageCompleted',
          data: {
            success: false,
          },
        };
      }
    },
    async verifyDockerImage(image: string): Promise<ControllerData> {
      try {
        const isAvailable = await workflowApi.isExecutorPrepared(image);

        return [
          { eventName: 'dockerConfigured', data: true },
          {
            eventName: 'isDockerImageAvailable',
            data: isAvailable,
          },
        ];
      } catch (e) {
        const error = e as NodeJS.ErrnoException;
        log.error(`Failed to verify docker image ${image}`, error);
        if (error.code === 'ENOENT' || error.code === 'ECONNREFUSED') {
          return {
            eventName: 'dockerConfigured',
            data: false,
          };
        }
        return {
          eventName: 'workflowError',
          data: error.message,
        };
      }
    },
  };
};
