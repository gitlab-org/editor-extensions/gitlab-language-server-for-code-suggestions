import { initWorkflowConnectionController } from './workflow_connection';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let connectionMock: any;

describe('WorkflowCommonController', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let workflowConnectionController: any;

  beforeEach(() => {
    connectionMock = {
      sendNotification: jest.fn(),
    };

    workflowConnectionController = initWorkflowConnectionController(connectionMock);
  });

  describe('openUrl', () => {
    it('will open the passed url', () => {
      workflowConnectionController.openUrl({ url: 'test' });

      expect(connectionMock.sendNotification).toHaveBeenCalledWith('$/gitlab/openUrl', {
        url: 'test',
      });
    });
  });

  describe('openFile', () => {
    it('will open the passed file', () => {
      workflowConnectionController.openFile({ filePath: 'test' });
      expect(connectionMock.sendNotification).toHaveBeenCalledWith('$/gitlab/openFile', {
        filePath: 'test',
      });
    });
  });
});
