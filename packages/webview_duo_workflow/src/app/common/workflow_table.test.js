import { GlTableLite } from '@gitlab/ui';
import { createTestingPinia } from '@pinia/testing';
import { mount } from '@vue/test-utils';
import { getIDfromGraphqlId } from '../utils.ts';
import { formatTimeAgo } from '../utils/time_utils';
import { WORKFLOW_SHOW_APP } from '../router/constants';
import WorkflowTable from './workflow_table.vue';

describe('WorkflowTable', () => {
  let wrapper;

  const mockWorkflows = [
    {
      id: 'gid://gitlab/DuoWorkflow/1',
      goal: 'Goal 1',
      projectId: 'project1',
      humanStatus: 'INPUT_REQUIRED',
      createdAt: '2023-01-01',
      updatedAt: '2023-01-02',
    },
    {
      id: 'gid://gitlab/DuoWorkflow/2',
      goal: 'Goal 2',
      projectId: 'project2',
      humanStatus: 'FINISHED',
      createdAt: '2023-01-03',
      updatedAt: '2023-01-04',
    },
  ];
  const createComponent = ({ props = {} } = {}) => {
    wrapper = mount(WorkflowTable, {
      propsData: {
        workflows: mockWorkflows,
        ...props,
      },
      pinia: createTestingPinia(),
    });
  };

  const findTable = () => wrapper.findComponent(GlTableLite);

  beforeEach(() => {
    createComponent();
  });

  it('renders ID and Goal of workflows in the table', () => {
    expect(findTable().exists()).toBe(true);

    mockWorkflows.forEach((workflow) => {
      expect(findTable().html()).toContain(getIDfromGraphqlId(workflow.id));
      expect(findTable().html()).toContain(workflow.goal);
    });
  });

  it('renders the relative time for last update', () => {
    expect(findTable().html()).toContain(formatTimeAgo(mockWorkflows[0].updatedAt));
  });

  it('renders the statuses', () => {
    expect(findTable().html()).toContain('Needs input');
    expect(findTable().html()).toContain('Complete');
  });

  it('adds link to the workflow goal', () => {
    const workflowIds = mockWorkflows.map((workflow) => getIDfromGraphqlId(workflow.id));
    const workflowGoalsLinks = wrapper.findAll('[data-testid="workflow-goal-link"]');

    expect(workflowGoalsLinks.length).toBe(workflowIds.length);

    workflowGoalsLinks.wrappers.forEach((link, index) => {
      expect(JSON.parse(link.attributes('data-test'))).toEqual({
        name: WORKFLOW_SHOW_APP,
        params: { workflowId: workflowIds[index] },
      });
    });
  });
});
