import { GlLink, GlLoadingIcon } from '@gitlab/ui';
import { createTestingPinia } from '@pinia/testing';
import { shallowMount } from '@vue/test-utils';
import { useWorkflowStore } from '../../../stores/workflow';
import { useMainStore } from '../../../stores/main';
import ProjectPath from '../../../common/project_path.vue';
import WorkflowTable from '../../../common/workflow_table.vue';
import RecentWorkflowsTable from './recent_workflows_table.vue';

let wrapper;
let workflowStore;
let mainStore;

const mockWorkflows = [
  {
    id: 'gid://gitlab/DuoWorkflow/1',
    goal: 'Test Goal 1',
    projectId: 'project1',
    humanStatus: 'RUNNING',
    createdAt: '2023-01-01',
    updatedAt: '2023-01-02',
  },
  {
    id: 'gid://gitlab/DuoWorkflow/2',
    goal: 'Test Goal 2',
    projectId: 'project2',
    humanStatus: 'FINISHED',
    createdAt: '2023-01-03',
    updatedAt: '2023-01-04',
  },
];

const createComponent = () => {
  const pinia = createTestingPinia({
    stubActions: false,
  });

  wrapper = shallowMount(RecentWorkflowsTable, {
    global: {
      plugins: [pinia],
    },
    stubs: {
      GlLink: false,
    },
  });

  workflowStore = useWorkflowStore();
  mainStore = useMainStore();
};

const findLoadingIcon = () => wrapper.findComponent(GlLoadingIcon);
const findProjectPath = () => wrapper.findComponent(ProjectPath);
const findWorkflowTable = () => wrapper.findComponent(WorkflowTable);
const findLink = () => wrapper.findComponent(GlLink);

describe('RecentWorkflowsTable', () => {
  beforeEach(() => {
    createComponent();
  });

  describe('when loading', () => {
    beforeEach(() => {
      workflowStore.setRecentWorkflowsLoading(true);
    });

    afterEach(() => {
      workflowStore.setRecentWorkflowsLoading(false);
    });

    it('renders the loading icon', () => {
      expect(findLoadingIcon().exists()).toBe(true);
    });
  });

  it('renders the project path', () => {
    expect(findProjectPath().exists()).toBe(true);
  });

  describe('when projectPath is set', () => {
    beforeEach(() => {
      mainStore.setProjectPath('test-project');
    });

    it('calls getRecentWorkflows action with the project path', () => {
      expect(workflowStore.getRecentWorkflows).toHaveBeenCalledWith('test-project');
    });
  });
  describe('when projectPath is not set', () => {
    beforeEach(() => {
      mainStore.setProjectPath(undefined);
    });
    it('does not call getRecentWorkflows', () => {
      expect(workflowStore.getRecentWorkflows).not.toHaveBeenCalled();
    });
  });

  describe('when there are no recet workflows', () => {
    beforeEach(() => {
      workflowStore.recentWorkflows = [];
    });

    it('does not render the workflow table', () => {
      expect(findWorkflowTable().exists()).toBe(false);
    });

    it('renders a message indicating no workflows', () => {
      expect(wrapper.text()).toContain("You haven't created a workflow in this project yet.");
    });
  });

  describe('View all button', () => {
    describe('when there are 5 or more recent workflows', () => {
      beforeEach(() => {
        workflowStore.recentWorkflows = Array(5).fill({
          id: 'gid://gitlab/DuoWorkflow/1',
          goal: 'Test Goal',
          projectId: 'project1',
          humanStatus: 'RUNNING',
          createdAt: '2023-01-01',
          updatedAt: '2023-01-02',
        });
      });
      it('renders the "View all" link', () => {
        expect(findLink().exists()).toBe(true);
      });
    });

    describe('when there are fewer than 5 recent workflows', () => {
      beforeEach(() => {
        workflowStore.recentWorkflows = Array(4).fill({
          id: 'gid://gitlab/DuoWorkflow/1',
          goal: 'Test Goal',
          projectId: 'project1',
          humanStatus: 'RUNNING',
          createdAt: '2023-01-01',
          updatedAt: '2023-01-02',
        });
      });
      it('does not render the "View all" link', () => {
        expect(findLink().exists()).toBe(false);
      });
    });
  });

  describe('when there are recent workflows', () => {
    beforeEach(() => {
      workflowStore.recentWorkflows = mockWorkflows;
    });

    it('does not render the loading icon', () => {
      expect(findLoadingIcon().exists()).toBe(false);
    });

    it('does not render the message indicating no workflows', () => {
      expect(wrapper.text()).not.toContain("You haven't created a workflow in this project yet.");
    });

    it('renders the workflow table', () => {
      expect(findWorkflowTable().exists()).toBe(true);
      expect(findWorkflowTable().props('workflows')).toEqual(mockWorkflows);
    });
  });
});
