<script>
import {
  GlButton,
  GlIcon,
  GlLoadingIcon,
  GlLink,
  GlModal,
  GlModalDirective,
  GlTooltipDirective,
} from '@gitlab/ui';
import DuoWorkflowCheckpoint from './duo_workflow_checkpoint.vue';
import DuoWorkflowChat from './duo_workflow_chat.vue';
import { getDuoWorkflowStatusDisplay } from '../../../utils';
import { DuoWorkflowStatus } from '../../../../types';
import {
  isApprovingPlan,
  isAwaitingUserInput,
  isRunning,
  isTerminated,
} from '../../../../common/duo_workflow_status';
import { WorkflowEvent } from '../../../../common/duo_workflow_events';
import FeedbackLink from '../../../common/feedback_link.vue';
import { mapActions, mapState } from 'pinia';
import { useWorkflowStore } from '../../../stores/workflow';

export default {
  name: 'DuoWorkflowExecution',
  components: {
    DuoWorkflowCheckpoint,
    DuoWorkflowChat,
    FeedbackLink,
    GlButton,
    GlIcon,
    GlLoadingIcon,
    GlLink,
    GlModal,
  },
  directives: {
    GlModalDirective,
    GlTooltipDirective,
  },
  props: {
    checkpoint: {
      type: Object,
      required: true,
    },
    status: {
      type: String,
      required: true,
    },
  },
  data() {
    return {
      currentEvent: null,
      hasAcceptedPlan: false,
    };
  },
  computed: {
    ...mapState(useWorkflowStore, ['workflowGoal', 'isLoadingWorkflow']),
    isSendingEvent() {
      return Boolean(this.currentEvent);
    },
    stopButton() {
      return {
        eventType: WorkflowEvent.STOP,
        icon: 'stop',
        category: 'primary',
        message: 'Workflow stops after the current task completes.',
      };
    },
    conversationHistory() {
      return this.checkpoint?.channel_values?.conversation_history || {};
    },
    uiChatLog() {
      return this.checkpoint?.channel_values?.ui_chat_log || [];
    },
    statusIcon() {
      switch (this.status) {
        case DuoWorkflowStatus.CREATED:
        case DuoWorkflowStatus.RUNNING:
          return 'spinner';
        case DuoWorkflowStatus.FINISHED:
          return 'check-circle';
        case DuoWorkflowStatus.STOPPED:
          return 'stop';
        case DuoWorkflowStatus.FAILED:
          return 'error';
        case DuoWorkflowStatus.INPUT_REQUIRED:
        case DuoWorkflowStatus.PLAN_APPROVAL:
          return 'question-o';
        default:
          return 'play';
      }
    },
    isRunningStatus() {
      return this.status === DuoWorkflowStatus.RUNNING;
    },
    isStopping() {
      return this.currentEvent === this.stopButton.eventType;
    },
    isRunning() {
      return this.status && isRunning(this.status);
    },
    enableActionButtons() {
      return this.isRunning && this.status !== DuoWorkflowStatus.CREATED && !this.isStopping;
    },
    showActions() {
      return this.status && !isTerminated(this.status);
    },
    transitionText() {
      switch (this.currentEvent) {
        case WorkflowEvent.RESUME:
          return 'Resuming...';
        case WorkflowEvent.STOP:
          return 'Stopping...';
        default:
          return 'Updating...';
      }
    },
    statusText() {
      if (this.isSendingEvent) {
        return this.transitionText;
      }

      return this.status ? getDuoWorkflowStatusDisplay(this.status) : 'Loading';
    },
  },
  watch: {
    status(newStatus) {
      // Only stop the pending "stopping" action when stopped is confirmed
      if (newStatus === DuoWorkflowStatus.STOPPED && this.currentEvent === WorkflowEvent.STOP) {
        this.currentEvent = null;
      }

      if (this.currentEvent !== WorkflowEvent.STOP) {
        this.currentEvent = null;
      }
    },
  },
  methods: {
    ...mapActions(useWorkflowStore, ['resumeWorkflow']),
    handleAcceptPlan() {
      this.hasAcceptedPlan = true;

      this.currentEvent = WorkflowEvent.RESUME;
      this.$toast.show('Execution will resume shortly.');
      // First we queue the RESUME event to the service, then we resume workflow.
      this.$emit('send-workflow-event', { eventType: WorkflowEvent.RESUME });
      this.resumeWorkflow();
    },
    stopWorkflow() {
      if (isAwaitingUserInput(this.status)) {
        this.resumeWorkflow();
      }

      this.currentEvent = this.stopButton.eventType;
      this.$emit('send-workflow-event', { eventType: this.stopButton.eventType });
    },
    sendMessage(message) {
      this.$emit('send-workflow-event', {
        eventType: WorkflowEvent.MESSAGE,
        message,
      });

      // If the chat is available, then we are awaiting input from the user.
      this.currentEvent = WorkflowEvent.MESSAGE;
      this.resumeWorkflow();
    },
  },
  modal: {
    actionPrimary: {
      text: 'Stop workflow',
      attributes: {
        variant: 'danger',
      },
    },
    actionSecondary: {
      text: 'Cancel',
      attributes: {
        variant: 'default',
      },
    },
  },
};
</script>
<template>
  <div class="gl-flex gl-h-full">
    <div class="gl-w-3/5 gl-pt-11 gl-h-full gl-overflow-auto">
      <div class="gl-border-b gl-mb-6">
        <div class="gl-flex gl-justify-between">
          <div class="gl-flex gl-justify-center gl-items-center">
            <gl-loading-icon
              v-if="isLoadingWorkflow"
              size="sm"
              variant="dots"
              data-testid="loading-icon"
            />
            <gl-loading-icon
              v-else-if="isRunningStatus || isSendingEvent"
              size="sm"
              data-testid="running-status-icon"
            />
            <gl-icon v-else :name="statusIcon" :size="16" data-testid="status-icon" />
            <h2 class="gl-ml-3 gl-text-lg gl-mb-0">{{ statusText }}</h2>
          </div>
          <div class="gl-flex gl-justify-center gl-items-center gl-min-h-7">
            <feedback-link class="gl-pr-5" />
            <div v-if="showActions">
              <gl-button
                title="Stop"
                :disabled="!enableActionButtons"
                :loading="isStopping"
                v-gl-modal-directive.stop-workflow-modal
                v-gl-tooltip.hover
                icon="stop"
                data-testid="stop-button"
              />
              <gl-modal
                modal-id="stop-workflow-modal"
                title="Stop workflow?"
                :action-primary="$options.modal.actionPrimary"
                :action-secondary="$options.modal.actionSecondary"
                @primary="stopWorkflow"
                size="sm"
              >
                Are you sure you want to stop this workflow? This action cannot be undone.
              </gl-modal>
            </div>
          </div>
        </div>
        <div v-if="workflowGoal" class="gl-text-secondary gl-py-5 gl-text-base overflow-text" data-testid="goal">
          {{ workflowGoal }}
        </div>
        <div
          v-else
          class="gl-flex gl-animate-skeleton-loader gl-h-4 gl-my-5 gl-rounded-base"
          data-testid="goal-skeleton-loader"
        ></div>
      </div>

      <duo-workflow-checkpoint
        :checkpoint="checkpoint"
        :status="status"
        :has-accepted-plan="hasAcceptedPlan"
        class="gl-min-w-1/2 gl-flex-grow gl-max-h-full"
      />
    </div>
    <div class="gl-w-2/5 gl-ml-4 gl-max-h-full">
      <duo-workflow-chat
        :conversation-history="conversationHistory"
        :ui-chat-log="uiChatLog"
        :status="status"
        :is-sending-event="isSendingEvent"
        @message="sendMessage"
        @accept-plan="handleAcceptPlan"
      />
    </div>
  </div>
</template>
