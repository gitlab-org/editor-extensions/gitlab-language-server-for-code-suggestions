import { shallowMount } from '@vue/test-utils';
import { createTestingPinia } from '@pinia/testing';
import { DuoWorkflowStatus } from '../../../../types/index.ts';
import DuoWorkflowCheckpoint from './duo_workflow_checkpoint.vue';
import CheckpointSkeletonLoader from './checkpoint_skeleton_loader.vue';
import StepStatusBadge from './step_status_badge.vue';

describe('DuoWorkflowCheckpoint', () => {
  let wrapper;
  let pinia;

  const defaultSteps = [
    { description: 'Find stuff', status: 'Completed' },
    { description: 'Change code', status: 'running' },
  ];

  const isActiveAttribute = 'active-step';

  const createWrapper = ({ props = {}, steps = defaultSteps } = {}) => {
    pinia = createTestingPinia();

    wrapper = shallowMount(DuoWorkflowCheckpoint, {
      propsData: {
        status: props.status || DuoWorkflowStatus.RUNNING,
        checkpoint: {
          ts: '2024-01-01T12:00:00Z',
          channel_values: {
            plan: {
              steps,
            },
            status: 'Planning',
          },
        },
        ...props,
      },
      global: {
        plugins: [pinia],
      },
    });
  };

  const findSkeletonLoader = () => wrapper.findComponent(CheckpointSkeletonLoader);
  const findSteps = () => wrapper.findAll('li');
  const findStatusBadges = () => wrapper.findAllComponents(StepStatusBadge);

  describe('when loading', () => {
    beforeEach(() => {
      createWrapper({ steps: [] });
    });

    it('should render the skeleton loader', () => {
      expect(findSkeletonLoader().exists()).toBe(true);
    });

    it('does not render any steps', () => {
      expect(findSteps()).toHaveLength(0);
    });
  });

  describe('when there are steps', () => {
    beforeEach(() => {
      createWrapper();
    });

    it('does not render the skeleton loader', () => {
      expect(findSkeletonLoader().exists()).toBe(false);
    });

    it('renders the steps', () => {
      expect(findSteps()).toHaveLength(2);
    });

    it('renders the status badge', () => {
      expect(findStatusBadges()).toHaveLength(2);
    });
  });

  describe('currentStep', () => {
    describe('when status is terminated', () => {
      beforeEach(() => {
        createWrapper({
          props: { status: DuoWorkflowStatus.FAILED },
        });
      });

      it('does not highlight any step', () => {
        findSteps().wrappers.forEach((step) => {
          expect(step.attributes('data-testid')).not.toBe(isActiveAttribute);
        });
      });
    });

    describe('when first step is not started', () => {
      describe('and user has not accepted plan', () => {
        beforeEach(() => {
          createWrapper({
            steps: [{ description: 'Not started step', status: 'Not Started' }, ...defaultSteps],
          });
        });

        it('does not highlight any step', () => {
          findSteps().wrappers.forEach((step) => {
            expect(step.attributes('data-testid')).not.toBe(isActiveAttribute);
          });
        });
      });

      describe('and user has accepted plan', () => {
        beforeEach(() => {
          createWrapper({
            props: { hasAcceptedPlan: true },
            steps: [{ description: 'Not started step', status: 'running' }, ...defaultSteps],
          });
        });
        it('highlights the step in progress', () => {
          expect(findSteps().at(0).attributes('data-testid')).toBe(isActiveAttribute);
          expect(findSteps().at(1).attributes('data-testid')).not.toBe(isActiveAttribute);
        });
      });
    });

    describe('when the first step is completed', () => {
      beforeEach(() => {
        createWrapper({
          steps: [{ description: 'ok', status: 'Completed' }, ...defaultSteps],
        });
      });

      it('highlights the next non completed step', () => {
        expect(findSteps()).toHaveLength(3);

        expect(findSteps().at(0).attributes('data-testid')).not.toBe(isActiveAttribute);
        expect(findSteps().at(1).attributes('data-testid')).not.toBe(isActiveAttribute);
        expect(findSteps().at(2).attributes('data-testid')).toBe(isActiveAttribute);
      });
    });
  });
});
