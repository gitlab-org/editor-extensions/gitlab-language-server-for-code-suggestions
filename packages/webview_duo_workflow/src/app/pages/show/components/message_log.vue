<script>
import throttle from 'lodash/throttle';
import {
  GlButton,
  GlFormGroup,
  GlFormInputGroup,
  GlFormTextarea,
  GlForm,
  GlSprintf,
  GlLoadingIcon,
} from '@gitlab/ui';

export const MESSAGE_MODEL_ROLES = {
  user: 'user',
  system: 'system',
  assistant: 'assistant',
};

export const i18n = {
  CHAT_SUBMIT_LABEL: 'Send chat message.',
};

export default {
  components: {
    GlButton,
    GlFormGroup,
    GlFormInputGroup,
    GlFormTextarea,
    GlForm,
    GlLoadingIcon,
    GlSprintf,
  },
  props: {
    /**
     * Array of messages to display in the chat.
     */
    messages: {
      type: Array,
      required: false,
      default: () => [],
    },
    /**
     * A non-recoverable error message to display in the chat.
     */
    error: {
      type: String,
      required: false,
      default: '',
    },
    /**
     * Whether the conversational interfaces should be enabled.
     */
    isChatAvailable: {
      type: Boolean,
      required: false,
      default: true,
    },
    /**
     * Override the default chat prompt placeholder text.
     */
    chatPromptPlaceholder: {
      type: String,
      required: false,
      default: '',
    },
    /**
     * Custom text to display when chat is disabled.
     */
    inputDisabledText: {
      type: String,
      required: false,
      default: 'Chat is currently unavailable',
    },
    /**
     * Whether to show Duo working message or not
     */
    isDuoWorking: {
      type: Boolean,
      required: false,
      default: false,
    },
    /**
     * Custom text for duo working message
     */
    duoWorkingText: {
      type: String,
      required: false,
      default: '%{strongStart}GitLab Duo%{strongEnd} is working',
    },

    /**
     * The maximum number of characters a user message can be
     */
    maxMessageSize: {
      type: Number,
      required: false,
      default: 4096,
    },
  },
  data() {
    return {
      prompt: '',
      scrolledToBottom: true,
      compositionJustEnded: false,
    };
  },
  computed: {
    hasMessages() {
      return this.messages?.length > 0;
    },
    lastMessage() {
      return this.messages?.at(-1);
    },
    inputPlaceholder() {
      return this.chatPromptPlaceholder;
    },
    sendDisabled() {
      return !this.prompt || !this.messageLengthValid;
    },
    charactersRemainingText() {
      return `${this.maxMessageSize - this.prompt.length} character(s) remaining`;
    },
    charactersOverText() {
      return `${this.prompt.length - this.maxMessageSize} character(s) over`;
    },
    messageLengthValid() {
      return this.prompt.length <= this.maxMessageSize;
    },
  },
  watch: {
    lastMessage(newMessage) {
      if (
        this.scrolledToBottom ||
        newMessage.message_type.toLowerCase() === MESSAGE_MODEL_ROLES.user
      ) {
        // only scroll to bottom on new message if the user hasn't explicitly scrolled up to view an earlier message
        // or if the user has just submitted a new message
        this.scrollToBottom();
      }
    },
  },
  created() {
    this.handleScrollingThrottled = throttle(this.handleScrolling, 200); // Assume a 200ms throttle for example
  },
  mounted() {
    this.scrollToBottom();
  },
  methods: {
    onCompositionEnd() {
      this.compositionJustEnded = true;
    },
    sendChatPrompt() {
      if (!this.messageLengthValid) return;
      /**
       * Emitted when a new user prompt should be sent out.
       *
       * @param {String} prompt The user prompt to send.
       */
      this.$emit('send-chat-prompt', this.prompt.trim());

      this.setPromptAndFocus();
    },
    handleScrolling(event) {
      const { scrollTop, offsetHeight, scrollHeight } = event.target;
      this.scrolledToBottom = scrollTop + offsetHeight >= scrollHeight;
    },
    async scrollToBottom() {
      await this.$nextTick();

      this.$refs.anchor?.scrollIntoView?.();
    },
    focusChatInput() {
      // This method is also called directly by consumers of this component
      // https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/blob/dae2d4669ab4da327921492a2962beae8a05c290/webviews/vue2/gitlab_duo_chat/src/App.vue#L109
      this.$refs.prompt?.$el?.focus();
    },
    sendChatPromptOnEnter(e) {
      const { metaKey, ctrlKey, altKey, shiftKey, isComposing } = e;
      const isModifierKey = metaKey || ctrlKey || altKey || shiftKey;

      return !(isModifierKey || isComposing || this.compositionJustEnded);
    },
    onInputKeyup(e) {
      const { key } = e;

      if (key === 'Enter' && this.sendChatPromptOnEnter(e)) {
        e.preventDefault();

        this.sendChatPrompt();
      }

      this.compositionJustEnded = false;
    },
    async setPromptAndFocus(prompt = '') {
      this.prompt = prompt;
      await this.$nextTick();
      this.focusChatInput();
    },
  },
  i18n,
};
</script>
<template>
  <aside
    id="chat-component"
    class="markdown-code-block gl-w-full gl-border-l gl-shadow-none gl-flex gl-flex-col gl-h-[100vh]"
    role="complementary"
    data-testid="chat-component"
  >
    <div
      class="gl-bg-inherit gl-h-full gl-shrink gl-overflow-auto gl-pt-4 gl-pl-4"
      data-testid="chat-history"
      @scroll="handleScrollingThrottled"
    >
      <slot v-for="msg in messages" name="message" :message="msg"> </slot>
      <div v-if="isDuoWorking" class="gl-flex gl-items-center" data-testid="duo-working-message">
        <gl-loading-icon :inline="true" variant="dots" class="gl-pr-3" />
        <gl-sprintf :message="duoWorkingText">
          <template #strong="{ content }">
            <strong>{{ content }}&nbsp;</strong>
          </template>
        </gl-sprintf>
      </div>
      <div key="anchor" ref="anchor" class="scroll-anchor"></div>
    </div>
    <footer
      data-testid="chat-footer"
      class="duo-chat-drawer-footer gl-b-white gl-border-t gl-bg-gray-10 gl-p-5"
      :class="{ 'duo-chat-drawer-body-scrim-on-footer': !scrolledToBottom }"
    >
      <gl-form
        v-if="isChatAvailable && hasMessages"
        data-testid="chat-prompt-form"
        @submit.stop.prevent="sendChatPrompt"
      >
        <div class="gl-relative gl-max-w-full">
          <slot name="chat-footer"></slot>
        </div>

        <gl-form-group
          :state="messageLengthValid"
          :invalid-feedback="charactersOverText"
          :valid-feedback="charactersRemainingText"
          class="gl-mb-0"
          data-testid="chat-prompt-form-group"
        >
          <gl-form-input-group :state="messageLengthValid">
            <div
              class="duo-chat-input gl-min-h-8 gl-max-w-full gl-grow gl-rounded-base gl-bg-white gl-align-top gl-shadow-inner-1-gray-400 gl-leading-1"
              :data-value="prompt"
            >
              <gl-form-textarea
                ref="prompt"
                v-model="prompt"
                :state="messageLengthValid"
                data-testid="chat-prompt-input"
                class="gl-absolute !gl-h-full gl-rounded-br-none gl-rounded-tr-none !gl-bg-transparent !gl-py-4 !gl-shadow-none"
                :class="{ 'gl-truncate': !prompt }"
                :placeholder="inputPlaceholder"
                autofocus
                @keydown.enter.exact.native.prevent
                @keyup.native="onInputKeyup"
                @compositionend="onCompositionEnd"
              />
            </div>
            <template #append>
              <gl-button
                icon="paper-airplane"
                category="primary"
                variant="confirm"
                class="!gl-absolute gl-bottom-2 gl-right-2 !gl-rounded-base"
                data-testid="chat-prompt-submit-button"
                :disabled="sendDisabled"
                :aria-label="$options.i18n.CHAT_SUBMIT_LABEL"
                @click="sendChatPrompt"
              />
            </template>
          </gl-form-input-group>
        </gl-form-group>
      </gl-form>
      <div
        v-else
        class="gl-flex gl-justify-center gl-items-center gl-h-full"
        data-testid="chat-disabled-text"
      >
        {{ inputDisabledText }}
      </div>
    </footer>
  </aside>
</template>
