import { shallowMount } from '@vue/test-utils';
import { GlButton } from '@gitlab/ui';
import WorkflowEndMessage from './workflow_end.vue';
import AgentMessage from './agent.vue';

describe('WorkflowEndMessage', () => {
  let wrapper;

  const createComponent = () => {
    wrapper = shallowMount(WorkflowEndMessage);
  };

  const findAgentMessage = () => wrapper.findComponent(AgentMessage);
  const findNewWorkflowButton = () => wrapper.findComponent(GlButton);

  describe('basic rendering', () => {
    beforeEach(() => {
      createComponent();
    });

    it('renders the end message content in agent message component', () => {
      expect(findAgentMessage().exists()).toBe(true);
      expect(findAgentMessage().props('message')).toEqual({
        content:
          "Review the changes. To improve the implementation, create another workflow and describe the specific changes you'd like to see.",
      });
    });

    it('renders the New Workflow button', () => {
      const button = findNewWorkflowButton();
      expect(button.exists()).toBe(true);
      expect(button.text()).toBe('New workflow');
    });
  });
});
