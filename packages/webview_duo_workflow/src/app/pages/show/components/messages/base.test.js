import { shallowMount } from '@vue/test-utils';
import BaseMessage from './base.vue';

describe('BaseMessage', () => {
  let wrapper;

  const createComponent = ({ props = {}, mountFn = shallowMount } = {}) => {
    wrapper = mountFn(BaseMessage, {
      propsData: {
        message: {
          content: 'Test message',
          role: 'user',
          ...props,
        },
        ...props,
      },
      global: {
        directives: {
          SafeHtml: {},
          GlTooltip: {},
        },
        provide: {
          renderGFM: () => jest.fn(),
          renderMarkdown: (content) => content,
        },
      },
    });
  };

  const findErrorIcon = () => wrapper.find('[data-testid="error"]');

  beforeEach(() => {
    createComponent();
  });

  it('renders the component', () => {
    expect(wrapper.exists()).toBe(true);
  });

  describe('basic rendering', () => {
    it('renders the message content', () => {
      createComponent({ props: { message: { content: 'Test message' } } });
      expect(wrapper.text()).toContain('Test message');
    });

    it('renders error icon when message has errors', () => {
      createComponent({
        props: { message: { content: 'Error message', errors: ['Something went wrong'] } },
      });
      expect(findErrorIcon().exists()).toBe(true);
    });

    it('renders error message when present', () => {
      const errorMessage = 'Error occurred';
      createComponent({
        props: { message: { content: 'Test message', errors: [errorMessage] } },
      });
      expect(wrapper.text()).toContain(errorMessage);
    });
  });
});
