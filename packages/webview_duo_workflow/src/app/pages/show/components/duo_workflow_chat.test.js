import 'core-js/proposals/set-methods-v2';
import { shallowMount } from '@vue/test-utils';
import { v4 } from 'uuid';
import { DuoWorkflowStatus } from '../../../../common/duo_workflow_status.ts';
import DuoWorkflowChat from './duo_workflow_chat.vue';
import MessageLog from './message_log.vue';
import SystemMessage from './messages/system.vue';
import AgentMessage from './messages/agent.vue';
import UserMessage from './messages/user.vue';
import WorkflowEndMessage from './messages/workflow_end.vue';

jest.mock('uuid');

const mockToolMessage = {
  message_type: 'tool',
  content: 'read_file ./file',
  timestamp: new Date() - 20,
};
const mockProseMessage = {
  message_type: 'agent',
  content: 'I am handing this over',
  timestamp: new Date() - 10,
};
const mockWorkflowEndMessage = {
  message_type: 'workflow_end',
  content: 'all done',
  timestamp: new Date() - 5,
};
const mockUiChatLog = [mockToolMessage, mockProseMessage, mockWorkflowEndMessage];

describe('DuoWorkflowChat', () => {
  let wrapper;

  const createWrapper = (propsData = {}) => {
    wrapper = shallowMount(DuoWorkflowChat, {
      propsData: {
        uiChatLog: mockUiChatLog,
        status: DuoWorkflowStatus.FINISHED,
        ...propsData,
      },
      stubs: {
        MessageLog,
      },
    });
  };

  const findMessageLog = () => wrapper.findComponent(MessageLog);

  const getAllMessages = () => findMessageLog().props('messages');

  const getAllMessageContent = () => getAllMessages().map((w) => w.content);

  describe('messages', () => {
    beforeEach(() => {
      createWrapper();
    });

    it('shows chat', () => {
      const allMessages = getAllMessageContent();

      expect(allMessages).toEqual([
        mockToolMessage.content,
        mockProseMessage.content,
        mockWorkflowEndMessage.content,
      ]);
    });

    it('distinguishes between prose and tools', () => {
      const allMessages = getAllMessages();

      const [systemMessage, agentMessage, workflowEndMessage] = findMessageLog().findAllComponents(
        '[data-testid="workflow-message"]',
      ).wrappers;
      expect(systemMessage.is(SystemMessage)).toBe(true);
      expect(agentMessage.is(AgentMessage)).toBe(true);
      expect(workflowEndMessage.is(WorkflowEndMessage)).toBe(true);
      expect(allMessages).toMatchObject([
        { message_type: 'tool' },
        { message_type: 'agent' },
        { message_type: 'workflow_end' },
      ]);
    });

    describe('Duo is working logic', () => {
      it.each`
        status                              | expected
        ${DuoWorkflowStatus.FINISHED}       | ${false}
        ${DuoWorkflowStatus.RUNNING}        | ${true}
        ${DuoWorkflowStatus.PLAN_APPROVAL}  | ${false}
        ${DuoWorkflowStatus.INPUT_REQUIRED} | ${false}
        ${DuoWorkflowStatus.FAILED}         | ${false}
      `('pass the prop correctly when status is $status', ({ expected, status }) => {
        createWrapper({ status });

        expect(findMessageLog().props('isDuoWorking')).toBe(expected);
      });

      describe('when sending an event', () => {
        beforeEach(() => {
          createWrapper({ status: DuoWorkflowStatus.PLAN_APPROVAL, isSendingEvent: true });
        });

        it('renders the duo is working logic', () => {
          expect(findMessageLog().props('isDuoWorking')).toBe(true);
        });
      });
    });

    describe('chat input', () => {
      it('shows the chat input when the workflow is in needs input status', () => {
        createWrapper({ status: DuoWorkflowStatus.INPUT_REQUIRED });

        expect(findMessageLog().props('isChatAvailable')).toBe(true);
      });

      it('highlights the last message when the workflow is in needs input status', () => {
        createWrapper({ status: DuoWorkflowStatus.INPUT_REQUIRED });

        const [message] = getAllMessages().reverse();

        expect(message.highlight).toBe(true);
      });

      it('hides chat input when workflow is running', () => {
        createWrapper({ status: DuoWorkflowStatus.RUNNING });

        expect(findMessageLog().props('isChatAvailable')).toBe(false);
      });

      it('adds user messages to the shown messges and emits the message up', async () => {
        v4.mockReturnValue('fake uuid');
        createWrapper({ status: DuoWorkflowStatus.RUNNING });

        await findMessageLog().vm.$emit('send-chat-prompt', 'hello');

        expect(wrapper.emitted('message')).toEqual([
          [{ message: 'hello', correlation_id: 'fake uuid' }],
        ]);
        const userMessage = findMessageLog()
          .findAllComponents('[data-testid="workflow-message"]')
          .wrappers.at(-1);

        expect(findMessageLog().props('messages').at(-1)).toMatchObject({
          content: 'hello',
          message_type: 'user',
        });

        expect(userMessage.is(UserMessage));
      });
    });
  });
});
