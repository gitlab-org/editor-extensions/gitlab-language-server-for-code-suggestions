import { shallowMount } from '@vue/test-utils';
import { GlFormInputGroup, GlFormGroup } from '@gitlab/ui';
import MessageLog, { MESSAGE_MODEL_ROLES } from './message_log.vue';

describe('MessageLog', () => {
  let wrapper;
  let messages;

  const createComponent = (props = {}, scopedSlots = {}) => {
    wrapper = shallowMount(MessageLog, {
      propsData: props,
      scopedSlots,
      stubs: {
        GlFormInputGroup,
        GlFormGroup,
      },
    });
  };

  const findChatHistory = () => wrapper.findComponent('[data-testid="chat-history"]');
  const findChatFooter = () => wrapper.findComponent('[data-testid="chat-footer"]');
  const findChatPromptForm = () => wrapper.findComponent('[data-testid="chat-prompt-form"]');
  const findChatPromptFormGroup = () =>
    wrapper.findComponent('[data-testid="chat-prompt-form-group"]');
  const findChatPromptInput = () => wrapper.findComponent('[data-testid="chat-prompt-input"]');
  const findChatPromptSubmitButton = () =>
    wrapper.findComponent('[data-testid="chat-prompt-submit-button"]');
  const findDisabledText = () => wrapper.find('[data-testid="chat-disabled-text"]');
  const findDuoWorkingMessage = () => wrapper.find('[data-testid="duo-working-message"]');

  beforeEach(() => {
    messages = [
      { id: 1, content: 'test', message_type: MESSAGE_MODEL_ROLES.user },
      { id: 2, content: 'response', message_type: MESSAGE_MODEL_ROLES.assistant },
    ];
  });

  describe('default mount', () => {
    beforeEach(() => {
      createComponent();
    });

    it('renders the component container', () => {
      expect(wrapper.find('#chat-component').exists()).toBe(true);
    });

    it('renders chat history container', () => {
      expect(findChatHistory().exists()).toBe(true);
    });

    it('does not render chat input when no messages exist', () => {
      expect(findChatFooter().exists()).toBe(true);
      expect(findChatPromptInput().exists()).toBe(false);
    });
  });

  describe('when duo is working', () => {
    beforeEach(() => {
      createComponent({
        messages,
        isDuoWorking: true,
      });
    });

    it('renders duo working message', () => {
      expect(findDuoWorkingMessage().exists()).toBe(true);
    });
  });

  describe('when duo is not working', () => {
    beforeEach(() => {
      createComponent({
        messages,
        isDuoWorking: false,
      });
    });

    it('does not render duo working message', () => {
      expect(findDuoWorkingMessage().exists()).toBe(false);
    });
  });

  describe('component structure', () => {
    describe('with messages and chat available', () => {
      messages = [{ id: 1, content: 'test', message_type: MESSAGE_MODEL_ROLES.user }];

      beforeEach(() => {
        createComponent({
          messages,
          isChatAvailable: true,
        });
      });

      it('renders chat footer when messages exist', () => {
        expect(findChatFooter().exists()).toBe(true);
      });

      it('renders chat input form', () => {
        expect(findChatPromptForm().exists()).toBe(true);
      });

      it('renders submit button', async () => {
        expect(findChatPromptSubmitButton().exists()).toBe(true);
      });
    });

    describe('with chat unavailable', () => {
      messages = [{ id: 1, content: 'test', message_type: MESSAGE_MODEL_ROLES.user }];

      beforeEach(() => {
        createComponent({
          messages,
          isChatAvailable: false,
          inputDisabledText: 'Chat is currently sleeping',
        });
      });

      it('renders chat footer', () => {
        expect(findChatFooter().exists()).toBe(true);
      });

      it('does not render input', () => {
        expect(findChatPromptInput().exists()).toBe(false);
      });

      it('renders disabled chat footer text', () => {
        expect(findDisabledText().text()).toBe('Chat is currently sleeping');
      });
    });

    describe('message rendering', () => {
      messages = [
        { id: 1, content: 'test1', message_type: MESSAGE_MODEL_ROLES.user },
        { id: 2, content: 'test2', message_type: MESSAGE_MODEL_ROLES.assistant },
      ];

      it('passes messages to slots correctly', () => {
        const messageSlot = jest.fn();
        createComponent(
          { messages },
          {
            message: messageSlot,
          },
        );

        expect(messageSlot).toHaveBeenCalledTimes(2);
        expect(messageSlot).toHaveBeenNthCalledWith(
          1,
          expect.objectContaining({ message: messages[0] }),
        );
        expect(messageSlot).toHaveBeenNthCalledWith(
          2,
          expect.objectContaining({ message: messages[1] }),
        );
      });
    });
  });

  describe('message sending functionality', () => {
    describe('character counter', () => {
      let maxMessageSize;
      let message;

      beforeEach(async () => {
        maxMessageSize = 4096;
        messages = [{ id: 1, content: 'test', message_type: MESSAGE_MODEL_ROLES.user }];
        createComponent({ messages, maxMessageSize });
      });

      describe('under max', () => {
        beforeEach(async () => {
          message = 'test message';
          await findChatPromptInput().vm.$emit('input', message);
        });

        it('shows how many characters remain', () => {
          expect(findChatPromptFormGroup().attributes('validfeedback')).toBe(
            `${maxMessageSize - message.length} character(s) remaining`,
          );
          expect(findChatPromptFormGroup().attributes('state')).toBeTruthy();
        });
      });

      describe('over max', () => {
        beforeEach(async () => {
          message = new Array(maxMessageSize + 1).fill('a').join('');
          await findChatPromptInput().vm.$emit('input', message);
        });

        it('shows how many characters remain', () => {
          expect(findChatPromptFormGroup().attributes('invalidfeedback')).toBe(
            `${message.length - maxMessageSize} character(s) over`,
          );
          expect(findChatPromptFormGroup().attributes('state')).toBeFalsy();
        });

        it('disables the send ability', async () => {
          expect(findChatPromptSubmitButton().props('disabled')).toBe(true);

          await findChatPromptInput().trigger('keyup', {
            key: 'Enter',
          });

          expect(wrapper.emitted('send-chat-prompt')).toBeUndefined();
        });
      });
    });
    describe('sendChatPrompt', () => {
      beforeEach(async () => {
        messages = [{ id: 1, content: 'test', message_type: MESSAGE_MODEL_ROLES.user }];
        createComponent({ messages });
        await findChatPromptInput().vm.$emit('input', '   test message   ');
        await findChatPromptSubmitButton().vm.$emit('click');
      });

      it('emits send-chat-prompt event with trimmed prompt', async () => {
        expect(wrapper.emitted('send-chat-prompt')[0]).toEqual(['test message']);
      });

      it('clears prompt after sending', () => {
        expect(findChatPromptInput().props('value')).toBe('');
      });
    });

    describe('sendChatPromptOnEnter', () => {
      let event;

      beforeEach(async () => {
        createComponent({ messages });
        await findChatPromptInput().vm.$emit('input', 'test message');
        event = {
          key: 'Enter',
          metaKey: false,
          ctrlKey: false,
          altKey: false,
          shiftKey: false,
          isComposing: false,
        };
      });

      it('sends for plain Enter key', async () => {
        await findChatPromptInput().trigger('keyup', event);

        expect(wrapper.emitted('send-chat-prompt')).toEqual([['test message']]);
      });

      it('does not send when meta key is pressed', async () => {
        event.metaKey = true;
        await findChatPromptInput().trigger('keyup', event);

        expect(wrapper.emitted('send-chat-prompt')).toBeUndefined();
      });

      it('does not send when ctrl key is pressed', async () => {
        event.ctrlKey = true;

        await findChatPromptInput().trigger('keyup', event);

        expect(wrapper.emitted('send-chat-prompt')).toBeUndefined();
      });

      it('does not send when alt key is pressed', async () => {
        event.altKey = true;

        await findChatPromptInput().trigger('keyup', event);

        expect(wrapper.emitted('send-chat-prompt')).toBeUndefined();
      });

      it('does not send when shift key is pressed', async () => {
        event.shiftKey = true;

        await findChatPromptInput().trigger('keyup', event);

        expect(wrapper.emitted('send-chat-prompt')).toBeUndefined();
      });

      it('does not send during IME composition', async () => {
        event.isComposing = true;

        await findChatPromptInput().trigger('keyup', event);

        expect(wrapper.emitted('send-chat-prompt')).toBeUndefined();
      });

      it('does not send when composition just ended', async () => {
        await findChatPromptInput().vm.$emit('compositionend');

        await findChatPromptInput().trigger('keyup', event);

        expect(wrapper.emitted('send-chat-prompt')).toBeUndefined();
      });
    });
  });
});
