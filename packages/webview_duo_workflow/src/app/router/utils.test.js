import {
  getRoutesNames,
  isPageWithoutContainer,
  isPageWithLimitedWidth,
  isValidRoute,
  validateRoute,
} from './utils';
import { WORKFLOW_SHOW_APP, WORKFLOW_NEW_APP, WORKFLOW_INDEX_APP } from './constants';

describe('utils', () => {
  describe('getRoutesNames', () => {
    it('should return an array of route names', () => {
      const routeNames = getRoutesNames();

      expect(Array.isArray(routeNames)).toBe(true);
      expect(routeNames.length).toBeGreaterThan(0);
      expect(routeNames).toContain(WORKFLOW_SHOW_APP);
    });
  });

  describe('isPageWithoutContainer', () => {
    it('should return true for WORKFLOW_SHOW_APP', () => {
      expect(isPageWithoutContainer(WORKFLOW_SHOW_APP)).toBe(true);
    });

    it('should return false for other valid page names', () => {
      const routeNames = getRoutesNames();
      const otherPageName = routeNames.find((name) => name !== WORKFLOW_SHOW_APP);
      if (otherPageName) {
        expect(isPageWithoutContainer(otherPageName)).toBe(false);
      }
    });
  });

  describe('isValidRoute', () => {
    it('should return true for valid page names', () => {
      const validRouteNames = getRoutesNames();
      expect(isValidRoute(validRouteNames[0])).toBe(true);
    });

    it('should return false for invalid page names', () => {
      const invalidRouteName = 'INVALID_ROUTE';
      expect(isValidRoute(invalidRouteName)).toBe(false);
    });
  });

  describe('validateRoute', () => {
    it('should throw an error for invalid page names', () => {
      const invalidRouteName = 'INVALID_ROUTE';
      expect(() => validateRoute(invalidRouteName)).toThrow(
        'Argument pageName is not valid, name: INVALID_ROUTE',
      );
    });

    it('should not throw an error for valid page names', () => {
      const validRouteNames = getRoutesNames();
      validRouteNames.forEach((name) => {
        expect(() => validateRoute(name)).not.toThrow();
      });
    });
  });

  describe('isPageWithLimitedWidth', () => {
    it('should return true for WORKFLOW_NEW_APP', () => {
      expect(isPageWithLimitedWidth(WORKFLOW_NEW_APP)).toBe(true);
    });

    it('should return true for WORKFLOW_INDEX_APP', () => {
      expect(isPageWithLimitedWidth(WORKFLOW_INDEX_APP)).toBe(true);
    });

    it('should return false for other valid page names', () => {
      const routeNames = getRoutesNames();
      const otherPageName = routeNames.find(
        (name) => name !== WORKFLOW_NEW_APP && name !== WORKFLOW_INDEX_APP,
      );
      if (otherPageName) {
        expect(isPageWithLimitedWidth(otherPageName)).toBe(false);
      }
    });
  });
});
