import { createRoutes } from './routes';
import { WORKFLOW_NEW_APP, WORKFLOW_SHOW_APP, WORKFLOW_INDEX_APP } from './constants';

export const getRoutesNames = () => {
  return createRoutes().map((route) => route.name);
};

export const isValidRoute = (pageName) => {
  return getRoutesNames().includes(pageName);
};

export const validateRoute = (pageName) => {
  if (!isValidRoute(pageName)) {
    throw new Error(`Argument pageName is not valid, name: ${pageName}`);
  }
};

export const isPageWithoutContainer = (pageName) => {
  validateRoute(pageName);
  return [WORKFLOW_SHOW_APP].includes(pageName);
};

export const isPageWithLimitedWidth = (pageName) => {
  validateRoute(pageName);

  return [WORKFLOW_NEW_APP, WORKFLOW_INDEX_APP].includes(pageName);
};
