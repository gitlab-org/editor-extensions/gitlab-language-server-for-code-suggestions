import { SupportedSinceInstanceVersion } from '@gitlab-org/core';
import { createInterfaceId } from '@gitlab/needle';
import {
  DuoWorkflowEvent,
  DuoWorkflowMessage,
  DuoWorkflowStatusUpdate,
  DuoWorkflowStatusUpdateResponse,
  RunWorkflowPayload,
  WorkflowEvent,
} from '@gitlab-org/webview-duo-workflow';

export type WorkflowGraphqlPayload = {
  query: string;
  variables?: Record<string, unknown>;
  supportedSinceInstanceVersion?: SupportedSinceInstanceVersion;
};

// FIXME: rename to WorkflowApi -- the LS project uses camel case for abbreviations  (Api, GraphQl)
export interface WorkflowAPI {
  disconnectCable(): void;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  getGraphqlData(payload: WorkflowGraphqlPayload): Promise<any>;
  getProjectPath(): string;
  isExecutorPrepared(image: string): Promise<boolean>;
  prepareExecutor(image: string): Promise<void>;
  runWorkflow(payload: RunWorkflowPayload): Promise<string>;
  watchWorkflowExecutor(): Promise<{ StatusCode: number }>;
  subscribeToUpdates(
    messageCallback: (message: DuoWorkflowEvent) => void,
    workflowId: string,
  ): Promise<void>;
  sendEvent(
    workflowID: string,
    eventType: WorkflowEvent,
    message?: DuoWorkflowMessage,
  ): Promise<void>;
  updateStatus(statusUpdate: DuoWorkflowStatusUpdate): Promise<DuoWorkflowStatusUpdateResponse>;
}

export const WorkflowAPI = createInterfaceId<WorkflowAPI>('WorkflowAPI');
