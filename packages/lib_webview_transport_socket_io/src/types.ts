import { WebviewId, WebviewInstanceId } from '@gitlab-org/webview-plugin';

export type WebviewInstanceInfo = {
  webviewId: WebviewId;
  webviewInstanceId: WebviewInstanceId;
};
