import {
  WebviewPlugin,
  WebviewPluginSetupParams,
  CreatePluginMessageMap,
} from '@gitlab-org/webview-plugin';
import { Logger } from '@gitlab-org/logging';
import { GitLabApiService, UserService } from '@gitlab-org/core';
import { AIContextManager } from '@gitlab-org/ai-context';
import { DuoChatSnowplowTracker } from '@gitlab-org/telemetry';
import { Injectable } from '@gitlab/needle';
import { Messages, WEBVIEW_ID, WEBVIEW_TITLE } from '../contract';
import { GitLabChatController } from './chat_controller';

@Injectable(WebviewPlugin, [
  GitLabApiService,
  UserService,
  Logger,
  AIContextManager,
  DuoChatSnowplowTracker,
])
export class DuoChatWebviewPlugin implements WebviewPlugin<Messages> {
  readonly id = WEBVIEW_ID;

  readonly title = WEBVIEW_TITLE;

  #gitlabApiClient: GitLabApiService;

  #userService: UserService;

  #logger: Logger;

  #aiContextManager: AIContextManager;

  #telemetryTracker: DuoChatSnowplowTracker;

  constructor(
    gitlabApiClient: GitLabApiService,
    userService: UserService,
    logger: Logger,
    aiContextManager: AIContextManager,
    telemetryTracker: DuoChatSnowplowTracker,
  ) {
    this.#gitlabApiClient = gitlabApiClient;
    this.#userService = userService;
    this.#logger = logger;
    this.#aiContextManager = aiContextManager;
    this.#telemetryTracker = telemetryTracker;
  }

  setup(params: WebviewPluginSetupParams<CreatePluginMessageMap<Messages>>) {
    const { webview, extension } = params;

    webview.onInstanceConnected((_, webviewMessageBus) => {
      const controller = new GitLabChatController(
        this.#gitlabApiClient,
        webviewMessageBus,
        extension,
        this.#logger,
        this.#userService,
        this.#aiContextManager,
        this.#telemetryTracker,
      );

      const newPromptSubscription = extension.onNotification(
        'newPrompt',
        async ({ prompt, fileContext }) => {
          await controller.handleExtensionPrompt(prompt, fileContext);
        },
      );

      return {
        dispose: () => {
          controller.dispose();
          newPromptSubscription.dispose();
        },
      };
    });
  }
}
