import { Logger, NullLogger } from '@gitlab-org/logging';

export const log: Logger = new NullLogger();
