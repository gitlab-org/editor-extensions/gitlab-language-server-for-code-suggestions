import { ApiRequest } from '@gitlab-org/core';

export type GitLabVersionResponse = {
  version: string;
  enterprise?: boolean;
};
export const versionRequest: ApiRequest<GitLabVersionResponse> = {
  type: 'rest',
  method: 'GET',
  path: '/version',
};
