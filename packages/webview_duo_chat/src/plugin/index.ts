import { WebviewPlugin } from '@gitlab-org/webview-plugin';
import { Logger } from '@gitlab-org/logging';
import { Messages, WEBVIEW_ID, WEBVIEW_TITLE } from '../contract';
import { GitLabApiClient } from './types';
import { GitLabChatController } from './chat_controller';
import { ChatPlatformManager } from './chat_platform_manager';
import { GitLabPlatformManagerForChat } from './port/chat/get_platform_manager_for_chat';

type DuoChatPluginFactoryParams = {
  gitlabApiClient: GitLabApiClient;
  logger: Logger;
};

export const duoChatPluginFactory = ({
  gitlabApiClient,
  logger,
}: DuoChatPluginFactoryParams): WebviewPlugin<Messages> => ({
  id: WEBVIEW_ID,
  title: WEBVIEW_TITLE,
  setup: ({ webview, extension }) => {
    const platformManager = new ChatPlatformManager(gitlabApiClient);
    const gitlabPlatformManagerForChat = new GitLabPlatformManagerForChat(platformManager);

    webview.onInstanceConnected((_, webviewMessageBus) => {
      const controller = new GitLabChatController(
        gitlabPlatformManagerForChat,
        webviewMessageBus,
        extension,
        logger,
      );

      const newPromptSubscription = extension.onNotification(
        'newPrompt',
        async ({ prompt, fileContext }) => {
          await controller.prompt(prompt, fileContext);
        },
      );

      return {
        dispose: () => {
          controller.dispose();
          newPromptSubscription.dispose();
        },
      };
    });
  },
});
