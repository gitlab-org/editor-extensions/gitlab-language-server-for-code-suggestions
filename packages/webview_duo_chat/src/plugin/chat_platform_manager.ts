import { ChatPlatformForAccount } from './chat_platform';
import {
  GitLabPlatformForAccount,
  GitLabPlatformForProject,
  GitLabPlatformManager,
} from './port/platform/gitlab_platform';
import { GitLabApiClient } from './types';

export class ChatPlatformManager implements GitLabPlatformManager {
  #client: GitLabApiClient;

  constructor(client: GitLabApiClient) {
    this.#client = client;
  }

  async getForActiveProject(): Promise<GitLabPlatformForProject | undefined> {
    // return new ChatPlatformForProject(this.#client);
    return undefined;
  }

  async getForActiveAccount(): Promise<GitLabPlatformForAccount | undefined> {
    return new ChatPlatformForAccount(this.#client);
  }

  async getForAllAccounts(): Promise<GitLabPlatformForAccount[]> {
    return [new ChatPlatformForAccount(this.#client)];
  }

  async getForSaaSAccount(): Promise<GitLabPlatformForAccount | undefined> {
    return new ChatPlatformForAccount(this.#client);
  }
}
