import {
  GitLabPlatformForAccount,
  GitLabPlatformForProject,
  GitLabPlatformManager,
} from '../platform/gitlab_platform';
import { account, gitlabPlatformForAccount } from '../test_utils/entities';
import { Account } from '../platform/gitlab_account';
import { createFakePartial } from '../test_utils/create_fake_partial';
import { GitLabProject } from '../platform/gitlab_project';
import { getChatSupport, ChatSupportResponseInterface } from './api/get_chat_support';
import {
  GitLabEnvironment,
  GitLabPlatformManagerForChat,
  GITLAB_COM_URL,
  GITLAB_ORG_URL,
  GITLAB_DEVELOPMENT_URL,
  GITLAB_STAGING_URL,
} from './get_platform_manager_for_chat';

// jest.mock('../utils/extension_configuration');
jest.mock('./api/get_chat_support', () => ({
  getChatSupport: jest.fn(),
}));

describe('GitLabPlatformManagerForChat', () => {
  let platformManagerForChat: GitLabPlatformManagerForChat;
  let gitlabPlatformManager: GitLabPlatformManager;

  const buildGitLabPlatformForAccount = (useAccount: Account): GitLabPlatformForAccount => ({
    ...gitlabPlatformForAccount,
    account: useAccount,
  });

  const firstGitlabPlatformForAccount: GitLabPlatformForAccount = buildGitLabPlatformForAccount({
    ...account,
    username: 'first-account',
  });
  const secondGitLabPlatformForAccount: GitLabPlatformForAccount = buildGitLabPlatformForAccount({
    ...account,
    username: 'second-account',
  });

  beforeEach(() => {
    gitlabPlatformManager = createFakePartial<GitLabPlatformManager>({
      getForActiveProject: jest.fn(),
      getForActiveAccount: jest.fn(),
      getForAllAccounts: jest.fn(),
      getForSaaSAccount: jest.fn(),
    });

    platformManagerForChat = new GitLabPlatformManagerForChat(gitlabPlatformManager);
  });

  afterEach(() => {
    // eslint-disable-next-line no-restricted-syntax
    jest.clearAllMocks();
  });

  describe('when no gitlab account is available', () => {
    beforeEach(() => {
      jest.mocked(gitlabPlatformManager.getForAllAccounts).mockResolvedValueOnce([]);
    });

    it('returns undefined', async () => {
      expect(await platformManagerForChat.getGitLabPlatform()).toBe(undefined);
    });
  });

  describe('when a single gitlab account is available', () => {
    let customGitlabPlatformForAccount: GitLabPlatformForAccount;

    beforeEach(() => {
      customGitlabPlatformForAccount = firstGitlabPlatformForAccount;

      jest
        .mocked(gitlabPlatformManager.getForAllAccounts)
        .mockResolvedValueOnce([customGitlabPlatformForAccount]);
    });

    it('returns undefined if the platform for the account does not have chat enabled', async () => {
      jest.mocked(getChatSupport).mockResolvedValue({ hasSupportForChat: false });
      expect(await platformManagerForChat.getGitLabPlatform()).toBeUndefined();
    });

    it('returns gitlab platform for that account if chat is available for the platform', async () => {
      jest
        .mocked(getChatSupport)
        .mockResolvedValue({ hasSupportForChat: true, platform: customGitlabPlatformForAccount });
      expect(await platformManagerForChat.getGitLabPlatform()).toBe(customGitlabPlatformForAccount);
    });
  });

  describe('when multiple gitlab accounts are available', () => {
    const firstPlatformWithChatEnabled: ChatSupportResponseInterface = {
      hasSupportForChat: true,
      platform: firstGitlabPlatformForAccount,
    };
    const secondPlatformWithChatEnabled: ChatSupportResponseInterface = {
      hasSupportForChat: true,
      platform: secondGitLabPlatformForAccount,
    };
    const platformWithoutChatEnabled: ChatSupportResponseInterface = { hasSupportForChat: false };

    beforeEach(() => {
      jest
        .mocked(gitlabPlatformManager.getForAllAccounts)
        .mockResolvedValueOnce([firstGitlabPlatformForAccount, secondGitLabPlatformForAccount]);
    });

    it.each`
      desc                | firstResolve                    | secondResolve                    | expectedPlatform
      ${'the first has'}  | ${firstPlatformWithChatEnabled} | ${platformWithoutChatEnabled}    | ${firstGitlabPlatformForAccount}
      ${'the second has'} | ${platformWithoutChatEnabled}   | ${secondPlatformWithChatEnabled} | ${secondGitLabPlatformForAccount}
      ${'several have'}   | ${firstPlatformWithChatEnabled} | ${secondPlatformWithChatEnabled} | ${firstGitlabPlatformForAccount}
    `(
      'returns the correct gitlab platform when $desc the chat enabled',
      async ({ firstResolve, secondResolve, expectedPlatform }) => {
        jest
          .mocked(getChatSupport)
          .mockResolvedValue(platformWithoutChatEnabled)
          .mockResolvedValueOnce(firstResolve)
          .mockResolvedValueOnce(secondResolve);
        expect(await platformManagerForChat.getGitLabPlatform()).toBe(expectedPlatform);
      },
    );

    it('correctly returns undefined if none of the platforms have chat enabled', async () => {
      jest.mocked(getChatSupport).mockResolvedValue(platformWithoutChatEnabled);
      expect(await platformManagerForChat.getGitLabPlatform()).toBe(undefined);
    });
  });

  describe('when getGitLabEnvironment should return the correct instance', () => {
    let customGitlabPlatformForAccount: GitLabPlatformForAccount;
    beforeEach(() => {
      customGitlabPlatformForAccount = firstGitlabPlatformForAccount;

      jest
        .mocked(gitlabPlatformManager.getForAllAccounts)
        .mockResolvedValueOnce([firstGitlabPlatformForAccount]);
    });

    it('should returns GITLAB_COM for GITLAB_COM_URL instance', async () => {
      customGitlabPlatformForAccount.account.instanceUrl = GITLAB_COM_URL;
      jest
        .mocked(getChatSupport)
        .mockResolvedValue({ hasSupportForChat: true, platform: customGitlabPlatformForAccount });
      expect(await platformManagerForChat.getGitLabEnvironment()).toBe(
        GitLabEnvironment.GITLAB_COM,
      );
    });

    it('should returns GITLAB_ORG for GITLAB_COM_URL instance', async () => {
      customGitlabPlatformForAccount.account.instanceUrl = GITLAB_ORG_URL;
      jest
        .mocked(getChatSupport)
        .mockResolvedValue({ hasSupportForChat: true, platform: customGitlabPlatformForAccount });
      expect(await platformManagerForChat.getGitLabEnvironment()).toBe(
        GitLabEnvironment.GITLAB_ORG,
      );
    });

    it('should returns GITLAB_DEVELOPMENT for GITLAB_DEVELOPMENT_URL instance', async () => {
      customGitlabPlatformForAccount.account.instanceUrl = GITLAB_DEVELOPMENT_URL;
      jest
        .mocked(getChatSupport)
        .mockResolvedValue({ hasSupportForChat: true, platform: customGitlabPlatformForAccount });
      expect(await platformManagerForChat.getGitLabEnvironment()).toBe(
        GitLabEnvironment.GITLAB_DEVELOPMENT,
      );
    });

    it('should returns GITLAB_STAGING for GITLAB_STAGING_URL instance', async () => {
      customGitlabPlatformForAccount.account.instanceUrl = GITLAB_STAGING_URL;
      jest
        .mocked(getChatSupport)
        .mockResolvedValue({ hasSupportForChat: true, platform: customGitlabPlatformForAccount });
      expect(await platformManagerForChat.getGitLabEnvironment()).toBe(
        GitLabEnvironment.GITLAB_STAGING,
      );
    });

    it('should returns GITLAB_STAGING_URL for any other instanceUrl', async () => {
      customGitlabPlatformForAccount.account.instanceUrl = '';
      jest
        .mocked(getChatSupport)
        .mockResolvedValue({ hasSupportForChat: true, platform: customGitlabPlatformForAccount });
      expect(await platformManagerForChat.getGitLabEnvironment()).toBe(
        GitLabEnvironment.GITLAB_SELF_MANAGED,
      );
    });
  });

  describe('getGqlProjectId', () => {
    it('should return a project when it exists', async () => {
      const projectGqlId = 'gid://gitlab/Project/123456';
      const createPartialGitLabPlatformForProject = () =>
        createFakePartial<GitLabPlatformForProject>({
          project: createFakePartial<GitLabProject>({
            gqlId: projectGqlId,
          }),
        });
      jest
        .mocked(gitlabPlatformManager.getForActiveProject)
        .mockResolvedValueOnce(createPartialGitLabPlatformForProject());

      const projectId = await platformManagerForChat.getProjectGqlId();

      expect(projectId).toBe(projectGqlId);
    });

    it(`should return undefined when a project doesn't exist`, async () => {
      jest.mocked(gitlabPlatformManager.getForActiveProject).mockResolvedValueOnce(undefined);

      const projectId = await platformManagerForChat.getProjectGqlId();

      expect(projectId).toBe(undefined);
    });
  });
});
