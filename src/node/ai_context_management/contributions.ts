import { ModuleJavaScriptFilePathResolver } from './module_javascript_file_path_resolver';

export const aiContextManagementContributions = [ModuleJavaScriptFilePathResolver] as const;
