import { readFile, writeFile } from 'fs/promises';
import path from 'path';
import { spawn } from 'child_process';
import { extract } from 'tar';
import { Cable } from '@anycable/core';
import {
  DuoWorkflowStatus,
  DuoWorkflowEvent,
  WorkflowEvent,
  WorkflowType,
} from '@gitlab-org/webview-duo-workflow';
import { Logger } from '@gitlab-org/logging';
import { ConfigService, DefaultConfigService, GitLabAPI } from '../../common';
import { LsFetch } from '../../common/fetch';
import { log } from '../../common/log';
import { createFakePartial } from '../../common/test_utils/create_fake_partial';
import { CurrentOs } from '../../common/os';
import { DEFAULT_DOCKER_IMAGE, DesktopWorkflowRunner } from './desktop_workflow_runner';
import { GenerateTokenResponse } from './executor';
import { WorkflowEventsChannel } from './api/graphql/workflow_events_response_channel';
import { TimeoutError } from './timeout_error';

jest.spyOn(log, 'error').mockImplementation(() => {});

jest.mock('fs/promises');
jest.mock('tar');
jest.mock('child_process');

const mockDockerMakeRequest = jest.fn();
jest.mock('./docker_client', () => ({
  DockerClient: jest.fn(() => ({ makeRequest: mockDockerMakeRequest })),
}));

const createFakeCable = () =>
  createFakePartial<Cable>({
    subscribe: jest.fn(),
    disconnect: jest.fn(),
  });

describe('DesktopWorkflowAPI', () => {
  let api: DesktopWorkflowRunner;
  let configService: ConfigService;
  let cable: Cable;

  const vendorPath = path.join(__dirname, '../vendor');
  const executorPath = path.join(vendorPath, 'duo-workflow-executor-docker-v0.0.6.tar.gz');
  const basePath = path.resolve('/Users/test/projects/LSP');
  const mockReadFile = readFile as jest.Mock;
  const mockWriteFile = writeFile as jest.Mock;
  const mockExtractTar = extract as unknown as jest.Mock;
  const mockSpawn = spawn as jest.Mock;
  const fetch = jest.fn();
  const lsFetch: LsFetch = createFakePartial<LsFetch>({ fetch, updateAgentOptions: jest.fn() });
  const mockFetchFromApi = jest.fn();
  const gitlabAPI = createFakePartial<GitLabAPI>({
    fetchFromApi: mockFetchFromApi,
    connectToCable: async () => cable,
  });
  const currentOs = createFakePartial<CurrentOs>({
    name: 'linux/arm64',
  });
  const mockLogger = createFakePartial<Logger>({
    info: jest.fn(),
    error: jest.fn(),
  });

  const generateTokenResponse: GenerateTokenResponse = {
    gitlab_rails: {
      base_url: 'https://my.gitlab.com',
      token: 'gitlab-rails-token',
    },
    duo_workflow_executor: {
      executor_binary_url: 'https://duo-workflow-executor.example.com',
      executor_binary_urls: {
        'linux/arm64': 'https://duo-workflow-executor.example.com/linux-arm64.tar.gz',
      },
      version: 'v0.0.6',
    },
    duo_workflow_service: {
      base_url: 'workflow-service-url:443',
      token: 'workflow-service-token',
      secure: true,
      headers: {
        'X-Gitlab-Host-Name': 'host-name',
        'X-Gitlab-Instance-Id': 'instance-id',
        'X-Gitlab-Realm': 'realm',
        'X-Gitlab-Version': 'version',
        'X-Gitlab-Global-User-Id': 'global-user-id',
      },
    },
  };

  beforeEach(() => {
    cable = createFakeCable();
    configService = new DefaultConfigService();
    api = new DesktopWorkflowRunner(configService, lsFetch, gitlabAPI, currentOs, mockLogger);
    configService.set('client.token', '123');
    configService.set('client.baseUrl', 'https://my.gitlab.com');
    configService.set('client.duo.workflow', { dockerSocket: '/host', useDocker: true });
    configService.set('client.workspaceFolders', [
      {
        uri: `file://${basePath}`,
        name: 'LSP',
      },
    ]);
    configService.set('client.projectPath', 'mynamespace/myproject');
    configService.set('client.telemetry.enabled', true);

    mockReadFile.mockResolvedValue(Buffer.from('file contents'));
    fetch.mockResolvedValue({
      ok: true,
      arrayBuffer: async () => new Uint8Array([1, 2, 3]).buffer,
    });

    mockFetchFromApi.mockResolvedValue({
      id: 'workflow-id',
    });

    mockFetchFromApi.mockResolvedValue(generateTokenResponse);

    mockReadFile.mockResolvedValue(Buffer.from('file contents'));
    currentOs.name = 'linux/arm64';
  });

  describe('executorCommand', () => {
    it('constructs a valid executor command', () => {
      const command = api.executorCommand('123', generateTokenResponse, 'Say hello');
      expect(command).toEqual([
        '/duo-workflow-executor',
        '--workflow-id=123',
        '--server',
        'workflow-service-url:443',
        '--goal',
        'Say hello',
        '--base-url',
        'https://my.gitlab.com',
        '--token',
        'gitlab-rails-token',
        '--duo-workflow-service-token',
        'workflow-service-token',
        '--realm',
        'realm',
        '--user-id',
        'global-user-id',
        '--instance-id',
        'instance-id',
        '--git-http-base-url',
        'https://my.gitlab.com',
        '--git-http-user',
        'auth',
        '--git-password',
        'gitlab-rails-token',
        '--ignore-git-dir-owners',
        '--telemetry-enabled',
      ]);
    });

    it('includes --insecure when secure: false', () => {
      const tokenResponse = {
        ...generateTokenResponse,
        duo_workflow_service: {
          ...generateTokenResponse.duo_workflow_service,
          secure: false,
        },
      };

      const command = api.executorCommand('123', tokenResponse, 'Say hello');
      expect(command).toContain('--insecure');
    });

    it('includes --debug when logLevel is debug', () => {
      configService.set('client.logLevel', 'debug');
      const command = api.executorCommand('123', generateTokenResponse, 'Say hello');
      expect(command).toContain('--debug');
    });

    it('includes --workflow-metadata when workflow_metadata is set', () => {
      const tokenResponse = {
        ...generateTokenResponse,
        workflow_metadata: { someMetadata: 'some value' },
      };

      const command = api.executorCommand('123', tokenResponse, 'Say hello');
      expect(command).toContain('--workflow-metadata');
      expect(command).toContain('{"someMetadata":"some value"}');
    });

    it('includes the workflow type when passed in', () => {
      const command = api.executorCommand(
        '123',
        generateTokenResponse,
        'Say hello',
        WorkflowType.SEARCH_AND_REPLACE,
      );
      expect(command).toContain('--definition');
      expect(command).toContain(WorkflowType.SEARCH_AND_REPLACE);
    });
  });

  describe('isExecutorPrepared', () => {
    describe('when useDocker is false', () => {
      beforeEach(() => {
        configService.set('client.duo.workflow', { dockerSocket: '/host', useDocker: false });
      });

      it('should return true', async () => {
        const result = await api.isExecutorPrepared();
        expect(result).toBe(true);
      });
    });

    describe('when useDocker is true', () => {
      it('should return true when the image exists', async () => {
        mockDockerMakeRequest.mockResolvedValueOnce(
          JSON.stringify([{ RepoTags: [DEFAULT_DOCKER_IMAGE] }]),
        );

        const result = await api.isExecutorPrepared();

        expect(result).toBe(true);
        expect(mockDockerMakeRequest).toHaveBeenCalledWith('/images/json', 'GET', '');
      });

      it('should return false when the image does not exist', async () => {
        mockDockerMakeRequest.mockResolvedValueOnce(
          JSON.stringify([{ RepoTags: ['non-existing-image:latest'] }]),
        );

        const result = await api.isExecutorPrepared();

        expect(result).toBe(false);
        expect(mockDockerMakeRequest).toHaveBeenCalledWith('/images/json', 'GET', '');
      });
    });
  });

  describe('prepareExecutor', () => {
    it('should pull the specified image', async () => {
      const imageName = DEFAULT_DOCKER_IMAGE;
      mockDockerMakeRequest.mockResolvedValueOnce('');

      await api.prepareExecutor();

      expect(mockDockerMakeRequest).toHaveBeenCalledWith(
        `/images/create?fromImage=${imageName}`,
        'POST',
        '',
        'text/plain',
        300000,
      );
    });

    it('should throw an error if pulling the image fails', async () => {
      mockDockerMakeRequest.mockRejectedValueOnce(new Error('Pull failed'));

      await expect(api.prepareExecutor()).rejects.toThrow('Pull failed');
    });
  });

  describe('runWorkflow', () => {
    describe('when correctly configured', () => {
      beforeEach(() => {
        const existingImage = 'test-image:latest';

        mockDockerMakeRequest.mockImplementation((apiPath: string) => {
          if (apiPath.includes('/containers/create')) {
            return JSON.stringify({ Id: 'created-container-id' });
          }
          if (apiPath.includes('/images/json')) {
            return JSON.stringify([{ RepoTags: [existingImage] }]);
          }
          if (apiPath.includes('/images/create')) {
            return '';
          }
          if (apiPath.includes('/containers/start')) {
            return '';
          }
          if (apiPath.includes('/containers/archive')) {
            return '';
          }
          throw new Error(`Unexpected Docker API call: ${apiPath}`);
        });
      });

      it('runWorkflow should successfully run a workflow', async () => {
        await api.runWorkflow({ goal: 'test-goal', image: 'test-image:latest' });

        expect(mockDockerMakeRequest).toHaveBeenCalledTimes(3);
        expect(mockReadFile).toHaveBeenCalledWith(executorPath);
        expect(mockFetchFromApi).toHaveBeenCalledWith({
          body: {
            project_id: 'mynamespace/myproject',
            goal: 'test-goal',
            workflow_definition: 'software_development',
          },
          method: 'POST',
          path: '/api/v4/ai/duo_workflows/workflows',
          supportedSinceInstanceVersion: {
            resourceName: 'create a workflow',
            version: '17.3.0',
          },
          type: 'rest',
        });
        expect(mockFetchFromApi).toHaveBeenCalledWith({
          method: 'POST',
          path: '/api/v4/ai/duo_workflows/direct_access',
          supportedSinceInstanceVersion: {
            resourceName: 'get workflow direct access',
            version: '17.3.0',
          },
          type: 'rest',
        });
      });

      it('runWorkflow does not download executor file when file is already present', async () => {
        await api.runWorkflow({ goal: 'test-goal', image: 'test-image' });
        expect(mockFetchFromApi).toHaveBeenCalledWith({
          body: {
            project_id: 'mynamespace/myproject',
            goal: 'test-goal',
            workflow_definition: 'software_development',
          },
          method: 'POST',
          path: '/api/v4/ai/duo_workflows/workflows',
          supportedSinceInstanceVersion: {
            resourceName: 'create a workflow',
            version: '17.3.0',
          },
          type: 'rest',
        });
      });

      describe('when receiving existingWorkflowId as an argument', () => {
        beforeEach(async () => {
          await api.runWorkflow({
            goal: 'test-goal',
            image: 'test-image',
            existingWorkflowId: 'existing-workflow-id',
          });
        });

        it('should not create a new workflow', () => {
          // Check that fetchFromApi was not called to create a new workflow
          expect(mockFetchFromApi).not.toHaveBeenCalledWith(
            expect.objectContaining({
              path: '/api/v4/ai/duo_workflows/workflows',
              method: 'POST',
            }),
          );

          // Check that fetchFromApi was called to get the workflow token
          expect(mockFetchFromApi).toHaveBeenCalledWith(
            expect.objectContaining({
              path: '/api/v4/ai/duo_workflows/direct_access',
              method: 'POST',
            }),
          );
        });
      });

      describe('when file is not present', () => {
        beforeEach(() => {
          mockReadFile.mockImplementationOnce(() => {
            throw new Error('File not found');
          });
        });

        it('runWorkflow downloads executor file ', async () => {
          await api.runWorkflow({ goal: 'test-goal', image: 'test-image' });

          expect(fetch).toHaveBeenCalledWith('https://duo-workflow-executor.example.com', {
            method: 'GET',
          });
          expect(mockWriteFile).toHaveBeenCalledWith(executorPath, Buffer.from([1, 2, 3]));
        });
      });

      describe('when a raw binary is used', () => {
        beforeEach(() => {
          configService.set('client.duo.workflow', { dockerSocket: '/host', useDocker: false });
          mockReadFile.mockImplementationOnce(() => {
            throw new Error('File not found');
          });
        });

        it('runWorkflow executes the binary', async () => {
          mockSpawn.mockReturnValue({
            stdout: {
              pipe: () => {},
            },
            stderr: {
              pipe: () => {},
            },
            on: () => {},
          });

          const id = await api.runWorkflow({
            goal: 'test-goal',
            image: 'test-image',
            type: WorkflowType.SEARCH_AND_REPLACE,
          });

          expect(fetch).toHaveBeenCalledWith(
            'https://duo-workflow-executor.example.com/linux-arm64.tar.gz',
            {
              method: 'GET',
            },
          );
          expect(mockExtractTar).toHaveBeenCalledWith({
            file: path.join(vendorPath, 'duo-workflow-executor-shell-v0.0.6.tar.gz'),
            C: vendorPath,
          });
          expect(mockSpawn).toHaveBeenCalledWith(path.join(vendorPath, 'duo-workflow-executor'), [
            `--workflow-id=${id}`,
            '--server',
            'workflow-service-url:443',
            '--goal',
            'test-goal',
            '--base-url',
            'https://my.gitlab.com',
            '--token',
            'gitlab-rails-token',
            '--duo-workflow-service-token',
            'workflow-service-token',
            '--realm',
            'realm',
            '--user-id',
            'global-user-id',
            '--instance-id',
            'instance-id',
            '--git-http-base-url',
            'https://my.gitlab.com',
            '--git-http-user',
            'auth',
            '--git-password',
            'gitlab-rails-token',
            '--ignore-git-dir-owners',
            '--definition',
            'search_and_replace',
            '--telemetry-enabled',
            '--base-path',
            basePath,
          ]);
        });

        it('runWorkflow throws an error for unsupported os', async () => {
          currentOs.name = 'unsupported/os';

          await expect(api.runWorkflow({ goal: 'test-goal', image: 'test-image' })).rejects.toThrow(
            'Executing a workflow with a binary is not supported for this OS/Arch',
          );
        });
      });
    });
  });

  describe('subscribeToUpdates', () => {
    describe('when #cable is already defined', () => {
      it('should disconnect existing cable', async () => {
        await api.subscribeToUpdates(jest.fn(), '123');
        await api.subscribeToUpdates(jest.fn(), '456');

        expect(cable.subscribe).toHaveBeenCalledTimes(2);
        expect(cable.disconnect).toHaveBeenCalled();
      });
    });

    describe('when a #cable is not yet defined', () => {
      let messageCallback: jest.Mock;
      let workflowId: string;

      beforeEach(async () => {
        messageCallback = jest.fn();
        workflowId = 'workflow-id';
        await api.subscribeToUpdates(messageCallback, workflowId);
      });

      it('should not call disconnect #cable', async () => {
        expect(cable.disconnect).not.toHaveBeenCalled();
      });
    });

    it('calls messageCallback on checkpoint', async () => {
      const status = DuoWorkflowStatus.FINISHED;
      const workflowEvent: DuoWorkflowEvent = {
        checkpoint: `{ "channel_values": { "status": "${status}" }}`,
        metadata: 'new metadata',
        workflowStatus: status,
        errors: [],
        workflowGoal: '',
      };
      const workflowId = '1';
      const messageCallback = jest.fn();

      await api.subscribeToUpdates(messageCallback, workflowId);

      expect(cable.subscribe).toHaveBeenCalledWith(expect.any(WorkflowEventsChannel));
      const channel = jest.mocked(cable.subscribe).mock.calls[0][0];

      channel.receive({ result: { data: { workflowEventsUpdated: workflowEvent } }, more: true });

      expect(messageCallback).toHaveBeenCalledWith(workflowEvent);
    });
  });

  describe('sendEvent', () => {
    const expectedParams = {
      body: {
        event_type: 'stop',
        message: '',
      },
      method: 'POST',
      path: '/api/v4/ai/duo_workflows/workflows/1/events',
      supportedSinceInstanceVersion: {
        resourceName: 'create a workflow event',
        version: '17.5.0',
      },
      type: 'rest',
    };

    describe('when message is not passed', () => {
      it('sends the request to events endpoint with a blank message', async () => {
        await api.sendEvent('1', WorkflowEvent.STOP);
        expect(mockFetchFromApi).toHaveBeenCalledWith(expectedParams);
      });
    });

    describe('when message is not passed', () => {
      it('sends the request to events endpoint with the passed in message', async () => {
        await api.sendEvent('1', WorkflowEvent.MESSAGE, {
          correlation_id: 'id',
          message: 'test message',
        });

        expect(mockFetchFromApi).toHaveBeenCalledWith({
          ...expectedParams,
          body: {
            event_type: 'message',
            correlation_id: 'id',
            message: 'test message',
          },
        });
      });
    });
  });

  describe('getGraphqlData', () => {
    const testQuery = 'query { property { value } }';

    describe('when there is data to return', () => {
      beforeEach(() => {
        mockFetchFromApi.mockResolvedValueOnce({
          data: {
            property: {
              value: 'nestedValue',
            },
          },
        });
      });

      it('calls fetchFromApi with the right data', async () => {
        await api.getGraphqlData({
          query: testQuery,
          variables: { userId: 1 },
          supportedSinceInstanceVersion: {
            version: '17.7',
            resourceName: 'test',
          },
        });

        expect(gitlabAPI.fetchFromApi).toHaveBeenCalledWith({
          supportedSinceInstanceVersion: { version: '17.7', resourceName: 'test' },
          query: testQuery,
          variables: { userId: 1 },
          type: 'graphql',
        });
      });
    });

    describe('when there is no data', () => {
      beforeEach(() => {
        mockFetchFromApi.mockResolvedValueOnce({
          data: {
            property: null,
          },
        });
      });

      it('returns resolved value', async () => {
        const result = await api.getGraphqlData({
          query: testQuery,
          variables: { userId: 1 },
        });

        expect(result).toEqual({
          data: {
            property: null,
          },
        });
      });
    });

    describe('when there is an error', () => {
      beforeEach(() => {
        mockFetchFromApi.mockRejectedValue(new Error('test error'));
      });

      it('throws and logs the error', async () => {
        const error = new Error('test error');
        await expect(
          api.getGraphqlData({
            query: testQuery,
            variables: { userId: 1 },
          }),
        ).rejects.toThrow(error);

        expect(mockLogger.error).toHaveBeenCalledWith(error);
      });
    });
  });

  describe('watchWorkflowExecutor', () => {
    it('continues to wait for execution to end when a timeout is received', async () => {
      const waitHandler = jest
        .fn()
        .mockRejectedValueOnce(new TimeoutError())
        .mockResolvedValueOnce(JSON.stringify({ StatusCode: 0 }));
      mockDockerMakeRequest.mockImplementation((apiPath: string) => {
        if (apiPath.includes('/create')) {
          return JSON.stringify({ Id: 'container-id' });
        }
        if (apiPath.includes('/start')) {
          return '';
        }
        if (apiPath.includes('/archive')) {
          return '';
        }
        if (apiPath.includes('/wait')) {
          return waitHandler();
        }
        throw new Error(`Unexpected Docker API call: ${apiPath}`);
      });

      await api.runWorkflow({ goal: 'test', image: 'docker-test' });
      mockDockerMakeRequest.mockClear();
      await api.watchWorkflowExecutor();

      expect(mockDockerMakeRequest).toHaveBeenCalledTimes(2);
      expect(mockDockerMakeRequest).toHaveBeenLastCalledWith(
        `/containers/container-id/wait`,
        'POST',
        '',
        'application/json',
        300000,
      );
    });
  });
});
