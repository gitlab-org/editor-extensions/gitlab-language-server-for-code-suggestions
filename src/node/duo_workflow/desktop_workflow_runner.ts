import { readFile, writeFile } from 'fs/promises';
import path from 'path';
import { fileURLToPath } from 'url';
import { WorkspaceFolder } from 'vscode-languageserver-protocol';
import { WorkflowAPI, WorkflowGraphqlPayload } from '@gitlab-lsp/workflow-api';
import {
  DuoWorkflowEvent,
  WorkflowEvent,
  DuoWorkflowStatusUpdate,
  DuoWorkflowStatusUpdateResponse,
  WorkflowType,
  DuoWorkflowMessage,
  RunWorkflowPayload,
} from '@gitlab-org/webview-duo-workflow';
import { withPrefix, Logger } from '@gitlab-org/logging';
import { Injectable } from '@gitlab/needle';
import { Cable } from '@anycable/core';
import { ConfigService, IConfig } from '../../common';
import { LsFetch } from '../../common/fetch';
import { CurrentOs } from '../../common/os';
import { GitLabApiClient } from '../../common/api';
import { LogLevel, LOG_LEVEL } from '../../common/log_types';
import { WorkflowEventsChannel } from './api/graphql/workflow_events_response_channel';
import { DockerClient } from './docker_client';
import { Executor, GenerateTokenResponse } from './executor';
import { DockerExecutor } from './docker_executor';
import { ShellExecutor } from './shell_executor';

interface CreateWorkflowResponse {
  id: string;
}

interface CreateWorkflowEventResponse {
  id: string;
  event_type: string;
  event_status: string;
  message: string;
}

export const DEFAULT_DOCKER_IMAGE =
  'registry.gitlab.com/gitlab-org/duo-workflow/default-docker-image/workflow-generic-image:v0.0.4';

@Injectable(WorkflowAPI, [ConfigService, LsFetch, GitLabApiClient, CurrentOs, Logger])
// FIXME: this class should either be called DesktopWorkflowAPI or the interface should be called WorkflowRunner
export class DesktopWorkflowRunner implements WorkflowAPI {
  #cable: Cable | undefined;

  #folders?: WorkspaceFolder[];

  #fetch: LsFetch;

  #api: GitLabApiClient;

  #projectPath: string | undefined;

  #logLevel: LogLevel | undefined;

  #telemetryEnabled: boolean;

  #currentOs: CurrentOs;

  #executor: Executor;

  #prefixedLogger: Logger;

  #logger: Logger;

  constructor(
    configService: ConfigService,
    fetch: LsFetch,
    api: GitLabApiClient,
    currentOs: CurrentOs,
    logger: Logger,
  ) {
    this.#fetch = fetch;
    this.#api = api;
    this.#telemetryEnabled = configService.get('client.telemetry.enabled') ?? true;
    this.#currentOs = currentOs;
    this.#executor = new ShellExecutor(currentOs, logger);
    this.#logger = logger;
    this.#prefixedLogger = withPrefix(logger, '[Duo Workflow Runner]');
    configService.onConfigChange((config) => this.#reconfigure(config));
  }

  #reconfigure(config: IConfig) {
    const dockerSocket = config.client.duo?.workflow?.dockerSocket;
    const useDocker = config.client.duo?.workflow?.useDocker ?? false;

    if (dockerSocket && useDocker) {
      const dockerClient = new DockerClient(dockerSocket);
      this.#executor = new DockerExecutor(dockerClient, DEFAULT_DOCKER_IMAGE, this.#logger);
    } else {
      this.#executor = new ShellExecutor(this.#currentOs, this.#logger);
    }
    this.#folders = config.client.workspaceFolders || [];
    this.#projectPath = config.client.projectPath;
    this.#logLevel = config.client.logLevel;
    const telemetryEnabled = config.client.telemetry?.enabled;
    if (typeof telemetryEnabled !== 'undefined' && this.#telemetryEnabled !== telemetryEnabled) {
      this.#telemetryEnabled = telemetryEnabled;
    }
  }

  getProjectPath() {
    return this.#projectPath || '';
  }

  async subscribeToUpdates(
    messageCallback: (message: DuoWorkflowEvent) => void,
    workflowId: string,
  ) {
    if (this.#cable) {
      this.disconnectCable();
    }

    const channel = new WorkflowEventsChannel({
      workflowId: `gid://gitlab/Ai::DuoWorkflows::Workflow/${workflowId}`,
    });

    this.#cable = await this.#api.connectToCable();

    channel.on('checkpoint', async (msg) => {
      await messageCallback(msg);
    });

    this.#cable.subscribe(channel);
  }

  disconnectCable() {
    if (this.#cable) {
      this.#cable.disconnect();
    }
  }

  async runWorkflow({
    goal,
    image,
    type,
    existingWorkflowId,
  }: RunWorkflowPayload): Promise<string> {
    if (!this.#folders || this.#folders.length === 0) {
      throw new Error('No workspace folders');
    }

    const folderName = fileURLToPath(this.#folders[0].uri);

    if (this.#folders.length > 0) {
      this.#prefixedLogger.info(
        `More than one workspace folder detected. Using workspace folder ${folderName}`,
      );
    }

    let workflowId = existingWorkflowId;
    if (!workflowId) {
      workflowId = await this.#createWorkflow(goal, type);
    }
    const workflowToken = await this.#getWorkflowToken();

    const executorVersion = workflowToken.duo_workflow_executor.version;
    const executorPath = path.join(
      __dirname,
      '../vendor',
      `duo-workflow-executor-${this.#executor.executorType()}-${executorVersion}.tar.gz`,
    );

    await this.#downloadWorkflowExecutor(this.#executor.executorUrl(workflowToken), executorPath);

    const executorCommand = this.executorCommand(workflowId, workflowToken, goal, type);

    await this.#executor.runWorkflow({ executorPath, folderName, executorCommand, image });

    return workflowId;
  }

  async watchWorkflowExecutor(): Promise<{ StatusCode: number }> {
    return this.#executor.watchWorkflowExecutor().then((status) => ({ StatusCode: status }));
  }

  async getGraphqlData({
    query,
    variables,
    supportedSinceInstanceVersion,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
  }: WorkflowGraphqlPayload): Promise<any> {
    try {
      return await this.#api?.fetchFromApi({
        type: 'graphql',
        query,
        variables: variables || {},
        supportedSinceInstanceVersion,
      });
    } catch (e) {
      this.#prefixedLogger.info(`Graphql fetch failed:`, e);

      const error = e as Error;

      this.#prefixedLogger.error(error);

      throw new Error(error.message);
    }
  }

  async updateStatus({ workflowId, statusEvent }: DuoWorkflowStatusUpdate) {
    return this.#api?.fetchFromApi<DuoWorkflowStatusUpdateResponse>({
      type: 'rest',
      path: `/api/v4/ai/duo_workflows/workflows/${workflowId}`,
      method: 'PATCH',
      body: {
        status_event: statusEvent,
      },
    });
  }

  async sendEvent(workflowID: string, eventType: WorkflowEvent, message?: DuoWorkflowMessage) {
    const response = await this.#api?.fetchFromApi<CreateWorkflowEventResponse>({
      type: 'rest',
      method: 'POST',
      path: `/api/v4/ai/duo_workflows/workflows/${workflowID}/events`,
      body: {
        event_type: eventType,
        ...(message || { message: '' }),
      },
      supportedSinceInstanceVersion: {
        resourceName: 'create a workflow event',
        version: '17.5.0',
      },
    });

    if (!response) {
      throw new Error('Failed to create event');
    }
  }

  async isExecutorPrepared(): Promise<boolean> {
    return this.#executor.isExecutorReady();
  }

  async prepareExecutor(): Promise<void> {
    await this.#executor.prepareExecutor();
  }

  async #downloadWorkflowExecutor(executorBinaryUrl: string, executorPath: string): Promise<void> {
    try {
      await readFile(executorPath);
      this.#prefixedLogger.info(`Found existing executor at ${executorPath}.`);
    } catch (error) {
      this.#prefixedLogger.info(
        `Downloading workflow executor from ${executorBinaryUrl} to ${executorPath}...`,
      );
      await this.#downloadFile(executorBinaryUrl, executorPath);
    }
  }

  async #downloadFile(url: string, outputPath: string): Promise<void> {
    const response = await this.#fetch.fetch(url, {
      method: 'GET',
    });

    if (!response.ok) {
      throw new Error(`Failed to download file: ${response.statusText}`);
    }

    const buffer = await response.arrayBuffer();
    await writeFile(outputPath, Buffer.from(buffer));
  }

  async #createWorkflow(goal: string, type?: WorkflowType): Promise<string> {
    try {
      const response = await this.#api?.fetchFromApi<CreateWorkflowResponse>({
        type: 'rest',
        method: 'POST',
        path: '/api/v4/ai/duo_workflows/workflows',
        body: {
          project_id: this.#projectPath,
          goal,
          workflow_definition: type || WorkflowType.SOFTWARE_DEVELOPMENT,
        },
        supportedSinceInstanceVersion: {
          resourceName: 'create a workflow',
          version: '17.3.0',
        },
      });

      return response.id;
    } catch (e) {
      const error = e as Error;
      this.#prefixedLogger.error('Failed to create the workflow', error);
      throw e;
    }
  }

  async #getWorkflowToken(): Promise<GenerateTokenResponse> {
    try {
      const token = await this.#api?.fetchFromApi<GenerateTokenResponse>({
        type: 'rest',
        method: 'POST',
        path: '/api/v4/ai/duo_workflows/direct_access',
        supportedSinceInstanceVersion: {
          resourceName: 'get workflow direct access',
          version: '17.3.0',
        },
      });

      return token;
    } catch (e) {
      const error = e as Error;
      this.#prefixedLogger.error('Failed to fetch the workflow token', error);
      throw e;
    }
  }

  executorCommand(
    workflowId: string,
    workflowToken: GenerateTokenResponse,
    goal: string,
    type?: WorkflowType,
  ) {
    this.#prefixedLogger.info(`Executor command: type: ${type}`);
    this.#prefixedLogger.info(`Received goal ${goal}`);

    return [
      '/duo-workflow-executor',
      `--workflow-id=${workflowId}`,
      '--server',
      workflowToken.duo_workflow_service.base_url || 'localhost:50052',
      '--goal',
      goal,
      '--base-url',
      workflowToken.gitlab_rails.base_url,
      '--token',
      workflowToken.gitlab_rails.token,
      '--duo-workflow-service-token',
      workflowToken.duo_workflow_service.token,
      '--realm',
      workflowToken.duo_workflow_service.headers['X-Gitlab-Realm'],
      '--user-id',
      workflowToken.duo_workflow_service.headers['X-Gitlab-Global-User-Id'],
      '--instance-id',
      workflowToken.duo_workflow_service.headers['X-Gitlab-Instance-Id'],
      '--git-http-base-url',
      workflowToken.gitlab_rails.base_url,
      '--git-http-user',
      'auth',
      '--git-password',
      workflowToken.gitlab_rails.token,
      '--ignore-git-dir-owners',
    ]
      .concat(
        this.#optional(Boolean(type), ['--definition', type || WorkflowType.SOFTWARE_DEVELOPMENT]),
      )
      .concat(this.#optional(!workflowToken.duo_workflow_service.secure, ['--insecure']))
      .concat(this.#optional(this.#logLevel === LOG_LEVEL.DEBUG, ['--debug']))
      .concat(this.#optional(this.#telemetryEnabled, ['--telemetry-enabled']))
      .concat(
        this.#optional(Boolean(workflowToken.workflow_metadata), [
          '--workflow-metadata',
          JSON.stringify(workflowToken.workflow_metadata),
        ]),
      );
  }

  #optional(predicate: boolean, values: string[]) {
    return predicate ? values : [];
  }
}
