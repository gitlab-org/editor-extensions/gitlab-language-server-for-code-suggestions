export interface GenerateTokenResponse {
  gitlab_rails: {
    base_url: string;
    token: string;
  };
  duo_workflow_executor: {
    executor_binary_url: string;
    executor_binary_urls: Record<string, string>;
    version: string;
  };
  duo_workflow_service: {
    base_url: string;
    token: string;
    secure: boolean;
    headers: {
      'X-Gitlab-Host-Name': string;
      'X-Gitlab-Instance-Id': string;
      'X-Gitlab-Realm': string;
      'X-Gitlab-Version': string;
      'X-Gitlab-Global-User-Id': string;
    };
  };
  workflow_metadata?: object;
}

export type RunWorkflowOptions = {
  executorPath: string;
  executorCommand: string[];
  image: string;
  folderName: string;
};

export interface Executor {
  executorType(): string;
  runWorkflow(opts: RunWorkflowOptions): Promise<void>;
  watchWorkflowExecutor(): Promise<number>;
  executorUrl(token: GenerateTokenResponse): string;
  isExecutorReady(): Promise<boolean>;
  prepareExecutor(): Promise<void>;
}
