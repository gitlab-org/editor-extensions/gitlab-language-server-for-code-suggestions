import { readFile } from 'fs/promises';
import { Logger, withPrefix } from '@gitlab-org/logging';
import { DockerClient } from './docker_client';
import { TimeoutError } from './timeout_error';
import { Executor, RunWorkflowOptions, GenerateTokenResponse } from './executor';

export class DockerExecutor implements Executor {
  #dockerClient: DockerClient;

  #logger: Logger;

  #image: string;

  #container: { Id: string } | null = null;

  constructor(dockerClient: DockerClient, image: string, logger: Logger) {
    this.#dockerClient = dockerClient;
    this.#logger = withPrefix(logger, '[Docker Executor]');
    this.#image = image;
  }

  executorType(): string {
    return 'docker';
  }

  executorUrl(token: GenerateTokenResponse) {
    return token.duo_workflow_executor.executor_binary_url;
  }

  async runWorkflow({
    executorPath,
    executorCommand,
    folderName,
  }: RunWorkflowOptions): Promise<void> {
    if (!this.#dockerClient) {
      throw new Error('Docker socket not configured');
    }
    const containerOptions = {
      Image: this.#image,
      Cmd: [...executorCommand, '--base-path', '/workspace'],
      HostConfig: {
        Binds: [`${folderName}:/workspace`],
      },
    };

    // Create a container
    this.#container = JSON.parse(
      await this.#dockerClient.makeRequest(
        '/containers/create',
        'POST',
        JSON.stringify(containerOptions),
      ),
    );

    // Check if Id in createResponse
    if (!this.#container?.Id) {
      throw new Error('Failed to create container: No Id in response');
    }

    const containerID = this.#container.Id;

    this.#logger.info(`Created the docker container: ${containerID}`);

    // Copy the executor into the container
    await this.#copyExecutor(containerID, executorPath);

    // Start the container
    await this.#dockerClient.makeRequest(`/containers/${containerID}/start`, 'POST', '');
    this.#logger.info(`Started the docker container: ${containerID}`);
  }

  async #copyExecutor(containerID: string, executorPath: string) {
    if (!this.#dockerClient) {
      throw new Error('Docker client not initialized');
    }

    const fileContents = await readFile(executorPath);
    await this.#dockerClient.makeRequest(
      `/containers/${containerID}/archive?path=/`,
      'PUT',
      fileContents,
      'application/x-tar',
    );
  }

  async isExecutorReady(): Promise<boolean> {
    if (!this.#dockerClient) {
      throw new Error('Docker socket not configured');
    }

    const imagesJson = await this.#dockerClient.makeRequest('/images/json', 'GET', '');
    const images = JSON.parse(imagesJson);

    return images.some((img: { RepoTags: string[] }) => img.RepoTags?.includes(this.#image));
  }

  async prepareExecutor() {
    if (!this.#dockerClient) {
      throw new Error('Docker socket not configured');
    }

    this.#logger.info(`Pulling image ${this.#image}`);

    await this.#dockerClient.makeRequest(
      `/images/create?fromImage=${this.#image}`,
      'POST',
      '',
      'text/plain',
      300000,
    );
  }

  async watchWorkflowExecutor(): Promise<number> {
    if (!this.#dockerClient) {
      throw new Error('Docker socket not configured');
    }

    if (!this.#container?.Id) {
      throw new Error('No currently running container');
    }

    this.#logger.info(`Watching executor container ${this.#container?.Id}`);

    try {
      const response = await this.#dockerClient.makeRequest(
        `/containers/${this.#container?.Id}/wait`,
        'POST',
        '',
        'application/json',
        300000,
      );

      const res = JSON.parse(response);

      this.#logger.info(`Container exit code: ${res.StatusCode}`);

      return res.StatusCode;
    } catch (e) {
      if (e instanceof TimeoutError) {
        return this.watchWorkflowExecutor();
      }

      this.#logger.error(`Container watch error ${e}`);
      return 1;
    }
  }
}
