import { spawn } from 'child_process';
import { dirname, join } from 'path';
import { extract } from 'tar';
import { Logger, withPrefix } from '@gitlab-org/logging';
import { CurrentOs } from '../../common/os';
import { Executor, RunWorkflowOptions, GenerateTokenResponse } from './executor';

export class ShellExecutor implements Executor {
  #currentOs: CurrentOs;

  #workflowExecutorStatusCode: Promise<number>;

  #logger: Logger;

  constructor(currentOs: CurrentOs, logger: Logger) {
    this.#workflowExecutorStatusCode = Promise.resolve(1);
    this.#currentOs = currentOs;
    this.#logger = withPrefix(logger, '[Workflow Shell Executor]');
  }

  executorType(): string {
    return 'shell';
  }

  isExecutorReady(): Promise<boolean> {
    return Promise.resolve(true);
  }

  prepareExecutor(): Promise<void> {
    return Promise.resolve();
  }

  watchWorkflowExecutor(): Promise<number> {
    return this.#workflowExecutorStatusCode;
  }

  executorUrl(token: GenerateTokenResponse) {
    const osName = this.#currentOs.name || '';
    const binaryUrls = token.duo_workflow_executor.executor_binary_urls;

    if (!osName || !binaryUrls[osName]) {
      throw new Error('Executing a workflow with a binary is not supported for this OS/Arch');
    }
    return binaryUrls[osName];
  }

  async runWorkflow({
    executorPath,
    executorCommand,
    folderName,
  }: RunWorkflowOptions): Promise<void> {
    const executorDir = dirname(executorPath);
    const [binary, ...args] = executorCommand;
    const binaryPath = join(executorDir, binary);

    try {
      await extract({ file: executorPath, C: executorDir });
      this.#logger.info(`Extracted a binary into ${executorDir}`);
    } catch (e) {
      this.#logger.info(`Failed to extract:`, e);

      const error = e as Error;

      this.#logger.error(error);

      throw error;
    }

    const childProcess = spawn(binaryPath, [...args, '--base-path', folderName]);

    this.#logger.info(`Execution started`);

    childProcess.stdout.pipe(process.stdout);
    childProcess.stderr.pipe(process.stderr);

    this.#workflowExecutorStatusCode = new Promise((resolve, reject) => {
      childProcess.on('error', (error) => {
        this.#logger.error(`Failed to execute binary: ${error}`);
        reject(error);
      });

      childProcess.on('close', (code) => {
        this.#logger.info(`Closed with code: ${code}`);
        resolve(code || 0);
      });
    });
  }
}
