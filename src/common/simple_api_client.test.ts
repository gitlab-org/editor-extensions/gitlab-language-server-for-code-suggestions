import { ApiRequest, GetRequest, GraphQLRequest } from '@gitlab-org/core';
import { createFakeResponse } from './test_utils/create_fake_response';
import { DefaultSimpleApiClient } from './simple_api_client';
import { LsFetch } from './fetch';
import { ClientInfo } from './tracking/code_suggestions/code_suggestions_tracking_types';
import { createFakePartial } from './test_utils/create_fake_partial';
import { handleFetchError } from './handle_fetch_error';
import { getLanguageServerVersion } from './utils/get_language_server_version';

jest.mock('./handle_fetch_error');

describe('SimpleApiClient', () => {
  const lsFetch: LsFetch = createFakePartial<LsFetch>({
    get: jest.fn(),
    post: jest.fn(),
    patch: jest.fn(),
  });

  const clientInfo: ClientInfo = {
    name: 'TestClient',
    version: '1.0.0',
  };

  const baseUrl = 'https://gitlab.com';
  const token = 'test-token';
  let client: DefaultSimpleApiClient;

  beforeEach(() => {
    client = new DefaultSimpleApiClient(lsFetch, clientInfo, baseUrl, token);
  });

  describe('getDefaultHeaders', () => {
    it('returns correct default headers', () => {
      const headers = client.getDefaultHeaders();

      expect(headers).toEqual({
        Authorization: `Bearer ${token}`,
        'User-Agent': `gitlab-language-server:${getLanguageServerVersion()} (${clientInfo.name}:${clientInfo.version})`,
        'X-Gitlab-Language-Server-Version': getLanguageServerVersion(),
      });
    });
  });

  describe('fetchFromApi', () => {
    const mockResponse = { data: 'test' };
    const fakeResponse = createFakeResponse({ json: mockResponse });

    beforeEach(() => {
      jest.resetAllMocks();
    });

    it('makes a GraphQL request', async () => {
      const testRequest: GraphQLRequest<unknown> = {
        type: 'graphql',
        query: 'query {test}',
        variables: {
          var: 'test',
        },
      };
      jest.mocked(lsFetch.post).mockResolvedValue(
        createFakeResponse({
          headers: { 'Content-Type': 'application/json' },
          text: '{ "data": {} }',
        }),
      );

      await client.fetchFromApi(testRequest);

      expect(lsFetch.post).toHaveBeenCalledWith(
        'https://gitlab.com/api/graphql',
        expect.objectContaining({
          headers: {
            'Content-Type': 'application/json',
            ...client.getDefaultHeaders(),
          },
          body: '{"query":"query {test}","variables":{"var":"test"}}',
        }),
      );
    });

    it('makes a GET request with correct URL and headers', async () => {
      jest.mocked(lsFetch.get).mockResolvedValue(fakeResponse);
      const testRequest: GetRequest<unknown> = {
        type: 'rest',
        method: 'GET',
        path: '/api/v4/test',
      };

      const request: ApiRequest<unknown> = {
        ...testRequest,
        searchParams: { param1: 'value1', param2: 'value2' },
      };

      await client.fetchFromApi(request);

      expect(lsFetch.get).toHaveBeenCalledWith(
        'https://gitlab.com/api/v4/test?param1=value1&param2=value2',
        expect.objectContaining({
          headers: client.getDefaultHeaders(),
        }),
      );
    });

    it('makes a POST request with correct URL and body', async () => {
      jest.mocked(lsFetch.post).mockResolvedValue(fakeResponse);
      const request: ApiRequest<unknown> = {
        type: 'rest',
        method: 'POST',
        path: '/api/v4/test',
        body: { data: 'test-data' },
      };

      await client.fetchFromApi(request);

      expect(lsFetch.post).toHaveBeenCalledWith(
        'https://gitlab.com/api/v4/test',
        expect.objectContaining({
          headers: {
            'Content-Type': 'application/json',
            ...client.getDefaultHeaders(),
          },
          body: JSON.stringify({ data: 'test-data' }),
        }),
      );
    });

    it('makes a PATCH request with correct URL and body', async () => {
      jest.mocked(lsFetch.patch).mockResolvedValue(fakeResponse);
      const request: ApiRequest<unknown> = {
        type: 'rest',
        method: 'PATCH',
        path: '/api/v4/test',
        body: { data: 'test-data' },
      };

      await client.fetchFromApi(request);

      expect(lsFetch.patch).toHaveBeenCalledWith(
        'https://gitlab.com/api/v4/test',
        expect.objectContaining({
          headers: {
            'Content-Type': 'application/json',
            ...client.getDefaultHeaders(),
          },
          body: JSON.stringify({ data: 'test-data' }),
        }),
      );
    });

    describe.each([
      { method: 'GET', fetchFn: 'get' },
      { method: 'POST', fetchFn: 'post' },
      { method: 'PATCH', fetchFn: 'patch' },
    ] as const)('$method requests', ({ method, fetchFn }) => {
      let request: ApiRequest<unknown>;
      beforeEach(() => {
        jest
          .mocked(lsFetch[fetchFn])
          .mockResolvedValue(createFakeResponse({ json: { data: 'test' } }));
        request = {
          type: 'rest',
          method: method as 'GET' | 'POST' | 'PATCH',
          path: '/api/v4/test',
        };
      });

      it('uses handleFetchError with the correct resource name', async () => {
        const errorResponse = createFakeResponse({ status: 404 });
        jest.mocked(lsFetch[fetchFn]).mockResolvedValue(errorResponse);
        jest.mocked(handleFetchError).mockRejectedValue(new Error('test error'));

        await expect(client.fetchFromApi(request)).rejects.toThrow();
        expect(handleFetchError).toHaveBeenCalledWith(errorResponse, 'test');
      });

      it('merges custom headers with default headers', async () => {
        const customHeaders = { 'Custom-Header': 'value' };

        await client.fetchFromApi({ ...(request as GetRequest<unknown>), headers: customHeaders });

        const expectedHeaders = {
          ...client.getDefaultHeaders(),
          ...customHeaders,
          ...(method !== 'GET' ? { 'Content-Type': 'application/json' } : {}),
        };

        expect(lsFetch[fetchFn]).toHaveBeenCalledWith(
          'https://gitlab.com/api/v4/test',
          expect.objectContaining({
            headers: expectedHeaders,
          }),
        );
      });

      it('passes abort signal to fetch request', async () => {
        const abortSignal = new AbortController().signal;

        await client.fetchFromApi({ ...request, signal: abortSignal });

        expect(lsFetch[fetchFn as keyof LsFetch]).toHaveBeenCalledWith(
          expect.any(String),
          expect.objectContaining({
            signal: abortSignal,
          }),
        );
      });
    });
  });
});
