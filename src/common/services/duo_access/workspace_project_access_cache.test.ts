import { WorkspaceFolder } from 'vscode-languageserver-protocol';
import { GitLabApiClient } from '../../api';
import { createFakePartial } from '../../test_utils/create_fake_partial';
import { createMockFsClient } from '../fs/fs.test_utils';
import { fsPathToUri } from '../fs/utils';
import {
  VirtualFileSystemService,
  VirtualFileSystemEvents,
} from '../fs/virtual_file_system_service';
import { ConfigService } from '../../config_service';
import {
  DefaultDuoWorkspaceProjectAccessCache,
  GqlProjectWithDuoEnabledInfo,
} from './workspace_project_access_cache';

jest.mock('../../api');
jest.useFakeTimers();

describe('DefaultDuoWorkspaceProjectAccessCache', () => {
  const virtualFileSystemService = createFakePartial<VirtualFileSystemService>({
    onFileSystemEvent: jest.fn(),
  });
  const configService = createFakePartial<ConfigService>({
    get: jest.fn().mockReturnValue('https://gitlab.com'),
  });

  const mockFsClient = createMockFsClient();

  const api = createFakePartial<GitLabApiClient>({
    fetchFromApi: jest.fn(),
  });

  const sampleGitConfig =
    '[remote "origin"]\n\turl = https://gitlab.com/gitlab-org/gitlab-development-kit.git\n\tfetch = +refs/heads/*:refs/remotes/origin/*\n';

  describe('VirtualFileSystemService events', () => {
    it('should update the cache with duo projects for each workspace folder and emit event', async () => {
      const workspaceFolder1: WorkspaceFolder = {
        uri: 'file:///path/to/workspace1',
        name: 'Workspace 1',
      };
      const workspaceFolder2: WorkspaceFolder = {
        uri: 'file:///path/to/workspace2',
        name: 'Workspace 2',
      };

      jest.mocked(mockFsClient.promises.readFile).mockResolvedValue(sampleGitConfig);

      jest.mocked(api.fetchFromApi).mockResolvedValue({
        project: { duoFeaturesEnabled: true },
      } as { project: GqlProjectWithDuoEnabledInfo });

      const cache = new DefaultDuoWorkspaceProjectAccessCache(
        mockFsClient,
        api,
        virtualFileSystemService,
        configService,
      );
      const listener = jest.fn();
      cache.onDuoProjectCacheUpdate(listener);

      const [eventHandler] = jest.mocked(virtualFileSystemService.onFileSystemEvent).mock.calls[0];

      eventHandler(VirtualFileSystemEvents.WorkspaceFilesEvent, {
        workspaceFolder: workspaceFolder1,
        files: [
          fsPathToUri('/path/to/workspace1/project1/.git/config'),
          fsPathToUri('/path/to/workspace1/project2/.git/config'),
        ],
      });
      await jest.runAllTimersAsync();

      eventHandler(VirtualFileSystemEvents.WorkspaceFilesEvent, {
        workspaceFolder: workspaceFolder2,
        files: [fsPathToUri('/path/to/workspace2/project3/.git/config')],
      });
      await jest.runAllTimersAsync();

      expect(cache.getProjectsForWorkspaceFolder(workspaceFolder1)).toHaveLength(2);
      expect(cache.getProjectsForWorkspaceFolder(workspaceFolder2)).toHaveLength(1);

      const emittedCache = new Map();
      emittedCache.set(workspaceFolder1.uri, cache.getProjectsForWorkspaceFolder(workspaceFolder1));
      emittedCache.set(workspaceFolder2.uri, cache.getProjectsForWorkspaceFolder(workspaceFolder2));
      expect(listener).toHaveBeenCalledWith(emittedCache);
    });

    it('should have enabled set to false if the project does not have duo features enabled', async () => {
      const workspaceFolder: WorkspaceFolder = {
        uri: 'file:///path/to/workspace',
        name: 'Workspace',
      };

      jest.mocked(mockFsClient.promises.readFile).mockResolvedValue(sampleGitConfig);

      jest.mocked(api.fetchFromApi).mockResolvedValue({
        project: { duoFeaturesEnabled: false },
      } as { project: GqlProjectWithDuoEnabledInfo });

      const cache = new DefaultDuoWorkspaceProjectAccessCache(
        mockFsClient,
        api,
        virtualFileSystemService,
        configService,
      );

      const [eventHandler] = jest.mocked(virtualFileSystemService.onFileSystemEvent).mock.calls[0];

      eventHandler(VirtualFileSystemEvents.WorkspaceFilesEvent, {
        workspaceFolder,
        files: [fsPathToUri('/path/to/workspace/project1/.git/config')],
      });
      await jest.runAllTimersAsync();

      const projects = cache.getProjectsForWorkspaceFolder(workspaceFolder);

      expect(projects).toHaveLength(1);
      expect(projects[0]).toMatchObject({
        enabled: false,
      });
    });

    it('should handle errors and continue updating the cache', async () => {
      const workspaceFolder: WorkspaceFolder = {
        uri: 'file:///path/to/workspace',
        name: 'Workspace',
      };

      jest
        .mocked(mockFsClient.promises.readFile)
        .mockRejectedValueOnce(new Error('Read file error'))
        .mockResolvedValueOnce(sampleGitConfig);

      jest.mocked(api.fetchFromApi).mockResolvedValue({
        project: { duoFeaturesEnabled: true },
      } as { project: GqlProjectWithDuoEnabledInfo });

      const cache = new DefaultDuoWorkspaceProjectAccessCache(
        mockFsClient,
        api,
        virtualFileSystemService,
        configService,
      );

      const [eventHandler] = jest.mocked(virtualFileSystemService.onFileSystemEvent).mock.calls[0];

      eventHandler(VirtualFileSystemEvents.WorkspaceFilesEvent, {
        workspaceFolder,
        files: [
          fsPathToUri('/path/to/workspace/project1/.git/config'),
          fsPathToUri('/path/to/workspace/project2/.git/config'),
        ],
      });
      await jest.runAllTimersAsync();

      const projects = cache.getProjectsForWorkspaceFolder(workspaceFolder);
      expect(projects).toHaveLength(1);
      expect(projects[0]).toMatchObject({
        projectPath: 'gitlab-development-kit',
        enabled: true,
      });
    });

    it('works with windows styled paths', async () => {
      if (process.platform !== 'win32') {
        return;
      }

      const workspaceFolder: WorkspaceFolder = {
        uri: 'file:///C:/path/to/workspace',
        name: 'Workspace',
      };

      jest.mocked(mockFsClient.promises.readFile).mockResolvedValue(sampleGitConfig);

      jest.mocked(api.fetchFromApi).mockResolvedValue({
        project: { duoFeaturesEnabled: true },
      } as { project: GqlProjectWithDuoEnabledInfo });

      const cache = new DefaultDuoWorkspaceProjectAccessCache(
        mockFsClient,
        api,
        virtualFileSystemService,
        configService,
      );

      const [eventHandler] = jest.mocked(virtualFileSystemService.onFileSystemEvent).mock.calls[0];

      eventHandler(VirtualFileSystemEvents.WorkspaceFilesEvent, {
        workspaceFolder,
        files: [fsPathToUri('C:\\path\\to\\workspace\\project1\\.git\\config')],
      });
      await jest.runAllTimersAsync();

      const projects = cache.getProjectsForWorkspaceFolder(workspaceFolder);
      expect(projects).toHaveLength(1);
      expect(projects[0]).toMatchObject({
        projectPath: 'gitlab-development-kit',
        enabled: true,
        host: 'gitlab.com',
        namespace: 'gitlab-org',
        namespaceWithPath: 'gitlab-org/gitlab-development-kit',
      });
    });
  });

  describe('getProjectsForWorkspaceFolder', () => {
    it('should return the projects for the given workspace folder', async () => {
      const workspaceFolder: WorkspaceFolder = {
        uri: 'file:///path/to/workspace',
        name: 'Workspace',
      };

      jest.mocked(mockFsClient.promises.readFile).mockResolvedValue(sampleGitConfig);

      jest.mocked(api.fetchFromApi).mockResolvedValue({
        project: { duoFeaturesEnabled: true },
      } as { project: GqlProjectWithDuoEnabledInfo });

      const cache = new DefaultDuoWorkspaceProjectAccessCache(
        mockFsClient,
        api,
        virtualFileSystemService,
        configService,
      );

      // Get the event handler that was registered
      const [eventHandler] = jest.mocked(virtualFileSystemService.onFileSystemEvent).mock.calls[0];

      eventHandler(VirtualFileSystemEvents.WorkspaceFilesEvent, {
        workspaceFolder,
        files: [
          fsPathToUri('/path/to/workspace/project1/.git/config'),
          fsPathToUri('/path/to/workspace/project2/.git/config'),
        ],
      });
      await jest.runAllTimersAsync();

      const projects = cache.getProjectsForWorkspaceFolder(workspaceFolder);

      expect(projects).toHaveLength(2);
      expect(projects[0]).toMatchObject({
        projectPath: 'gitlab-development-kit',
        uri: 'file:///path/to/workspace/project1/.git/config',
        enabled: true,
        host: 'gitlab.com',
        namespace: 'gitlab-org',
        namespaceWithPath: 'gitlab-org/gitlab-development-kit',
      });
      expect(projects[1]).toMatchObject({
        projectPath: 'gitlab-development-kit',
        uri: 'file:///path/to/workspace/project2/.git/config',
        enabled: true,
        host: 'gitlab.com',
        namespace: 'gitlab-org',
        namespaceWithPath: 'gitlab-org/gitlab-development-kit',
      });
    });
  });
});
