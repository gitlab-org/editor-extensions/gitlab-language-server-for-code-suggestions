import { ApiReconfiguredData, GitLabApiService, InstanceInfo } from '@gitlab-org/core';
import { AIContextSearchRequest } from '@gitlab-org/ai-context';
import { createFakePartial } from '../../test_utils/create_fake_partial';
import {
  DefaultDuoFeatureAccessService,
  DuoFeature,
  DuoCodeSuggestionsContext,
} from './duo_feature_access_service';

type MockAiFeaturesAPIResponse = {
  duoChatAvailableFeatures: DuoFeature[];
  codeSuggestionsContexts: DuoCodeSuggestionsContext[];
};

describe('DuoFeatureAccessService', () => {
  let service: DefaultDuoFeatureAccessService;
  let mockGitLabApiService: GitLabApiService;

  const duoChatAIRequest = { featureType: 'duo_chat' } as AIContextSearchRequest;
  const codeSuggestionsAIRequest = { featureType: 'code_suggestions' } as AIContextSearchRequest;

  function mockApiResponse(features: MockAiFeaturesAPIResponse) {
    const { duoChatAvailableFeatures, codeSuggestionsContexts } = features;

    jest.mocked(mockGitLabApiService.fetchFromApi).mockResolvedValue({
      currentUser: {
        duoChatAvailableFeatures,
        codeSuggestionsContexts,
      },
    });
  }

  function mockApiResponseFor1760(chatFeatures: DuoFeature[]) {
    jest.mocked(mockGitLabApiService.fetchFromApi).mockResolvedValue({
      currentUser: {
        duoChatAvailableFeatures: chatFeatures,
      },
    });
  }

  describe('isFeatureEnabled', () => {
    describe('when the API is correctly configured', () => {
      beforeEach(() => {
        mockGitLabApiService = createFakePartial<GitLabApiService>({
          fetchFromApi: jest.fn(),
          onApiReconfigured: jest.fn(),
          instanceInfo: createFakePartial<InstanceInfo>({ instanceVersion: '17.9.0' }),
        });
        service = new DefaultDuoFeatureAccessService(mockGitLabApiService);
      });

      it.each([undefined, duoChatAIRequest, codeSuggestionsAIRequest])(
        'allows providers with no feature requirements',
        async (aiRequest) => {
          const requiredFeature = undefined;
          const result = await service.isFeatureEnabled(requiredFeature, aiRequest);

          expect(result).toEqual({ enabled: true });
          expect(mockGitLabApiService.fetchFromApi).not.toHaveBeenCalled();
        },
      );

      it.each([
        { aiRequest: undefined, testFeature: DuoFeature.IncludeIssueContext },
        { aiRequest: duoChatAIRequest, testFeature: DuoFeature.IncludeIssueContext },
        { aiRequest: codeSuggestionsAIRequest, testFeature: DuoCodeSuggestionsContext.OpenTabs },
      ])('fetches features only once for multiple checks', async ({ aiRequest, testFeature }) => {
        mockApiResponse({
          duoChatAvailableFeatures: [DuoFeature.IncludeIssueContext],
          codeSuggestionsContexts: [DuoCodeSuggestionsContext.OpenTabs],
        });

        await service.isFeatureEnabled(testFeature, aiRequest);
        await service.isFeatureEnabled(testFeature, aiRequest);

        expect(mockGitLabApiService.fetchFromApi).toHaveBeenCalledTimes(1);
      });

      it.each([
        { aiRequest: undefined, testFeature: DuoFeature.IncludeIssueContext },
        { aiRequest: duoChatAIRequest, testFeature: DuoFeature.IncludeIssueContext },
        { aiRequest: codeSuggestionsAIRequest, testFeature: DuoCodeSuggestionsContext.OpenTabs },
      ])('re-fetches features after API reconfiguration', async ({ aiRequest, testFeature }) => {
        mockApiResponse({
          duoChatAvailableFeatures: [DuoFeature.IncludeIssueContext],
          codeSuggestionsContexts: [DuoCodeSuggestionsContext.OpenTabs],
        });

        await service.isFeatureEnabled(testFeature, aiRequest);
        expect(mockGitLabApiService.fetchFromApi).toHaveBeenCalledTimes(1);

        const callback = jest.mocked(mockGitLabApiService.onApiReconfigured).mock.calls[0][0];
        callback(createFakePartial<ApiReconfiguredData>({}));

        await service.isFeatureEnabled(testFeature, aiRequest);
        expect(mockGitLabApiService.fetchFromApi).toHaveBeenCalledTimes(2);
      });

      it.each([
        {
          aiRequest: undefined,
          requiredFeature: DuoFeature.IncludeIssueContext,
        },
        {
          aiRequest: duoChatAIRequest,
          requiredFeature: DuoFeature.IncludeIssueContext,
        },
        {
          aiRequest: codeSuggestionsAIRequest,
          requiredFeature: DuoCodeSuggestionsContext.OpenTabs,
        },
      ])(
        'disables provider when required feature is not available for feature type',
        async ({ aiRequest, requiredFeature }) => {
          mockApiResponse({
            duoChatAvailableFeatures: [DuoFeature.IncludeFileContext],
            codeSuggestionsContexts: [DuoCodeSuggestionsContext.Imports],
          });

          const result = await service.isFeatureEnabled(requiredFeature, aiRequest);
          expect(result).toEqual({
            enabled: false,
            disabledReasons: [`Feature "${requiredFeature}" is not enabled`],
          });
        },
      );

      it.each([
        { aiRequest: undefined, requiredFeature: DuoCodeSuggestionsContext.OpenTabs },
        { aiRequest: duoChatAIRequest, requiredFeature: DuoCodeSuggestionsContext.OpenTabs },
        { aiRequest: codeSuggestionsAIRequest, requiredFeature: DuoFeature.IncludeFileContext },
      ])(
        'disables provider without fetching from API if required feature does not match the feature type',
        async ({ aiRequest, requiredFeature }) => {
          const result = await service.isFeatureEnabled(requiredFeature, aiRequest);

          expect(mockGitLabApiService.fetchFromApi).not.toHaveBeenCalled();
          expect(result).toEqual({
            enabled: false,
            disabledReasons: [`Feature "${requiredFeature}" is not enabled`],
          });
        },
      );

      it.each([
        { aiRequest: undefined, requiredFeature: DuoFeature.IncludeIssueContext },
        { aiRequest: duoChatAIRequest, requiredFeature: DuoFeature.IncludeIssueContext },
        {
          aiRequest: codeSuggestionsAIRequest,
          requiredFeature: DuoCodeSuggestionsContext.OpenTabs,
        },
      ])(
        'disables feature-gated providers on API error',
        async ({ aiRequest, requiredFeature }) => {
          jest.mocked(mockGitLabApiService.fetchFromApi).mockRejectedValue(new Error('API Error'));

          const result = await service.isFeatureEnabled(requiredFeature, aiRequest);
          expect(result).toEqual({
            enabled: false,
            disabledReasons: [`Feature "${requiredFeature}" is not enabled`],
          });
        },
      );

      it('makes correct GraphQL query for Duo Chat features', async () => {
        mockApiResponse({ duoChatAvailableFeatures: [], codeSuggestionsContexts: [] });

        await service.isFeatureEnabled(DuoFeature.IncludeIssueContext);

        expect(mockGitLabApiService.fetchFromApi).toHaveBeenCalledWith({
          type: 'graphql',
          query: expect.stringContaining('duoChatAvailableFeatures'),
          variables: {},
          supportedSinceInstanceVersion: {
            version: '17.6.0',
            resourceName: 'get Duo available features',
          },
        });
      });

      it('makes correct GraphQL query for Code Suggestions features', async () => {
        mockApiResponse({ duoChatAvailableFeatures: [], codeSuggestionsContexts: [] });

        const aiRequest = { featureType: 'code_suggestions' } as AIContextSearchRequest;
        await service.isFeatureEnabled(DuoCodeSuggestionsContext.OpenTabs, aiRequest);

        expect(mockGitLabApiService.fetchFromApi).toHaveBeenCalledWith({
          type: 'graphql',
          query: expect.stringContaining('codeSuggestionsContexts'),
          variables: {},
          supportedSinceInstanceVersion: {
            version: '17.6.0',
            resourceName: 'get Duo available features',
          },
        });
      });

      describe('provider feature requirements', () => {
        it.each([
          DuoFeature.IncludeFileContext,
          DuoFeature.IncludeSnippetContext,
          DuoFeature.IncludeIssueContext,
          DuoFeature.IncludeMergeRequestContext,
          'include_dependency_context',
        ] as DuoFeature[])('%s provider requires correct feature', async (requiredFeature) => {
          mockApiResponse({
            duoChatAvailableFeatures: [requiredFeature],
            codeSuggestionsContexts: [],
          });

          const result = await service.isFeatureEnabled(requiredFeature);
          expect(result.enabled).toBe(true);

          const resultWithAIRequest = await service.isFeatureEnabled(
            requiredFeature,
            duoChatAIRequest,
          );
          expect(resultWithAIRequest.enabled).toBe(true);
        });

        it.each([
          DuoCodeSuggestionsContext.OpenTabs,
          DuoCodeSuggestionsContext.Imports,
          DuoCodeSuggestionsContext.RepositoryXray,
        ])('%s Code Suggestions provider requires correct feature', async (requiredFeature) => {
          mockApiResponse({
            duoChatAvailableFeatures: [],
            codeSuggestionsContexts: [requiredFeature],
          });

          const aiRequest = { featureType: 'code_suggestions' } as AIContextSearchRequest;
          const result = await service.isFeatureEnabled(requiredFeature, aiRequest);
          expect(result.enabled).toBe(true);
        });
      });

      describe('caching behavior', () => {
        it.each([
          { aiRequest: undefined, testFeature: DuoFeature.IncludeIssueContext },
          { aiRequest: duoChatAIRequest, testFeature: DuoFeature.IncludeIssueContext },
          { aiRequest: codeSuggestionsAIRequest, testFeature: DuoCodeSuggestionsContext.OpenTabs },
        ])('caches negative results', async ({ aiRequest, testFeature }) => {
          jest
            .mocked(mockGitLabApiService.fetchFromApi)
            .mockRejectedValueOnce(new Error('API Error'));

          await service.isFeatureEnabled(testFeature, aiRequest);
          await service.isFeatureEnabled(testFeature, aiRequest);

          expect(mockGitLabApiService.fetchFromApi).toHaveBeenCalledTimes(1);
        });

        it('uses same promise for concurrent Duo Chat requests', async () => {
          mockApiResponse({
            duoChatAvailableFeatures: [DuoFeature.IncludeIssueContext],
            codeSuggestionsContexts: [],
          });

          await Promise.all([
            // an undefined aiRequest is treated as a Duo Chat request
            service.isFeatureEnabled(DuoFeature.IncludeIssueContext),
            service.isFeatureEnabled(DuoFeature.IncludeIssueContext),
            service.isFeatureEnabled(DuoFeature.IncludeMergeRequestContext, duoChatAIRequest),
            service.isFeatureEnabled(DuoFeature.IncludeFileContext, duoChatAIRequest),
          ]);

          expect(mockGitLabApiService.fetchFromApi).toHaveBeenCalledTimes(1);
        });

        it('uses same promise for concurrent Code Suggestions requests', async () => {
          mockApiResponse({
            duoChatAvailableFeatures: [],
            codeSuggestionsContexts: [DuoCodeSuggestionsContext.OpenTabs],
          });

          await Promise.all([
            service.isFeatureEnabled(DuoCodeSuggestionsContext.OpenTabs, codeSuggestionsAIRequest),
            service.isFeatureEnabled(DuoCodeSuggestionsContext.Imports, codeSuggestionsAIRequest),
            service.isFeatureEnabled(
              DuoCodeSuggestionsContext.RepositoryXray,
              codeSuggestionsAIRequest,
            ),
          ]);

          expect(mockGitLabApiService.fetchFromApi).toHaveBeenCalledTimes(1);
        });
      });
    });

    describe('when the API is configured for GitLab 17.6.0 to 17.8.x', () => {
      beforeEach(() => {
        mockGitLabApiService = createFakePartial<GitLabApiService>({
          fetchFromApi: jest.fn(),
          onApiReconfigured: jest.fn(),
          instanceInfo: createFakePartial<InstanceInfo>({ instanceVersion: '17.6.0' }),
        });
        service = new DefaultDuoFeatureAccessService(mockGitLabApiService);
      });

      it('makes correct GraphQL query for Duo Chat features', async () => {
        mockApiResponseFor1760([]);

        await service.isFeatureEnabled(DuoFeature.IncludeIssueContext);

        expect(mockGitLabApiService.fetchFromApi).toHaveBeenCalledWith({
          type: 'graphql',
          query: expect.stringContaining('duoChatAvailableFeatures'),
          variables: {},
          supportedSinceInstanceVersion: {
            version: '17.6.0',
            resourceName: 'get Duo available features',
          },
        });
      });

      describe('provider feature requirements', () => {
        it.each([
          DuoFeature.IncludeFileContext,
          DuoFeature.IncludeSnippetContext,
          DuoFeature.IncludeIssueContext,
          DuoFeature.IncludeMergeRequestContext,
          'include_dependency_context',
        ] as DuoFeature[])(
          '%s Duo Chat provider requires correct feature',
          async (requiredFeature) => {
            mockApiResponseFor1760([requiredFeature]);

            const result = await service.isFeatureEnabled(requiredFeature);
            expect(result.enabled).toBe(true);

            const resultWithAIRequest = await service.isFeatureEnabled(
              requiredFeature,
              duoChatAIRequest,
            );
            expect(resultWithAIRequest.enabled).toBe(true);
          },
        );

        it.each([
          DuoCodeSuggestionsContext.OpenTabs,
          DuoCodeSuggestionsContext.Imports,
          DuoCodeSuggestionsContext.RepositoryXray,
        ])(
          '%s Code Suggestions provider is not supported and disabled',
          async (requiredFeature) => {
            mockApiResponseFor1760([]);

            const aiRequest = { featureType: 'code_suggestions' } as AIContextSearchRequest;
            const result = await service.isFeatureEnabled(requiredFeature, aiRequest);

            expect(result).toEqual({
              enabled: false,
              disabledReasons: [`Feature "${requiredFeature}" is not enabled`],
            });
          },
        );
      });
    });

    describe('when the API has not yet been correctly configured', () => {
      beforeEach(() => {
        mockGitLabApiService = createFakePartial<GitLabApiService>({
          fetchFromApi: jest.fn(),
          onApiReconfigured: jest.fn().mockReturnValue({ dispose: jest.fn() }),
          instanceInfo: undefined, // Simulate uninitialized API
        });
        service = new DefaultDuoFeatureAccessService(mockGitLabApiService);
      });

      it.each([
        { aiRequest: duoChatAIRequest, testFeature: DuoFeature.IncludeIssueContext },
        { aiRequest: codeSuggestionsAIRequest, testFeature: DuoCodeSuggestionsContext.Imports },
      ])(
        'waits for API configuration when instanceInfo is not available',
        async ({ aiRequest, testFeature }) => {
          const disposeMock = jest.fn();
          jest.mocked(mockGitLabApiService.onApiReconfigured).mockReturnValue({
            dispose: disposeMock,
          });
          mockApiResponse({
            duoChatAvailableFeatures: [DuoFeature.IncludeIssueContext],
            codeSuggestionsContexts: [DuoCodeSuggestionsContext.Imports],
          });

          const featuresPromise = service.isFeatureEnabled(testFeature, aiRequest);

          const mockApiReconfiguredCallback = jest.mocked(mockGitLabApiService.onApiReconfigured)
            .mock.calls[1][0];

          Object.defineProperty(mockGitLabApiService, 'instanceInfo', {
            get: () =>
              createFakePartial<InstanceInfo>({
                instanceVersion: '17.6.0',
              }),
          });
          mockApiReconfiguredCallback(createFakePartial<ApiReconfiguredData>({}));

          const result = await featuresPromise;

          expect(result).toEqual({ enabled: true });
          expect(mockGitLabApiService.fetchFromApi).toHaveBeenCalledTimes(1);
          expect(disposeMock).toHaveBeenCalled();
        },
      );

      it.each([
        {
          aiRequest: undefined,
          testFeature1: DuoFeature.IncludeIssueContext,
          testFeature2: DuoFeature.IncludeMergeRequestContext,
        },
        {
          aiRequest: duoChatAIRequest,
          testFeature1: DuoFeature.IncludeIssueContext,
          testFeature2: DuoFeature.IncludeMergeRequestContext,
        },
        {
          aiRequest: codeSuggestionsAIRequest,
          testFeature1: DuoCodeSuggestionsContext.OpenTabs,
          testFeature2: DuoCodeSuggestionsContext.Imports,
        },
      ])(
        'handles multiple concurrent requests for Duo Chat features while waiting for API configuration',
        async ({ aiRequest, testFeature1, testFeature2 }) => {
          mockApiResponse({
            duoChatAvailableFeatures: [
              DuoFeature.IncludeIssueContext,
              DuoFeature.IncludeMergeRequestContext,
            ],
            codeSuggestionsContexts: [
              DuoCodeSuggestionsContext.OpenTabs,
              DuoCodeSuggestionsContext.Imports,
            ],
          });

          const promise1 = service.isFeatureEnabled(testFeature1, aiRequest);
          const promise2 = service.isFeatureEnabled(testFeature2, aiRequest);

          const mockApiReconfiguredCallback = jest.mocked(mockGitLabApiService.onApiReconfigured)
            .mock.calls[1][0];

          Object.defineProperty(mockGitLabApiService, 'instanceInfo', {
            get: () =>
              createFakePartial<InstanceInfo>({
                instanceVersion: '17.6.0',
              }),
          });
          mockApiReconfiguredCallback(createFakePartial<ApiReconfiguredData>({}));

          const [result1, result2] = await Promise.all([promise1, promise2]);

          expect(result1).toEqual({ enabled: true });
          expect(result2).toEqual({ enabled: true });
          expect(mockGitLabApiService.fetchFromApi).toHaveBeenCalledTimes(1);
        },
      );
    });
  });
});
