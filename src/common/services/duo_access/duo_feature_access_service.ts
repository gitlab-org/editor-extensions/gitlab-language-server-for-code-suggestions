import { GitLabApiService } from '@gitlab-org/core';
import { createInterfaceId, Injectable } from '@gitlab/needle';
import { gql } from 'graphql-request';
import { AIContextPolicyResponse, AIContextSearchRequest } from '@gitlab-org/ai-context';
import { log } from '../../log';
import { ifVersionGte } from '../../utils/if_version_gte';

export enum DuoFeature {
  IncludeFileContext = 'include_file_context',
  IncludeSnippetContext = 'include_snippet_context',
  IncludeMergeRequestContext = 'include_merge_request_context',
  IncludeIssueContext = 'include_issue_context',
  IncludeDependencyContext = 'include_dependency_context',
  IncludeLocalGitContext = 'include_local_git_context',
}

export enum DuoCodeSuggestionsContext {
  OpenTabs = 'open_tabs',
  Imports = 'imports',
  RepositoryXray = 'repository_xray',
}

export interface DuoFeatureAccessService {
  isFeatureEnabled(
    requiredFeature?: DuoFeature | DuoCodeSuggestionsContext,
    aiRequest?: AIContextSearchRequest,
  ): Promise<AIContextPolicyResponse>;
}

type AIContextSearchRequestFeatureType = AIContextSearchRequest['featureType'];

type EnabledDuoFeatures = Record<
  AIContextSearchRequestFeatureType,
  Set<DuoFeature | DuoCodeSuggestionsContext>
>;

export const DuoFeatureAccessService =
  createInterfaceId<DuoFeatureAccessService>('DuoFeatureAccessService');

@Injectable(DuoFeatureAccessService, [GitLabApiService])
export class DefaultDuoFeatureAccessService implements DuoFeatureAccessService {
  readonly #gitlabApiService: GitLabApiService;

  #featuresPromise: Promise<EnabledDuoFeatures> | null = null;

  constructor(gitlabApiService: GitLabApiService) {
    this.#gitlabApiService = gitlabApiService;

    this.#gitlabApiService.onApiReconfigured(() => {
      // Next time a provider feature access is checked we will re-fetch using updated API details
      this.#featuresPromise = null;
    });
  }

  async #fetchFeatures(): Promise<EnabledDuoFeatures> {
    if (!this.#gitlabApiService.instanceInfo) {
      // Handle a race condition where isContextProviderAllowed (and thus fetchFeatures) can be called before the API
      // has been configured for the first time. This happens consistently in integration tests. In this case, we will
      // wait for the first reconfiguration and then call back into fetchFeatures to continue checking the provider.
      return new Promise((resolve) => {
        const listener = this.#gitlabApiService.onApiReconfigured(() => {
          listener.dispose();
          resolve(this.#fetchFeatures());
        });
      });
    }

    try {
      type GqlAvailableFeaturesResponse = {
        currentUser: {
          duoChatAvailableFeatures: DuoFeature[];
          codeSuggestionsContexts?: DuoCodeSuggestionsContext[];
        };
      };
      const query = ifVersionGte(
        this.#gitlabApiService.instanceInfo.instanceVersion,
        '17.9.0',
        () => gql`
          query getDuoAvailableFeatures {
            currentUser {
              duoChatAvailableFeatures
              codeSuggestionsContexts
            }
          }
        `,
        () => gql`
          query getDuoChatAvailableFeatures {
            currentUser {
              duoChatAvailableFeatures
            }
          }
        `,
      );

      const response = await this.#gitlabApiService.fetchFromApi<GqlAvailableFeaturesResponse>({
        type: 'graphql',
        query,
        variables: {},
        supportedSinceInstanceVersion: {
          version: '17.6.0',
          resourceName: 'get Duo available features',
        },
      });

      const { duoChatAvailableFeatures, codeSuggestionsContexts } = response.currentUser;
      log.debug(
        `[DuoFeatureAccessService] Fetched Duo available features for current user: Chat (${duoChatAvailableFeatures}), Code Suggestions (${codeSuggestionsContexts})`,
      );
      return {
        duo_chat: new Set(duoChatAvailableFeatures),
        code_suggestions: new Set(codeSuggestionsContexts),
      };
    } catch (error) {
      log.error('[DuoFeatureAccessService] Error fetching Duo available features:', error);

      // Empty set means no features will be available
      return {
        duo_chat: new Set<DuoFeature>(),
        code_suggestions: new Set<DuoCodeSuggestionsContext>(),
      };
    }
  }

  async isFeatureEnabled(
    requiredFeature?: DuoFeature | DuoCodeSuggestionsContext,
    aiRequest?: AIContextSearchRequest,
  ): Promise<AIContextPolicyResponse> {
    if (!requiredFeature) {
      return { enabled: true };
    }

    const featureType = aiRequest?.featureType || 'duo_chat';
    if (this.isFeatureInType(featureType, requiredFeature)) {
      if (!this.#featuresPromise) {
        this.#featuresPromise = this.#fetchFeatures();
      }
      const features = await this.#featuresPromise;
      if (features[featureType].has(requiredFeature)) {
        return { enabled: true };
      }
    }

    return {
      enabled: false,
      disabledReasons: [`Feature "${requiredFeature}" is not enabled`],
    };
  }

  isFeatureInType(
    featureType: AIContextSearchRequestFeatureType,
    requiredFeature: DuoFeature | DuoCodeSuggestionsContext,
  ): boolean {
    if (featureType === 'duo_chat') {
      return Object.values(DuoFeature).includes(requiredFeature as DuoFeature);
    }

    if (featureType === 'code_suggestions') {
      return Object.values(DuoCodeSuggestionsContext).includes(
        requiredFeature as DuoCodeSuggestionsContext,
      );
    }

    return false;
  }
}
