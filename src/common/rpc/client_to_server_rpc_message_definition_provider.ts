import { Container } from '@gitlab/needle';
import { Logger, withPrefix } from '@gitlab-org/logging';
import { RpcMessageDefinition, RpcMessageDefinitionProvider } from '@gitlab-org/rpc';
import { EndpointProvider } from '@gitlab-org/rpc-endpoint';

export class DefaultClientToServerRpcMessageDefinitionProvider
  implements RpcMessageDefinitionProvider
{
  readonly #container: Container;

  readonly #logger: Logger;

  constructor(container: Container, logger: Logger) {
    this.#container = container;
    this.#logger = withPrefix(logger, '[DefaultClientToServerMessageDefinitionProvider]: ');
  }

  getMessageDefinitions(): RpcMessageDefinition[] {
    const definitions = this.#container
      .getAll(EndpointProvider)
      .flatMap((source) => source.getEndpoints());

    this.#logger.debug(
      `Found ${definitions.length} client-to-server message definitions from sources`,
    );

    return definitions;
  }
}
