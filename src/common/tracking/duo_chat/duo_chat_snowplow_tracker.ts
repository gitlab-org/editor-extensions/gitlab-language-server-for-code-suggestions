import { SelfDescribingJson, StructuredEvent } from '@snowplow/tracker-core';
import { Injectable } from '@gitlab/needle';
import { Logger, withPrefix } from '@gitlab-org/logging';
import {
  DUO_CHAT_EVENT,
  DUO_CHAT_CATEGORY,
  DuoChatContext,
  DuoChatSnowplowTracker,
  isTrackFeedbackContext,
} from '@gitlab-org/telemetry';
import { pickBy } from 'lodash';
import { ConfigService, IConfig } from '../../config_service';
import { SnowplowService } from '../snowplow/snowplow_service';
import { SAAS_INSTANCE_URL } from '../constants';
import { ISnowplowClientContext, IClientContext } from '../snowplow/constants';
import { version as lsVersion } from '../ls_info.json';
import * as IdeExtensionContextSchema from '../code_suggestions/schemas/ide_extension_version-1-1-0.json';
import * as StandardContextSchema from '../snowplow/schemas/standard_context_schema-1-1-1.json';
import { StandardContext } from '../snowplow/standard_context';

@Injectable(DuoChatSnowplowTracker, [ConfigService, SnowplowService, StandardContext, Logger])
export class DefaultDuoChatSnowplowTracker implements DuoChatSnowplowTracker {
  #snowplowService: SnowplowService;

  #configService: ConfigService;

  #standardContext: StandardContext;

  #logger: Logger;

  #options = {
    enabled: true,
    baseUrl: SAAS_INSTANCE_URL,
  };

  #clientContext: ISnowplowClientContext = {
    schema: 'iglu:com.gitlab/ide_extension_version/jsonschema/1-1-0',
    data: {},
  };

  constructor(
    configService: ConfigService,
    snowplowService: SnowplowService,
    standardContext: StandardContext,
    logger: Logger,
  ) {
    this.#configService = configService;
    this.#configService.onConfigChange((config) => this.#reconfigure(config));
    this.#snowplowService = snowplowService;
    this.#standardContext = standardContext;
    this.#logger = withPrefix(logger, '[DuoChatTelemetry]');
  }

  isEnabled(): boolean {
    return this.#options.enabled;
  }

  async #reconfigure(config: IConfig) {
    const { baseUrl } = config.client;
    const enabled = config.client.telemetry?.enabled;

    if (typeof enabled !== 'undefined' && this.#options.enabled !== enabled) {
      this.#options.enabled = enabled;

      if (enabled === false) {
        this.#logger.warn(
          `Telemetry is disabled. Please, consider enabling telemetry to improve our service.`,
        );
      } else if (enabled === true) {
        this.#logger.info(`Telemetry is enabled.`);
      }
    }

    if (baseUrl) {
      this.#options.baseUrl = baseUrl;
    }

    this.#setClientContext({
      extension: config.client.telemetry?.extension,
      ide: config.client.telemetry?.ide,
    });
  }

  #setClientContext(context: IClientContext) {
    this.#clientContext.data = {
      ide_name: context?.ide?.name ?? null,
      ide_vendor: context?.ide?.vendor ?? null,
      ide_version: context?.ide?.version ?? null,
      extension_name: context?.extension?.name ?? null,
      extension_version: context?.extension?.version ?? null,
      language_server_version: lsVersion ?? null,
    };
  }

  async trackEvent(event: DUO_CHAT_EVENT, context: DuoChatContext) {
    const structuredEvent: StructuredEvent | null = this.#buildStructuredEvent(event, context);

    if (!structuredEvent) {
      return;
    }

    try {
      const standardContext = this.#buildStandardContext(context);
      if (!standardContext) {
        return;
      }

      const contexts: SelfDescribingJson[] = [standardContext, this.#clientContext];

      const standardContextValid = this.#snowplowService.validateContext(
        StandardContextSchema,
        standardContext.data,
      );

      if (!standardContextValid) {
        return;
      }

      const ideExtensionContextValid = this.#snowplowService.validateContext(
        IdeExtensionContextSchema,
        this.#clientContext?.data,
      );

      if (!ideExtensionContextValid) {
        return;
      }

      await this.#snowplowService.trackStructuredEvent(structuredEvent, contexts);
    } catch (error) {
      this.#logger.warn(`Failed to track telemetry event: ${event}`, error);
    }
  }

  #buildStructuredEvent(event: DUO_CHAT_EVENT, context: DuoChatContext): StructuredEvent | null {
    if (isTrackFeedbackContext(context)) {
      const { didWhat, improveWhat, feedbackChoices } = context;

      const hasFeedback =
        Boolean(didWhat) || Boolean(improveWhat) || Boolean(feedbackChoices?.length);
      if (!hasFeedback) return null;

      return {
        category: DUO_CHAT_CATEGORY,
        action: event,
        label: 'response_feedback',
        property: context.feedbackChoices?.join(','),
      };
    }

    return null;
  }

  #buildStandardContext(context: DuoChatContext): SelfDescribingJson | null {
    if (isTrackFeedbackContext(context)) {
      const { improveWhat, didWhat } = context;

      const extra = pickBy({ improveWhat, didWhat }, (value): value is string => Boolean(value));

      return this.#standardContext.build(extra);
    }

    return null;
  }
}
