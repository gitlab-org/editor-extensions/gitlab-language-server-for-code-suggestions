import { Injectable } from '@gitlab/needle';
import { Logger, withPrefix } from '@gitlab-org/logging';
import { AIContextManager } from '@gitlab-org/ai-context';
import { PipelineLogPrefix, PreProcessor, PreProcessorItems } from './pre_processor_pipeline';

export const contentFetchedLog = (id: string) => {
  return `Content fetched for ${id}`;
};

@Injectable(PreProcessor, [Logger, AIContextManager])
export class ContextItemContentFetcher implements PreProcessor {
  name = 'context_item_content_fetcher' as const;

  readonly #logger: Logger;

  readonly #aiContextManager: AIContextManager;

  constructor(logger: Logger, aiContextManager: AIContextManager) {
    this.#logger = withPrefix(logger, `${PipelineLogPrefix}-[ContextItemContentFetcher]`);
    this.#aiContextManager = aiContextManager;
  }

  async process(items: PreProcessorItems) {
    const { aiContextItems } = items;
    const aiContextItemsWithContent = await this.#aiContextManager.retrieveContextItemsWithContent({
      featureType: 'code_suggestions',
      aiContextItems,
    });
    const validatedItems = aiContextItemsWithContent
      .map((item) => {
        if (!item.content) {
          this.#logger.error(`Content is missing for ${item.id}`);
          return null;
        }
        this.#logger.info(contentFetchedLog(item.id));
        return item;
      })
      .filter((item) => item !== null);

    return {
      preProcessorItems: {
        ...items,
        aiContextItems: validatedItems,
      },
    };
  }
}
