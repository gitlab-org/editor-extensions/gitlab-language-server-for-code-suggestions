import { ApiRequest } from '@gitlab-org/core';
import { isEqual } from 'lodash';
import { SimpleApiClient } from '../simple_api_client';
import { FetchError } from '../fetch_error';
import { createFakeResponse } from './create_fake_response';

export type Resp = { error?: Error; success?: unknown };
export type RequestResponse = [ApiRequest<unknown>, Resp];

export const success = (response: unknown): Resp => ({ success: response });
export const error = (e: Error): Resp => ({ error: e });

export class TestSimpleApiClient implements SimpleApiClient {
  responses: RequestResponse[];

  constructor() {
    this.responses = [];
  }

  getDefaultHeaders() {
    return {};
  }

  async fetchFromApi<TReturnType>(request: ApiRequest<TReturnType>): Promise<TReturnType> {
    const [, response] = this.responses.find(([req]) => isEqual(request, req)) || [];
    if (response?.error) throw response.error;
    if (response?.success) return response.success as TReturnType;

    throw new FetchError(
      createFakeResponse({ status: 404 }),
      `Request ${JSON.stringify(request)} did not match any recorded test cases in TestSimpleApiClient`,
    );
  }
}
