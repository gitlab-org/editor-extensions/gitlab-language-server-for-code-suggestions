import { AIContextItem } from '@gitlab-org/ai-context';
import { GitContextItem } from './local_git_context_provider';
import { DependencyAIContextItem } from './dependencies';
import { ImportAIContextItem } from './imports/import_context_provider';
import { OpenTabAIContextItem } from './open_tabs/open_tabs_provider';
import { IssueAIContextItem } from './issue';
import { MergeRequestAIContextItem } from './merge_request';
import { LocalFileAIContextItem } from './file_local_search';

export type AIContextItemByProviderType = {
  open_tab: OpenTabAIContextItem;
  import: ImportAIContextItem;
  local_file_search: LocalFileAIContextItem;
  issue: IssueAIContextItem;
  merge_request: MergeRequestAIContextItem;
  dependency: DependencyAIContextItem;
  local_git: GitContextItem;
};

export function asTypedContextItem<T extends keyof AIContextItemByProviderType>(
  item: AIContextItem,
  type: T,
): item is AIContextItemByProviderType[T] {
  return item.metadata.subType === type;
}
