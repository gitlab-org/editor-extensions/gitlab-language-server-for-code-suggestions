import { Injectable } from '@gitlab/needle';
import { WorkspaceFolder } from 'vscode-languageserver';
import { Utils } from 'vscode-uri';
import type {
  AIContextItem,
  AIContextItemMetadata,
  AIContextSearchRequest,
  AIContextPolicyResponse,
  AIContextRetrieveRequest,
} from '@gitlab-org/ai-context';
import { TextDocument } from 'vscode-languageserver-textdocument';
import { log } from '../../../log';
import { OpenTabsService } from '../../../open_tabs/open_tabs_service';
import { DuoProjectAccessChecker } from '../../../services/duo_access';
import {
  DuoFeature,
  DuoFeatureAccessService,
} from '../../../services/duo_access/duo_feature_access_service';
import { DuoProjectStatus } from '../../../services/duo_access/project_access_checker';
import type { DuoProject } from '../../../services/duo_access/workspace_project_access_cache';
import { parseURIString } from '../../../services/fs/utils';
import { isBinaryContent } from '../../../utils/binary_content';
import { AbstractAIContextProvider, getSearchRequestType } from '../../ai_context_provider';
import { FilePolicyProvider } from '../../context_policies/file_policy';
import { BINARY_FILE_DISABLED_REASON } from '../../context_transformers/ai_context_binary_file_transformer';
import { AIContextProvider } from '../..';
import { IDocContext } from '../../../document_transformer_service';
import { DISABLED_REASONS } from '../constants';

export const duoNotEnabledLog = (uri: string) => {
  return `duo features are not enabled for ${uri}`;
};

export type OpenTabMetadata = AIContextItemMetadata & {
  subType: 'open_tab';
  iid?: string;
  title?: string;
  relativePath?: string;
  workspaceFolder?: WorkspaceFolder;
  project?: string;
  languageId: TextDocument['languageId'];
  lastAccessed: number;
  lastModified: number;
  byteSize: number;
};

export type OpenTabAIContextItem = AIContextItem & {
  category: 'file';
  metadata: OpenTabMetadata;
};

export interface OpenTabContextProvider extends AbstractAIContextProvider<OpenTabAIContextItem> {}

@Injectable(AIContextProvider, [
  DuoProjectAccessChecker,
  FilePolicyProvider,
  DuoFeatureAccessService,
  OpenTabsService,
])
export class DefaultOpenTabContextProvider
  extends AbstractAIContextProvider<OpenTabAIContextItem>
  implements OpenTabContextProvider
{
  #projectAccessChecker: DuoProjectAccessChecker;

  #openTabsService: OpenTabsService;

  #policy: FilePolicyProvider;

  duoRequiredFeature = DuoFeature.IncludeFileContext;

  constructor(
    projectAccessChecker: DuoProjectAccessChecker,
    policy: FilePolicyProvider,
    duoFeatureAccessService: DuoFeatureAccessService,
    openTabsService: OpenTabsService,
  ) {
    super('open_tab', duoFeatureAccessService);
    this.#policy = policy;
    this.#projectAccessChecker = projectAccessChecker;
    this.#openTabsService = openTabsService;

    this.canItemBeAdded = async (
      contextItem: OpenTabAIContextItem,
    ): Promise<AIContextPolicyResponse> => {
      return this.#policy.isContextItemAllowed(contextItem.metadata.relativePath ?? '');
    };
  }

  async searchContextItems(query: AIContextSearchRequest): Promise<OpenTabAIContextItem[]> {
    const searchRequest = getSearchRequestType(query);
    switch (searchRequest.featureType) {
      case 'code_suggestions':
        return this.#getOpenFiles(searchRequest.iDocContext);
      case 'duo_chat': {
        if (searchRequest.query.trim() !== '') {
          return [];
        }
        return this.#getOpenFiles();
      }
      default: {
        throw new Error(`Unsupported feature type`);
      }
    }
  }

  async #getOpenFiles(iDocContext?: IDocContext): Promise<OpenTabAIContextItem[]> {
    const openTabs = this.#openTabsService.mostRecentTabs({
      context: iDocContext,
      includeCurrentFile: !iDocContext,
    });

    log.debug(`[OpenTabContextProvider] context item search for ${this.type}`);
    const promises = openTabs.map(async (openTab) => {
      let project: DuoProject | undefined;
      const disabledReasons: string[] = [];

      const fileContent = openTab.prefix + openTab.suffix;
      if (isBinaryContent(fileContent)) {
        disabledReasons.push(BINARY_FILE_DISABLED_REASON);
      }

      if (openTab.workspaceFolder) {
        const { project: projectFromChecker, status } =
          this.#projectAccessChecker.checkProjectStatus(openTab.uri, openTab.workspaceFolder);
        project = projectFromChecker;
        if (status === DuoProjectStatus.DuoDisabled) {
          log.debug(duoNotEnabledLog(openTab.uri));
          disabledReasons.push(DISABLED_REASONS.DUO_PROJECT_DISABLED);
        }
      }

      const { enabled: policyEnabled, disabledReasons: policyReasons = [] } =
        await this.#policy.isContextItemAllowed(openTab.fileRelativePath);
      if (!policyEnabled) {
        disabledReasons.push(...policyReasons);
      }

      const projectPath = project?.namespaceWithPath;

      const item = {
        id: openTab.uri,
        category: 'file' as const,
        metadata: {
          languageId: openTab.languageId,
          title: Utils.basename(parseURIString(openTab.uri)),
          project: projectPath ?? 'not a GitLab project',
          enabled: disabledReasons.length === 0,
          disabledReasons,
          icon: 'document',
          secondaryText: openTab.fileRelativePath,
          subType: 'open_tab' as const,
          subTypeLabel: 'Project file',
          relativePath: openTab.fileRelativePath,
          workspaceFolder: openTab.workspaceFolder ?? { name: '', uri: '' },
          lastAccessed: openTab.lastAccessed,
          lastModified: openTab.lastModified,
          byteSize: openTab.byteSize,
        },
      } satisfies OpenTabAIContextItem;

      log.debug(`[OpenTabContextProvider] open tab context item ${item.id} was found`);
      return item;
    });

    return Promise.all(promises);
  }

  async retrieveContextItemsWithContent(
    request?: AIContextRetrieveRequest<OpenTabAIContextItem>,
  ): Promise<OpenTabAIContextItem[]> {
    if (request?.aiContextItems) {
      return Promise.all(request.aiContextItems.map((item) => this.getItemWithContent(item)));
    }
    const items = await this.getSelectedContextItems();

    const itemsWithContentPromises = items.map((document) => {
      log.debug(
        `[OpenTabContextProvider] open tab context item ${document.id} was retrieved with content`,
      );
      return this.getItemWithContent(document);
    });

    return Promise.all(itemsWithContentPromises);
  }

  async getItemWithContent(item: OpenTabAIContextItem): Promise<OpenTabAIContextItem> {
    const itemFile = this.#openTabsService.openTabsCache.get(item.id);
    if (itemFile) {
      return {
        ...item,
        content: itemFile.prefix + itemFile.suffix,
      };
    }

    log.error(`[OpenTabContextProvider] failed to get content for item "${item.id}"`);
    return item;
  }
}
