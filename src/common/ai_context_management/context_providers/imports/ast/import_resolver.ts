import { createInterfaceId } from '@gitlab/needle';
import { SyntaxNode } from 'web-tree-sitter';
import { URI } from 'vscode-uri';
import { TreeSitterLanguageName } from '../../../../tree_sitter/languages';
import { IDocContext } from '../../../../document_transformer_service';
import { TreeAndLanguage } from '../../../../tree_sitter';
import { CaptureNameMap, QueryCaptureWithNames } from './utils';

type DefaultImportPathType = string;
type DefaultImportIdentifierType = string;

/*
 * This is the metadata for a single import identifier, which can be a part of a larger import statement.
 *
 * Example:
 * ```typescript
 * import { foo } from 'bar';
 * ```
 *
 * In this example, `foo` is the import identifier.
 * The import path is `bar`.
 * The language name is `typescript`.
 * The import type is `named`.
 * The identifier is `foo`.
 */
export type ImportIdentifier<
  TImportPathType = DefaultImportPathType,
  TImportIdentifierType = DefaultImportIdentifierType,
> = {
  /**
   * The node that contains the import identifier.
   * This should refer to the node in memory
   * if possible. You should always use
   * `web-tree-sitter` to get the node.
   */
  node: SyntaxNode;
  /**
   * The import path for the import.
   */
  importPath: string;

  /**
   * The type of the import.
   * This is used to strongly type the import type you expect to get from the query.
   */
  importPathType: TImportPathType;
  /**
   * The type of the import identifier.
   * This is used to strongly type the identifier you expect to get from the query.
   */
  identifierType: TImportIdentifierType;
  /**
   * The original identifier for the import.
   * This is optional because some imports do not have identifiers.
   */
  identifier?: string;
  /**
   * An alias for the import identifier that
   * points to the original identifier.
   *
   * This may not be applicable for all languages.
   */
  alias?: string;
};

/**
 * This is the metadata for a single import statement.
 *
 * Example:
 * ```typescript
 * import { foo } from 'bar';
 * ```
 *
 * In this example, the source document URI is `file:///path/to/file.ts`.
 * The import path is `bar`.
 * The language name is `typescript`.
 * The import identifiers are `foo`.
 *
 * The import identifiers are optional because some imports do not have identifiers.
 * For example, `import 'bar';` does not have any identifiers.
 */
export type ImportMetadata<
  TImportType = DefaultImportPathType,
  TIdentifierType = DefaultImportIdentifierType,
> = {
  sourceDocumentContext: IDocContext;
  importPath: string;
  languageName: TreeSitterLanguageName;
  importIdentifiers: ImportIdentifier<TImportType, TIdentifierType>[];
};

export type ResolvedImportMetadata<
  TImportType = DefaultImportPathType,
  TIdentifierType = DefaultImportIdentifierType,
> = {
  resolvedImportPath: URI;
} & ImportMetadata<TImportType, TIdentifierType>;

export abstract class AbstractImportResolver<
  /*
   * This is the type of the captures that are returned by the TreeSitter query.
   * You should use this to strongly type the captures you expect to get from the query.
   */
  T extends CaptureNameMap = CaptureNameMap,
  /*
   * This is the type of the import path for the import.
   * This is used to strongly type the import path you expect to get from the query.
   */
  TImportType = DefaultImportPathType,
  /**
   * This is the type of the identifier for the import.
   * This is used to strongly type the identifier you expect to get from the query.
   */
  TIdentifierType = DefaultImportIdentifierType,
> {
  /**
   * @returns the languages this import resolver supports
   */
  abstract readonly supportedLanguages: TreeSitterLanguageName[];

  /**
   * @returns A TreeSitter query that will capture the import paths in the source document
   * This is implemented for each language.
   */
  abstract getTreeSitterQuery(): string;

  /**
   * This is implemented for each language.
   * You can use the captures to get the import paths and then map them to the ImportMetadata
   *
   * If a document contains duplicate imports, the caller will expect
   * the same import path to be returned for each import.
   */
  abstract getImportMetadataByPath({
    captures,
    sourceDocumentContext,
    treeAndLanguage,
  }: {
    captures: QueryCaptureWithNames<T>[];
    sourceDocumentContext: IDocContext;
    treeAndLanguage: TreeAndLanguage;
  }): Promise<Map<string, ImportMetadata<TImportType, TIdentifierType>>>;

  /**
   * This is implemented for each language.
   * You can use the import path to resolve the import path to a URI.
   *
   * This method can access the file system, `RepositoryService`, or any other service
   * to resolve the import path to a URI for a given source document.
   */
  abstract resolveImportPath({
    importPath,
    sourceDocumentContext,
    sourceDocumentUri,
  }: {
    importPath: string;
    sourceDocumentContext: IDocContext;
    sourceDocumentUri: URI;
  }): Promise<URI | null>;

  /**
   * This is a helper method to check if the import resolver is enabled for a given language.
   *
   * This is used by the ImportContextProvider to determine if the import resolver should be used for a given language.
   */
  enabledForLanguage(languageName: TreeSitterLanguageName): boolean {
    return this.supportedLanguages.includes(languageName);
  }

  /**
   * This is an optional method that can be implemented for each language.
   *
   * This allows the specific import resolver to override the default behavior of the above methods.
   */
  resolveImports?({
    iDocContext,
    treeAndLanguage,
  }: {
    iDocContext: IDocContext;
    treeAndLanguage: TreeAndLanguage;
  }): Promise<Map<string, ResolvedImportMetadata<TImportType, TIdentifierType>>>;
}

// Generic AIContextProvider interface ID cannot handle contravariant parameters and covariant returns properly in DI registration
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const ImportResolver = createInterfaceId<AbstractImportResolver<any>>('ImportResolver');
