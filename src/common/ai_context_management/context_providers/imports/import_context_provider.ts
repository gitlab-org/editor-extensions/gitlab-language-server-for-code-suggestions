import { createCollectionId, Injectable } from '@gitlab/needle';
import {
  AIContextItem,
  AIContextItemMetadata,
  AIContextProviderType,
  AIContextRetrieveRequest,
  AIContextSearchRequest,
} from '@gitlab-org/ai-context';
import { Logger, withPrefix } from '@gitlab-org/logging';
import { URI } from 'vscode-uri';
import { AbstractAIContextProvider, getSearchRequestType } from '../../ai_context_provider';
import { AIContextProvider } from '../..';
import { FsClient } from '../../../services/fs/fs';
import {
  DuoProjectAccessChecker,
  DuoProjectStatus,
} from '../../../services/duo_access/project_access_checker';
import { FilePolicyProvider } from '../../context_policies/file_policy';
import { DuoProject } from '../../../services/duo_access/workspace_project_access_cache';
import { OpenTabsService } from '../../../open_tabs/open_tabs_service';
import { parseURIString } from '../../../services/fs/utils';
import { IDocContext } from '../../../document_transformer_service';
import {
  DuoCodeSuggestionsContext,
  DuoFeatureAccessService,
} from '../../../services/duo_access/duo_feature_access_service';
import { DISABLED_REASONS } from '../constants';
import {
  ResolvedImportMetadata,
  ImportResolver,
  AbstractImportResolver,
} from './ast/import_resolver';

export type ImportAIContextItem = AIContextItem & {
  category: 'file';
  metadata: AIContextItemMetadata & {
    subType: AIContextProviderType;
    enabled: boolean;
    project?: string;
  } & ResolvedImportMetadata;
};

export const ImportHandlerResolverCollection = createCollectionId(ImportResolver);

const ImportProviderType = 'import' as const;
const ImportProviderIcon = 'import' as const;

@Injectable(AIContextProvider, [
  Logger,
  FsClient,
  ImportHandlerResolverCollection,
  DuoProjectAccessChecker,
  FilePolicyProvider,
  OpenTabsService,
  DuoFeatureAccessService,
])
export class DefaultImportContextProvider
  extends AbstractAIContextProvider<ImportAIContextItem>
  implements AIContextProvider<ImportAIContextItem>
{
  readonly type = ImportProviderType;

  #logger: Logger;

  #fsClient: FsClient;

  #importResolvers: AbstractImportResolver[];

  #projectAccessChecker: DuoProjectAccessChecker;

  #policy: FilePolicyProvider;

  #openTabsService: OpenTabsService;

  duoRequiredFeature = DuoCodeSuggestionsContext.Imports;

  constructor(
    logger: Logger,
    fsClient: FsClient,
    importResolvers: AbstractImportResolver[],
    projectAccessChecker: DuoProjectAccessChecker,
    policy: FilePolicyProvider,
    openTabsService: OpenTabsService,
    duoFeatureAccessService: DuoFeatureAccessService,
  ) {
    super(ImportProviderType, duoFeatureAccessService);
    this.#logger = withPrefix(logger, '[ImportContextProvider]');
    this.#fsClient = fsClient;
    this.#importResolvers = importResolvers;
    this.#projectAccessChecker = projectAccessChecker;
    this.#policy = policy;
    this.#openTabsService = openTabsService;
  }

  /**
   * Analyzes imports in the given document and provides context for code suggestions.
   * This method implements a multi-step import analysis pipeline:
   *
   * 1. Language Handler Selection:
   *    - Validates if language info exists and finds appropriate import resolver
   *    - Each resolver declares supported languages (JS/TS/TSX/Vue etc.)
   *
   * 2. AST Captures:
   *    - Uses Tree-sitter to query the document's AST via the language's query
   *
   * 3. Import Metadata Extraction:
   *    - Processes Tree-sitter captures to extract normalized import data
   *    - Groups imports by their source module
   *
   * 4. Path Resolution:
   *    - Resolves import strings to actual file system URIs
   */
  async searchContextItems(
    aiContextSearchRequest: AIContextSearchRequest,
  ): Promise<ImportAIContextItem[]> {
    this.#logger.debug('searching imports');
    const searchRequest = getSearchRequestType(aiContextSearchRequest);
    if (searchRequest.featureType !== 'code_suggestions') {
      return [];
    }
    const { iDocContext, treeAndLanguage } = searchRequest;
    if (!treeAndLanguage?.languageInfo.name) {
      return [];
    }

    this.#logger.debug(
      `Getting context for code suggestions for language: ${treeAndLanguage?.languageInfo.name}`,
    );

    const importResolver = this.#importResolvers.find((resolver) =>
      resolver.enabledForLanguage(treeAndLanguage?.languageInfo.name),
    );
    if (!importResolver) {
      this.#logger.debug(
        `No import resolver found for language: ${treeAndLanguage?.languageInfo.name}`,
      );
      return [];
    }

    const captures = treeAndLanguage.language
      .query(importResolver.getTreeSitterQuery())
      .captures(treeAndLanguage.tree.rootNode);

    const importPathsToMetadata = await importResolver.getImportMetadataByPath({
      captures,
      sourceDocumentContext: iDocContext,
      treeAndLanguage,
    });
    const sourceDocumentUri = parseURIString(iDocContext.uri);

    const resolvedImportMetadataArray = (
      await Promise.all(
        Array.from(importPathsToMetadata.entries()).map(async ([importPath, metadata]) => {
          const resolvedImportPath = await importResolver.resolveImportPath({
            importPath,
            sourceDocumentContext: iDocContext,
            sourceDocumentUri,
          });
          return {
            ...metadata,
            resolvedImportPath,
          };
        }),
      )
    ).filter((item): item is ResolvedImportMetadata => item.resolvedImportPath !== null);

    const contextItems = await Promise.all(
      resolvedImportMetadataArray.map(async (metadata) => {
        return this.#createContextItem(metadata, iDocContext);
      }),
    );
    this.#logger.debug(`Found ${contextItems.length} import context items`);
    return contextItems;
  }

  async #createContextItem(
    metadata: ResolvedImportMetadata,
    iDocContext: IDocContext,
  ): Promise<ImportAIContextItem> {
    const { resolvedImportPath: uri } = metadata;
    const [disabledReasons, project] = await Promise.all([
      this.#getDisabledReasons(uri, iDocContext),
      this.#getProjectInfo(uri, iDocContext),
    ]);

    return {
      id: uri.toString(),
      category: 'file' as const,
      metadata: {
        title: uri.fsPath,
        enabled: disabledReasons.length === 0,
        disabledReasons,
        icon: ImportProviderIcon,
        subType: ImportProviderType,
        subTypeLabel: 'Imported file',
        secondaryText: uri.fsPath,
        project: project?.namespaceWithPath ?? 'not a GitLab project',
        ...metadata,
      },
    };
  }

  async #getDisabledReasons(uri: URI, iDocContext: IDocContext): Promise<string[]> {
    const disabledReasons: string[] = [];

    if (iDocContext.workspaceFolder) {
      const { status } = this.#projectAccessChecker.checkProjectStatus(
        uri.toString(),
        iDocContext.workspaceFolder,
      );
      if (status === DuoProjectStatus.DuoDisabled) {
        this.#logger.debug(`duo features are not enabled for ${uri.toString()}`);
        disabledReasons.push(DISABLED_REASONS.DUO_PROJECT_DISABLED);
      }
    }

    const { enabled: policyEnabled, disabledReasons: policyReasons = [] } =
      await this.#policy.isContextItemAllowed(uri.fsPath);
    if (!policyEnabled) {
      disabledReasons.push(...policyReasons);
    }

    return disabledReasons;
  }

  async #getProjectInfo(uri: URI, iDocContext: IDocContext): Promise<DuoProject | undefined> {
    if (!iDocContext.workspaceFolder) {
      return undefined;
    }

    const { project } = this.#projectAccessChecker.checkProjectStatus(
      uri.toString(),
      iDocContext.workspaceFolder,
    );
    return project;
  }

  #getFileContent(uri: URI): Promise<string> {
    // check if the imported file already exists in memory to avoid disk I/O
    const openFile = this.#openTabsService.openTabsCache.get(uri.fsPath);
    if (openFile) {
      return Promise.resolve(`${openFile.prefix}${openFile.suffix}`);
    }

    return this.#fsClient.promises.readFile(uri.fsPath).then((buffer) => buffer.toString('utf-8'));
  }

  async addSelectedContextItem(): Promise<void> {
    throw new Error('Method not implemented.');
  }

  async removeSelectedContextItem(): Promise<void> {
    throw new Error('Method not implemented.');
  }

  async clearSelectedContextItems(): Promise<void> {
    throw new Error('Method not implemented.');
  }

  async replaceSelectedContextItem(): Promise<void> {
    throw new Error('Method not implemented.');
  }

  async getSelectedContextItems(): Promise<ImportAIContextItem[]> {
    return [];
  }

  async retrieveContextItemsWithContent(
    request?: AIContextRetrieveRequest<ImportAIContextItem>,
  ): Promise<ImportAIContextItem[]> {
    if (request?.aiContextItems) {
      return Promise.all(
        request.aiContextItems.map(async (item) => {
          return {
            ...item,
            content: await this.#getFileContent(item.metadata.resolvedImportPath),
          };
        }),
      );
    }
    return [];
  }

  async getItemWithContent(item: ImportAIContextItem): Promise<ImportAIContextItem> {
    return item;
  }
}
