import { DefaultAIContextManager } from './ai_context_manager';
import { RelativeJavaScriptFilePathResolver } from './context_providers/imports/ast/javascript/ast/import_file_path_resolver/relative_javascript_file_path_resolver';
import { DefaultTsConfigStore } from './context_providers/imports/ast/javascript/ast/import_file_path_resolver/tsconfig_store';
import { DefaultAiContextTransformerService } from './context_transformers/ai_context_transformer_service';
import { DefaultSecretContextTransformer } from './context_transformers/ai_context_secret_transformer';
import { DefaultBinaryFileTransformer } from './context_transformers/ai_context_binary_file_transformer';
import { DefaultFilePolicyProvider } from './context_policies/file_policy';
import { DefaultDependencyScanner } from './context_providers/depdendency_scanner/scanner';
import { DefaultDependencyContextProvider } from './context_providers/dependencies';
import { DefaultLocalFileContextProvider } from './context_providers/file_local_search';
import { DefaultOpenTabContextProvider } from './context_providers/open_tabs/open_tabs_provider';
import { DefaultIssueContextProvider } from './context_providers/issue';
import { DefaultLocalGitContextProvider } from './context_providers/local_git_context_provider';
import { DefaultMergeRequestContextProvider } from './context_providers/merge_request';
import { DefaultImportContextProvider } from './context_providers/imports/import_context_provider';
import { JavascriptImportResolver } from './context_providers/imports/ast/javascript/javascript_import_resolver';
import { DefaultJavascriptImportStore } from './context_providers/imports/ast/javascript/javascript_import_store';

export const aiContextManagementContributions = [
  DefaultSecretContextTransformer,
  DefaultBinaryFileTransformer,
  DefaultAiContextTransformerService,
  DefaultAIContextManager,
  DefaultOpenTabContextProvider,
  DefaultFilePolicyProvider,
  DefaultLocalFileContextProvider,
  DefaultIssueContextProvider,
  DefaultMergeRequestContextProvider,
  DefaultDependencyContextProvider,
  DefaultDependencyScanner,
  DefaultLocalGitContextProvider,
  DefaultImportContextProvider,
  DefaultJavascriptImportStore,
  JavascriptImportResolver,
  RelativeJavaScriptFilePathResolver,
  DefaultTsConfigStore,
] as const;
