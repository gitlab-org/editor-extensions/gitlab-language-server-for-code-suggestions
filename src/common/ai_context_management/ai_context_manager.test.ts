import { GitLabApiService, InstanceInfo } from '@gitlab-org/core';
import { AIContextProviderType, AIContextSearchRequest } from '@gitlab-org/ai-context';
import { TestLogger } from '@gitlab-org/logging';
import { createFakePartial } from '../test_utils/create_fake_partial';
import { DefaultConfigService, ConfigService } from '../config_service';
import { DefaultRepositoryService } from '../services/git/repository_service';
import { FsClient } from '../services/fs/fs';
import {
  DefaultDuoFeatureAccessService,
  DuoCodeSuggestionsContext,
} from '../services/duo_access/duo_feature_access_service';
import { DuoProjectAccessChecker } from '../services/duo_access';
import { OpenTabsService } from '../open_tabs/open_tabs_service';
import { IssueContextProvider } from './context_providers/issue';
import type { MergeRequestContextProvider } from './context_providers/merge_request';
import type { AiContextTransformerService } from './context_transformers/ai_context_transformer_service';
import {
  OPEN_TAB_FILE,
  ANOTHER_OPEN_TAB_FILE,
  INVALID_SUBTYPE_ITEM,
  NO_SUBTYPE_ITEM,
  DISABLED_OPEN_TAB_FILE,
  FILE_LOCAL_SEARCH_ITEM,
  DEPENDENCY_ITEM,
  ISSUE_ITEM,
  MERGE_REQUEST_ITEM,
  IMPORT_FILE,
} from './test_utils/mock_data';
import { FilePolicyProvider } from './context_policies/file_policy';
import { DefaultOpenTabContextProvider } from './context_providers/open_tabs/open_tabs_provider';
import { DefaultAIContextManager } from './ai_context_manager';
import {
  DefaultLocalFileContextProvider,
  LocalFilesContextProvider,
} from './context_providers/file_local_search';
import { DependencyContextProvider } from './context_providers/dependencies';
import { LocalGitContextProvider } from './context_providers/local_git_context_provider';
import { AbstractImportResolver } from './context_providers/imports/ast/import_resolver';
import { DefaultImportContextProvider } from './context_providers/imports/import_context_provider';
import { AIContextProvider } from '.';

describe('AI Context Manager', () => {
  let manager: DefaultAIContextManager;
  let mockContextTransformerService: AiContextTransformerService;
  let configService: ConfigService;

  let mockOpenTabProvider: DefaultOpenTabContextProvider;
  let mockImportProvider: DefaultImportContextProvider;
  let mockLocalFilesProvider: LocalFilesContextProvider;
  let mockDependencyProvider: DependencyContextProvider;
  let mockIssueContextProvider: IssueContextProvider;
  let mockMergeRequestProvider: MergeRequestContextProvider;
  let mockLocalGitProvider: LocalGitContextProvider;

  let logger: TestLogger;

  const createMockProvider = <T extends { type: AIContextProviderType }>(
    type: AIContextProviderType,
  ) =>
    createFakePartial<T>({
      type,
      addSelectedContextItem: jest.fn(),
      removeSelectedContextItem: jest.fn(),
      getSelectedContextItems: jest.fn(),
      searchContextItems: jest.fn(),
      retrieveContextItemsWithContent: jest.fn(),
      getItemWithContent: jest.fn(),
      getContextForCodeSuggestions: jest.fn(),
      isAvailable: jest.fn().mockResolvedValue({ enabled: true }),
    } as unknown as T);

  beforeEach(() => {
    configService = new DefaultConfigService();
    mockContextTransformerService = createFakePartial<AiContextTransformerService>({
      transform: jest.fn().mockImplementation((item) => item),
    });
    mockOpenTabProvider = createMockProvider<DefaultOpenTabContextProvider>('open_tab');
    mockImportProvider = createMockProvider<DefaultImportContextProvider>('import');
    mockLocalFilesProvider = createMockProvider<LocalFilesContextProvider>('local_file_search');
    mockDependencyProvider = createMockProvider<DependencyContextProvider>('dependency');
    mockMergeRequestProvider = createMockProvider<MergeRequestContextProvider>('merge_request');
    mockLocalGitProvider = createMockProvider<LocalGitContextProvider>('local_git');
    mockIssueContextProvider = createMockProvider<IssueContextProvider>('issue');
    logger = new TestLogger();

    manager = new DefaultAIContextManager(logger, configService, mockContextTransformerService, [
      mockOpenTabProvider,
      mockImportProvider,
      mockLocalFilesProvider,
      mockDependencyProvider,
      mockIssueContextProvider,
      mockMergeRequestProvider,
      mockLocalGitProvider,
    ] as AIContextProvider[]);
    return manager.getAvailableCategories();
  });

  describe('addSelectedContextItem', () => {
    it('adds the context item to the provider', async () => {
      const response = await manager.addSelectedContextItem(OPEN_TAB_FILE);
      expect(response).toBe(true);
      expect(mockOpenTabProvider.addSelectedContextItem).toHaveBeenCalledWith(OPEN_TAB_FILE);
    });

    it('return false if adding context item fails', () => {
      const error = new Error('Failed to add context item');
      mockOpenTabProvider.addSelectedContextItem = jest.fn().mockImplementation(() => {
        throw error;
      });

      expect(manager.addSelectedContextItem(OPEN_TAB_FILE)).resolves.toBe(false);
    });

    it('returns false if adding item to disabled provider', async () => {
      jest.mocked(mockOpenTabProvider.isAvailable).mockResolvedValue({ enabled: false });
      await manager.getAvailableCategories();

      await expect(manager.addSelectedContextItem(OPEN_TAB_FILE)).resolves.toBe(false);
      expect(mockOpenTabProvider.addSelectedContextItem).not.toHaveBeenCalled();
    });

    it.each([INVALID_SUBTYPE_ITEM, NO_SUBTYPE_ITEM])(
      'returns false if no provider found for type',
      (item) => {
        expect(manager.addSelectedContextItem(item)).resolves.toBe(false);
      },
    );
  });

  describe('removeSelectedContextItem', () => {
    it('removes a context item from the provider', async () => {
      const result = await manager.removeSelectedContextItem(OPEN_TAB_FILE);
      expect(result).toBe(true);
      expect(mockOpenTabProvider.removeSelectedContextItem).toHaveBeenCalledWith(OPEN_TAB_FILE.id);
    });

    it('returns false if removing a context item fails', () => {
      const error = new Error('Failed to remove context item');
      mockOpenTabProvider.removeSelectedContextItem = jest.fn().mockImplementation(() => {
        throw error;
      });

      expect(manager.removeSelectedContextItem(OPEN_TAB_FILE)).resolves.toBe(false);
    });

    it.each([INVALID_SUBTYPE_ITEM, NO_SUBTYPE_ITEM])(
      'returns false if no provider found for type',
      (item) => {
        expect(manager.removeSelectedContextItem(item)).resolves.toBe(false);
      },
    );
  });

  describe('searchContextItems', () => {
    it('calls getContextItems() on the provider', async () => {
      const mockOpenTabsContext = [OPEN_TAB_FILE, ANOTHER_OPEN_TAB_FILE];
      mockOpenTabProvider.searchContextItems = jest.fn().mockResolvedValue(mockOpenTabsContext);
      mockOpenTabProvider.getSelectedContextItems = jest.fn().mockResolvedValue([]);

      const mockLocalFilesContext = [FILE_LOCAL_SEARCH_ITEM];
      mockLocalFilesProvider.searchContextItems = jest
        .fn()
        .mockResolvedValue(mockLocalFilesContext);
      mockLocalFilesProvider.getSelectedContextItems = jest.fn().mockResolvedValue([]);

      const mockDependenciesContext = [DEPENDENCY_ITEM];
      mockDependencyProvider.searchContextItems = jest
        .fn()
        .mockResolvedValue(mockDependenciesContext);
      mockDependencyProvider.getSelectedContextItems = jest.fn().mockResolvedValue([]);

      const mockIssuesContext = [ISSUE_ITEM];
      mockIssueContextProvider.searchContextItems = jest.fn().mockResolvedValue(mockIssuesContext);
      mockIssueContextProvider.getSelectedContextItems = jest.fn().mockResolvedValue([]);

      mockMergeRequestProvider.getSelectedContextItems = jest.fn().mockResolvedValue([]);

      const result = await manager.searchContextItems({
        featureType: 'duo_chat',
        category: 'file',
        query: 'test',
        workspaceFolders: [],
      });
      expect(result).toEqual([...mockOpenTabsContext, ...mockLocalFilesContext]);
      expect(mockOpenTabProvider.searchContextItems).toHaveBeenCalled();
      expect(mockLocalFilesProvider.searchContextItems).toHaveBeenCalled();

      const depResult = await manager.searchContextItems({
        featureType: 'duo_chat',
        category: 'dependency',
        query: 'test',
        workspaceFolders: [],
      });
      expect(depResult).toEqual([...mockDependenciesContext]);
      expect(mockDependencyProvider.searchContextItems).toHaveBeenCalled();

      const issueResult = await manager.searchContextItems({
        featureType: 'duo_chat',
        category: 'issue',
        query: 'test',
        workspaceFolders: [],
      });
      expect(issueResult).toEqual([...mockIssuesContext]);
      expect(mockIssueContextProvider.searchContextItems).toHaveBeenCalled();
    });

    it('should call providers with "workspaceFolders" from query params or fallback to config value', async () => {
      const query: AIContextSearchRequest = {
        featureType: 'duo_chat',
        category: 'file',
        query: 'test',
        workspaceFolders: [],
      };

      await manager.searchContextItems(query);
      expect(mockOpenTabProvider.searchContextItems).toHaveBeenLastCalledWith(query);

      const workspaceFolders = [
        {
          uri: 'file:///workspace',
          name: 'Test Workspace',
        },
      ];
      configService.set('client.workspaceFolders', workspaceFolders);
      const noWSFQuery: AIContextSearchRequest = {
        featureType: 'duo_chat',
        category: 'file',
        query: 'test',
      };
      await manager.searchContextItems(noWSFQuery);
      expect(mockOpenTabProvider.searchContextItems).toHaveBeenLastCalledWith({
        ...noWSFQuery,
        workspaceFolders,
      });
    });

    it('should filter out already selected items', async () => {
      const mockContext = [OPEN_TAB_FILE, ANOTHER_OPEN_TAB_FILE];
      const selectedItems = [OPEN_TAB_FILE];
      mockOpenTabProvider.searchContextItems = jest.fn().mockResolvedValue(mockContext);
      mockOpenTabProvider.getSelectedContextItems = jest.fn().mockResolvedValue(selectedItems);
      mockLocalFilesProvider.searchContextItems = jest.fn().mockResolvedValue([]);
      mockLocalFilesProvider.getSelectedContextItems = jest.fn().mockResolvedValue([]);
      mockDependencyProvider.searchContextItems = jest.fn().mockResolvedValue([]);
      mockDependencyProvider.getSelectedContextItems = jest.fn().mockResolvedValue([]);
      mockIssueContextProvider.searchContextItems = jest.fn().mockResolvedValue([]);
      mockIssueContextProvider.getSelectedContextItems = jest.fn().mockResolvedValue([]);
      mockMergeRequestProvider.getSelectedContextItems = jest.fn().mockResolvedValue([]);

      const result = await manager.searchContextItems({
        featureType: 'duo_chat',
        category: 'file',
        query: 'test',
        workspaceFolders: [],
      });

      expect(result).toEqual([ANOTHER_OPEN_TAB_FILE]);
      expect(mockOpenTabProvider.searchContextItems).toHaveBeenCalled();
      expect(mockOpenTabProvider.getSelectedContextItems).toHaveBeenCalled();
    });

    it('should put disabled items at the end of the list', async () => {
      const mockContext = [OPEN_TAB_FILE, DISABLED_OPEN_TAB_FILE, ANOTHER_OPEN_TAB_FILE];
      mockOpenTabProvider.searchContextItems = jest.fn().mockResolvedValue(mockContext);
      mockOpenTabProvider.getSelectedContextItems = jest.fn().mockResolvedValue([]);
      mockLocalFilesProvider.searchContextItems = jest.fn().mockResolvedValue([]);
      mockDependencyProvider.searchContextItems = jest.fn().mockResolvedValue([]);
      mockDependencyProvider.getSelectedContextItems = jest.fn().mockResolvedValue([]);
      mockIssueContextProvider.searchContextItems = jest.fn().mockResolvedValue([]);
      mockIssueContextProvider.getSelectedContextItems = jest.fn().mockResolvedValue([]);
      mockMergeRequestProvider.getSelectedContextItems = jest.fn().mockResolvedValue([]);

      const result = await manager.searchContextItems({
        featureType: 'duo_chat',
        category: 'file',
        query: 'test',
        workspaceFolders: [],
      });

      expect(result).toEqual([OPEN_TAB_FILE, ANOTHER_OPEN_TAB_FILE, DISABLED_OPEN_TAB_FILE]);
      expect(mockOpenTabProvider.searchContextItems).toHaveBeenCalled();
      expect(mockOpenTabProvider.getSelectedContextItems).toHaveBeenCalled();
    });

    it('does not call providers of other categories', async () => {
      mockOpenTabProvider.searchContextItems = jest.fn().mockResolvedValue([]);
      mockOpenTabProvider.getSelectedContextItems = jest.fn().mockReturnValue([]);
      mockLocalFilesProvider.searchContextItems = jest.fn().mockResolvedValue([]);
      mockLocalFilesProvider.getSelectedContextItems = jest.fn().mockReturnValue([]);
      mockIssueContextProvider.searchContextItems = jest.fn().mockResolvedValue([]);
      mockIssueContextProvider.getSelectedContextItems = jest.fn().mockResolvedValue([]);
      mockMergeRequestProvider.searchContextItems = jest.fn().mockResolvedValue([]);
      mockMergeRequestProvider.getSelectedContextItems = jest
        .fn()
        .mockResolvedValue([MERGE_REQUEST_ITEM]);

      await manager.searchContextItems({
        featureType: 'duo_chat',
        category: 'merge_request',
        query: 'test',
        workspaceFolders: [],
      });

      expect(mockOpenTabProvider.searchContextItems).not.toHaveBeenCalled();
      expect(mockLocalFilesProvider.searchContextItems).not.toHaveBeenCalled();
      expect(mockMergeRequestProvider.searchContextItems).toHaveBeenCalledTimes(1);
    });

    it('returns empty array when all providers for category are disabled', async () => {
      jest.mocked(mockOpenTabProvider.isAvailable).mockResolvedValue({ enabled: false });
      jest.mocked(mockLocalFilesProvider.isAvailable).mockResolvedValue({ enabled: false });
      await manager.getAvailableCategories();

      const result = await manager.searchContextItems({
        featureType: 'duo_chat',
        category: 'file',
        query: 'test',
        workspaceFolders: [],
      });

      expect(result).toEqual([]);
      expect(mockOpenTabProvider.searchContextItems).not.toHaveBeenCalled();
      expect(mockLocalFilesProvider.searchContextItems).not.toHaveBeenCalled();
    });

    it('handles code_suggestions feature type', async () => {
      const mockOpenTabsContext = [OPEN_TAB_FILE, ANOTHER_OPEN_TAB_FILE];
      mockOpenTabProvider.searchContextItems = jest.fn().mockResolvedValue(mockOpenTabsContext);

      const mockImportsContext = [IMPORT_FILE];
      mockImportProvider.searchContextItems = jest.fn().mockResolvedValue(mockImportsContext);

      const result = await manager.searchContextItems({
        featureType: 'code_suggestions' as const,
        category: 'file',
        query: 'test',
        workspaceFolders: [],
      });

      expect(result).toEqual([...mockOpenTabsContext, ...mockImportsContext]);
      expect(mockOpenTabProvider.searchContextItems).toHaveBeenCalled();
      expect(mockLocalFilesProvider.searchContextItems).toHaveBeenCalled();
    });

    it('only uses enabled providers for code_suggestions', async () => {
      jest.mocked(mockLocalFilesProvider.isAvailable).mockResolvedValue({ enabled: false });
      await manager.getAvailableCategories();

      const mockOpenTabsContext = [OPEN_TAB_FILE];
      mockOpenTabProvider.searchContextItems = jest.fn().mockResolvedValue(mockOpenTabsContext);
      mockLocalFilesProvider.searchContextItems = jest.fn();

      const result = await manager.searchContextItems({
        featureType: 'code_suggestions' as const,
        category: 'file',
        query: 'test',
        workspaceFolders: [],
      });

      expect(result).toEqual(mockOpenTabsContext);
      expect(mockOpenTabProvider.searchContextItems).toHaveBeenCalled();
      expect(mockLocalFilesProvider.searchContextItems).not.toHaveBeenCalled();
    });
  });

  describe('getAvailableCategories', () => {
    it('returns the correct categories by default', async () => {
      const categories = await manager.getAvailableCategories();

      expect(categories).toEqual([
        'file',
        'file',
        'dependency',
        'issue',
        'merge_request',
        'local_git',
      ]);
    });

    it('excludes categories when the provider is not available', async () => {
      jest.mocked(mockMergeRequestProvider.isAvailable).mockResolvedValue({ enabled: false });

      const categories = await manager.getAvailableCategories();

      expect(categories).toContain('file');
      expect(categories).toContain('dependency');
      expect(categories).toContain('issue');
      expect(categories).toContain('local_git');
      expect(categories).not.toContain('merge_request');
    });
  });

  describe('retrieveContextItemsWithContent for duo_chat', () => {
    it('calls retrieveSelectedContextItemsWithContent() on all Duo Chat providers', async () => {
      jest
        .mocked(mockOpenTabProvider.retrieveContextItemsWithContent)
        .mockResolvedValue([OPEN_TAB_FILE, ANOTHER_OPEN_TAB_FILE]);
      jest
        .mocked(mockLocalFilesProvider.retrieveContextItemsWithContent)
        .mockResolvedValue([FILE_LOCAL_SEARCH_ITEM]);
      jest
        .mocked(mockDependencyProvider.retrieveContextItemsWithContent)
        .mockResolvedValue([DEPENDENCY_ITEM]);
      jest
        .mocked(mockIssueContextProvider.retrieveContextItemsWithContent)
        .mockResolvedValue([ISSUE_ITEM]);
      jest
        .mocked(mockMergeRequestProvider.retrieveContextItemsWithContent)
        .mockResolvedValue([MERGE_REQUEST_ITEM]);

      const result = await manager.retrieveContextItemsWithContent();

      expect(result).toEqual([
        OPEN_TAB_FILE,
        ANOTHER_OPEN_TAB_FILE,
        FILE_LOCAL_SEARCH_ITEM,
        DEPENDENCY_ITEM,
        ISSUE_ITEM,
        MERGE_REQUEST_ITEM,
      ]);
      expect(mockOpenTabProvider.retrieveContextItemsWithContent).toHaveBeenCalledTimes(1);
      expect(mockLocalFilesProvider.retrieveContextItemsWithContent).toHaveBeenCalledTimes(1);
      expect(mockDependencyProvider.retrieveContextItemsWithContent).toHaveBeenCalledTimes(1);
      expect(mockIssueContextProvider.retrieveContextItemsWithContent).toHaveBeenCalledTimes(1);
      expect(mockMergeRequestProvider.retrieveContextItemsWithContent).toHaveBeenCalledTimes(1);
    });

    it('only retrieves from enabled Duo Chat providers', async () => {
      jest
        .mocked(mockOpenTabProvider.retrieveContextItemsWithContent)
        .mockResolvedValue([OPEN_TAB_FILE]);
      jest
        .mocked(mockLocalFilesProvider.retrieveContextItemsWithContent)
        .mockResolvedValue([FILE_LOCAL_SEARCH_ITEM]);

      jest.mocked(mockLocalFilesProvider.isAvailable).mockResolvedValue({ enabled: false });
      await manager.getAvailableCategories();

      const result = await manager.retrieveContextItemsWithContent();

      expect(result).toEqual([OPEN_TAB_FILE]);
      expect(mockOpenTabProvider.retrieveContextItemsWithContent).toHaveBeenCalled();
      expect(mockLocalFilesProvider.retrieveContextItemsWithContent).not.toHaveBeenCalled();
    });

    it('transforms all items through the transformer service', async () => {
      [
        mockOpenTabProvider,
        mockLocalFilesProvider,
        mockDependencyProvider,
        mockIssueContextProvider,
        mockMergeRequestProvider,
      ].forEach((provider) =>
        jest.mocked(provider.retrieveContextItemsWithContent).mockResolvedValue([]),
      );

      const item = OPEN_TAB_FILE;
      const transformedItem = { ...item, content: 'transformed' };
      jest.mocked(mockOpenTabProvider.retrieveContextItemsWithContent).mockResolvedValue([item]);
      jest.mocked(mockContextTransformerService.transform).mockResolvedValue(transformedItem);

      const result = await manager.retrieveContextItemsWithContent();

      expect(mockContextTransformerService.transform).toHaveBeenCalledTimes(1);
      expect(mockContextTransformerService.transform).toHaveBeenCalledWith(item);

      expect(result).toEqual([transformedItem]);
    });

    it('strips content when transformer service throws an error', async () => {
      const item = OPEN_TAB_FILE;
      const error = new Error('Transform failed');
      jest.mocked(mockOpenTabProvider.retrieveContextItemsWithContent).mockResolvedValue([item]);
      jest.mocked(mockContextTransformerService.transform).mockRejectedValue(error);

      const result = await manager.retrieveContextItemsWithContent();

      expect(mockContextTransformerService.transform).toHaveBeenCalledWith(item);
      expect(result).toEqual([
        {
          ...item,
          content: undefined,
        },
      ]);
    });
  });

  describe('retrieveContextItemsWithContent for code_suggestions', () => {
    it('calls retrieveContextItemsWithContent with grouped items by provider type', async () => {
      const openTabItem = { ...OPEN_TAB_FILE, id: 'open-tab-1' };
      const importItem = { ...IMPORT_FILE, id: 'import-file-1' };

      jest
        .mocked(mockOpenTabProvider.retrieveContextItemsWithContent)
        .mockResolvedValue([openTabItem]);
      jest
        .mocked(mockImportProvider.retrieveContextItemsWithContent)
        .mockResolvedValue([importItem]);

      jest.mocked(mockDependencyProvider.retrieveContextItemsWithContent).mockClear();

      const request = {
        featureType: 'code_suggestions' as const,
        aiContextItems: [openTabItem, importItem],
      };

      await manager.retrieveContextItemsWithContent(request);

      expect(mockOpenTabProvider.retrieveContextItemsWithContent).toHaveBeenCalledWith({
        featureType: 'code_suggestions' as const,
        aiContextItems: [openTabItem],
      });
      expect(mockImportProvider.retrieveContextItemsWithContent).toHaveBeenCalledWith({
        featureType: 'code_suggestions' as const,
        aiContextItems: [importItem],
      });
    });

    it('only uses enabled providers for code suggestions', async () => {
      jest.mocked(mockLocalFilesProvider.isAvailable).mockResolvedValue({ enabled: false });

      const openTabItem = { ...OPEN_TAB_FILE, id: 'open-tab-1' };
      const fileSearchItem = { ...FILE_LOCAL_SEARCH_ITEM, id: 'file-search-1' };

      const request = {
        featureType: 'code_suggestions' as const,
        aiContextItems: [openTabItem, fileSearchItem],
      };

      jest
        .mocked(mockOpenTabProvider.retrieveContextItemsWithContent)
        .mockResolvedValue([openTabItem]);

      await manager.getAvailableCategories();

      const result = await manager.retrieveContextItemsWithContent(request);

      expect(result).toEqual([openTabItem]);
      expect(mockOpenTabProvider.retrieveContextItemsWithContent).toHaveBeenCalled();
      expect(mockLocalFilesProvider.retrieveContextItemsWithContent).not.toHaveBeenCalled();
    });

    it('should not call retrieveContextItemsWithContent if invalid item type', async () => {
      const invalidItem = { ...INVALID_SUBTYPE_ITEM, id: 'invalid-1' };
      const openTabItem = { ...OPEN_TAB_FILE, id: 'open-tab-1' };

      const request = {
        featureType: 'code_suggestions' as const,
        aiContextItems: [invalidItem, openTabItem],
      };

      jest
        .mocked(mockOpenTabProvider.retrieveContextItemsWithContent)
        .mockResolvedValue([openTabItem]);

      const result = await manager.retrieveContextItemsWithContent(request);

      expect(result).toEqual([openTabItem]);
      expect(mockOpenTabProvider.retrieveContextItemsWithContent).toHaveBeenCalledWith({
        featureType: 'code_suggestions' as const,
        aiContextItems: [openTabItem],
      });
    });

    it('transforms items through the transformer service', async () => {
      const item = OPEN_TAB_FILE;
      const transformedItem = { ...item, content: 'transformed' };

      const request = {
        featureType: 'code_suggestions' as const,
        aiContextItems: [item],
      };

      jest.mocked(mockOpenTabProvider.retrieveContextItemsWithContent).mockResolvedValue([item]);
      jest.mocked(mockContextTransformerService.transform).mockResolvedValue(transformedItem);

      const result = await manager.retrieveContextItemsWithContent(request);

      expect(mockContextTransformerService.transform).toHaveBeenCalledTimes(1);
      expect(mockContextTransformerService.transform).toHaveBeenCalledWith(item);
      expect(result).toEqual([transformedItem]);
    });
  });

  describe('getItemWithContent', () => {
    it('should call getItemWithContent on the openTab provider', async () => {
      const item = OPEN_TAB_FILE;
      await manager.getItemWithContent(item);

      expect(mockOpenTabProvider.getItemWithContent).toHaveBeenCalledWith(item);
    });

    it('should call getItemWithContent on the localFiles provider', async () => {
      const item = FILE_LOCAL_SEARCH_ITEM;
      await manager.getItemWithContent(item);

      expect(mockLocalFilesProvider.getItemWithContent).toHaveBeenCalledWith(item);
    });

    it('throws an error if provided with an unknown item type', async () => {
      await expect(manager.getItemWithContent(INVALID_SUBTYPE_ITEM)).rejects.toThrow(
        'No provider found for type "invalid:subtype"',
      );
    });

    it('transforms item through the transformer service', async () => {
      const item = OPEN_TAB_FILE;
      const transformedItem = { ...item, content: 'transformed' };
      jest.mocked(mockContextTransformerService.transform).mockResolvedValue(transformedItem);
      jest.mocked(mockOpenTabProvider.getItemWithContent).mockResolvedValue(item);

      const result = await manager.getItemWithContent(item);

      expect(mockContextTransformerService.transform).toHaveBeenCalledTimes(1);
      expect(mockContextTransformerService.transform).toHaveBeenCalledWith(item);

      expect(result).toEqual(transformedItem);
    });

    it('strips content when transformer service throws an error', async () => {
      const item = OPEN_TAB_FILE;
      const error = new Error('Transform failed');
      jest.mocked(mockOpenTabProvider.getItemWithContent).mockResolvedValue(item);
      jest.mocked(mockContextTransformerService.transform).mockRejectedValue(error);

      const result = await manager.getItemWithContent(item);

      expect(mockContextTransformerService.transform).toHaveBeenCalledWith(item);
      expect(result).toEqual({
        ...item,
        content: undefined,
      });
    });
  });

  describe('getProviderForType', () => {
    it('returns the provider for the given type', async () => {
      const provider = await manager.getProviderForType('open_tab');
      expect(provider).toEqual(mockOpenTabProvider);
    });

    it('throws an error if the provider is not found', async () => {
      await expect(manager.getProviderForType('unknown' as AIContextProviderType)).rejects.toThrow(
        'No provider found for type "unknown"',
      );
    });
  });

  describe('#getCodeSuggestionsProviders', () => {
    it('always includes OpenTabContextProvider for code suggestions regardless of isAvailable result', async () => {
      jest.mocked(mockOpenTabProvider.isAvailable).mockResolvedValue({ enabled: false });
      jest.mocked(mockLocalFilesProvider.isAvailable).mockResolvedValue({ enabled: false });
      jest.mocked(mockDependencyProvider.isAvailable).mockResolvedValue({ enabled: false });
      jest.mocked(mockIssueContextProvider.isAvailable).mockResolvedValue({ enabled: false });
      jest.mocked(mockMergeRequestProvider.isAvailable).mockResolvedValue({ enabled: false });
      jest.mocked(mockLocalGitProvider.isAvailable).mockResolvedValue({ enabled: false });

      await manager.getAvailableCategories();

      const openTabItem = { ...OPEN_TAB_FILE, id: 'open-tab-1' };

      jest.mocked(mockOpenTabProvider.searchContextItems).mockResolvedValue([openTabItem]);

      jest.mocked(mockOpenTabProvider.searchContextItems).mockClear();

      const searchRequest = {
        featureType: 'code_suggestions' as const,
        category: 'file',
        query: 'test',
        workspaceFolders: [],
      };

      const result = await manager.searchContextItems(searchRequest);

      expect(mockOpenTabProvider.searchContextItems).toHaveBeenCalled();
      expect(result).toEqual([openTabItem]);

      expect(mockLocalFilesProvider.searchContextItems).not.toHaveBeenCalled();
      expect(mockDependencyProvider.searchContextItems).not.toHaveBeenCalled();
      expect(mockIssueContextProvider.searchContextItems).not.toHaveBeenCalled();
      expect(mockMergeRequestProvider.searchContextItems).not.toHaveBeenCalled();
      expect(mockLocalGitProvider.searchContextItems).not.toHaveBeenCalled();
    });

    it('includes all enabled providers for code suggestions search', async () => {
      jest.mocked(mockOpenTabProvider.isAvailable).mockResolvedValue({ enabled: true });
      jest.mocked(mockImportProvider.isAvailable).mockResolvedValue({ enabled: true });
      jest.mocked(mockLocalFilesProvider.isAvailable).mockResolvedValue({ enabled: false });
      jest.mocked(mockDependencyProvider.isAvailable).mockResolvedValue({ enabled: false });
      jest.mocked(mockIssueContextProvider.isAvailable).mockResolvedValue({ enabled: false });
      jest.mocked(mockMergeRequestProvider.isAvailable).mockResolvedValue({ enabled: false });
      jest.mocked(mockLocalGitProvider.isAvailable).mockResolvedValue({ enabled: false });

      await manager.getAvailableCategories();

      const searchRequest = {
        featureType: 'code_suggestions' as const,
        category: 'file',
        query: 'test',
        workspaceFolders: [],
      };

      jest.mocked(mockOpenTabProvider.searchContextItems).mockResolvedValue([OPEN_TAB_FILE]);
      jest.mocked(mockImportProvider.searchContextItems).mockResolvedValue([IMPORT_FILE]);
      jest
        .mocked(mockLocalFilesProvider.searchContextItems)
        .mockResolvedValue([FILE_LOCAL_SEARCH_ITEM]);
      jest.mocked(mockIssueContextProvider.searchContextItems).mockResolvedValue([ISSUE_ITEM]);

      const result = await manager.searchContextItems(searchRequest);

      expect(result).toContainEqual(OPEN_TAB_FILE);
      expect(result).toContainEqual(IMPORT_FILE);

      expect(mockLocalFilesProvider.searchContextItems).not.toHaveBeenCalled();
      expect(mockDependencyProvider.searchContextItems).not.toHaveBeenCalled();
      expect(mockMergeRequestProvider.searchContextItems).not.toHaveBeenCalled();
      expect(mockLocalGitProvider.searchContextItems).not.toHaveBeenCalled();
    });
  });
});

describe('AI Context Manager - Code Suggestions Path', () => {
  let manager: DefaultAIContextManager;
  let mockContextTransformerService: AiContextTransformerService;
  let configService: ConfigService;
  let logger: TestLogger;

  let mockProjectAccessChecker: DuoProjectAccessChecker;
  let duoFeatureAccessService: DefaultDuoFeatureAccessService;
  let mockGitLabApiService: GitLabApiService;

  let mockOpenTabProvider: DefaultOpenTabContextProvider;
  let mockImportProvider: DefaultImportContextProvider;
  let mockLocalFilesProvider: DefaultLocalFileContextProvider;

  function createOpenTabProviderMock() {
    const mockPolicy = createFakePartial<FilePolicyProvider>({});
    const mockOpenTabsService = createFakePartial<OpenTabsService>({});
    const provider = new DefaultOpenTabContextProvider(
      mockProjectAccessChecker,
      mockPolicy,
      duoFeatureAccessService,
      mockOpenTabsService,
    );

    provider.searchContextItems = jest
      .fn()
      .mockResolvedValue([OPEN_TAB_FILE, ANOTHER_OPEN_TAB_FILE]);
    provider.getSelectedContextItems = jest.fn().mockResolvedValue([OPEN_TAB_FILE]);

    return provider;
  }

  function createImportProviderMock() {
    const mockLogger = new TestLogger();
    const mockFsClient = createFakePartial<FsClient>({});
    const mockImportResolvers = [createFakePartial<AbstractImportResolver>({})];
    const mockPolicy = createFakePartial<FilePolicyProvider>({});
    const mockOpenTabsService = createFakePartial<OpenTabsService>({});
    const provider = new DefaultImportContextProvider(
      mockLogger,
      mockFsClient,
      mockImportResolvers,
      mockProjectAccessChecker,
      mockPolicy,
      mockOpenTabsService,
      duoFeatureAccessService,
    );

    provider.searchContextItems = jest.fn().mockResolvedValue([IMPORT_FILE]);
    provider.getSelectedContextItems = jest.fn().mockResolvedValue([IMPORT_FILE]);

    return provider;
  }

  function createLocalFilesProviderMock() {
    const mockRepositoryService = createFakePartial<DefaultRepositoryService>({});
    const mockFsClient = createFakePartial<FsClient>({});
    const provider = new DefaultLocalFileContextProvider(
      mockRepositoryService,
      mockProjectAccessChecker,
      mockFsClient,
      duoFeatureAccessService,
    );

    provider.searchContextItems = jest.fn().mockResolvedValue([FILE_LOCAL_SEARCH_ITEM]);
    provider.getSelectedContextItems = jest.fn().mockResolvedValue([FILE_LOCAL_SEARCH_ITEM]);

    return provider;
  }

  beforeEach(() => {
    logger = new TestLogger();
    configService = new DefaultConfigService();
    mockContextTransformerService = createFakePartial<AiContextTransformerService>({
      transform: jest.fn().mockImplementation((item) => item),
    });

    mockProjectAccessChecker = createFakePartial<DuoProjectAccessChecker>({});

    mockGitLabApiService = createFakePartial<GitLabApiService>({
      fetchFromApi: jest.fn(),
      onApiReconfigured: jest.fn(),
      instanceInfo: createFakePartial<InstanceInfo>({ instanceVersion: '17.6.0' }),
    });
    duoFeatureAccessService = new DefaultDuoFeatureAccessService(mockGitLabApiService);

    mockOpenTabProvider = createOpenTabProviderMock();
    mockImportProvider = createImportProviderMock();
    mockLocalFilesProvider = createLocalFilesProviderMock();

    manager = new DefaultAIContextManager(logger, configService, mockContextTransformerService, [
      mockOpenTabProvider,
      mockImportProvider,
      mockLocalFilesProvider,
    ] as unknown as AIContextProvider[]);
  });

  describe('searchContextItems for code_suggestions', () => {
    it('calls searchContextItems() on all enabled Code Suggestions providers', async () => {
      jest.mocked(mockGitLabApiService.fetchFromApi).mockResolvedValue({
        currentUser: {
          codeSuggestionsContexts: [
            DuoCodeSuggestionsContext.OpenTabs,
            DuoCodeSuggestionsContext.Imports,
          ] as DuoCodeSuggestionsContext[],
        },
      });

      const result = await manager.searchContextItems({
        featureType: 'code_suggestions',
      } as AIContextSearchRequest);

      expect(result).toEqual([OPEN_TAB_FILE, ANOTHER_OPEN_TAB_FILE, IMPORT_FILE]);
      expect(mockOpenTabProvider.searchContextItems).toHaveBeenCalledTimes(1);
      expect(mockImportProvider.searchContextItems).toHaveBeenCalledTimes(1);

      expect(mockLocalFilesProvider.searchContextItems).not.toHaveBeenCalled();
    });
  });
});
