import { createInterfaceId } from '@gitlab/needle';
import {
  AIContextItem,
  AIContextCategory,
  AIContextProviderType,
  AIContextPolicyResponse,
  AIContextSearchRequest,
  AIContextRetrieveRequest,
} from '@gitlab-org/ai-context';

export interface AIContextProvider<T extends AIContextItem = AIContextItem> {
  type: AIContextProviderType;
  addSelectedContextItem: (contextItem: T) => Promise<void>;
  removeSelectedContextItem: (id: string) => Promise<void>;
  clearSelectedContextItems: () => Promise<void>;
  replaceSelectedContextItem: (oldItem: T, newItem: T) => Promise<void>;
  getSelectedContextItems: () => Promise<T[]>;
  searchContextItems: (query: AIContextSearchRequest) => Promise<T[]>;
  retrieveContextItemsWithContent: (request?: AIContextRetrieveRequest<T>) => Promise<T[]>;
  getItemWithContent: (item: T) => Promise<T>;
  isAvailable: (aiRequest?: AIContextSearchRequest) => Promise<AIContextPolicyResponse>;
}

// Generic AIContextProvider interface ID cannot handle contravariant parameters and covariant returns properly in DI registration
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const AIContextProvider = createInterfaceId<AIContextProvider<any>>('AIContextProvider');

export interface AIContextPolicyProvider {
  isContextItemAllowed: (relativePath: string) => Promise<AIContextPolicyResponse>;
}

export const AIContextEndpoints = {
  QUERY: '$/gitlab/ai-context/query',
  ADD: '$/gitlab/ai-context/add',
  REMOVE: '$/gitlab/ai-context/remove',
  CURRENT_ITEMS: '$/gitlab/ai-context/current-items',
  RETRIEVE: '$/gitlab/ai-context/retrieve',
  GET_PROVIDER_CATEGORIES: '$/gitlab/ai-context/get-provider-categories',
  CLEAR: '$/gitlab/ai-context/clear',
  GET_ITEM_CONTENT: '$/gitlab/ai-context/get-item-content',
} as const;

export type AIContextEndpointTypes = {
  [AIContextEndpoints.QUERY]: {
    request: AIContextSearchRequest;
    response: AIContextItem[];
  };
  [AIContextEndpoints.ADD]: {
    request: AIContextItem;
    response: boolean;
  };
  [AIContextEndpoints.REMOVE]: {
    request: AIContextItem;
    response: boolean;
  };
  [AIContextEndpoints.CURRENT_ITEMS]: {
    request: undefined;
    response: AIContextItem[];
  };
  [AIContextEndpoints.RETRIEVE]: {
    request: undefined;
    response: AIContextItem[];
  };
  [AIContextEndpoints.GET_PROVIDER_CATEGORIES]: {
    request: undefined;
    response: AIContextCategory[];
  };
  [AIContextEndpoints.CLEAR]: {
    request: undefined;
    response: boolean;
  };
  [AIContextEndpoints.GET_ITEM_CONTENT]: {
    request: AIContextItem;
    response: AIContextItem;
  };
};
export type GitDiffRequest = {
  /**
   * The URI of the repository to get the diff for
   */
  repositoryUri: string;
  /**
   * The branch to get the diff for
   * This will compare the current changes to the branch
   */
  branch: string;
};

export const AiContextEditorRequests = {
  GIT_DIFF: '$/gitlab/ai-context/git-diff',
  GIT_COMMIT_CONTENTS: '$/gitlab/ai-context/git-commit-contents',
} as const;
export type EditorRequestEndpointTypes = {
  [AiContextEditorRequests.GIT_DIFF]: {
    request: GitDiffRequest;
    response: string;
  };
};
