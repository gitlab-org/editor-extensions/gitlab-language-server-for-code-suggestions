import { createInterfaceId } from '@gitlab/needle';
import type { AIContextItem } from '@gitlab-org/ai-context';

export interface AiContextTransformer {
  transform(context: AIContextItem): Promise<AIContextItem>;
}

export const AiContextTransformer = createInterfaceId<AiContextTransformer>('AiContextTransformer');
