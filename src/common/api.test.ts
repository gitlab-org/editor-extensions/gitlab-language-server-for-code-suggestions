import fetch from 'cross-fetch';
import { ApiReconfiguredData, ApiRequest, GetRequest, PostRequest } from '@gitlab-org/core';
import {
  GitLabAPI,
  CodeSuggestionResponse,
  InvalidTokenCheckResponse,
  ValidTokenCheckResponse,
  PersonalAccessToken,
  OAuthTokenInfoResponse,
} from './api';
import { CODE_SUGGESTIONS_RESPONSE, FILE_INFO } from './test_utils/mocks';
import { FetchBase, LsFetch } from './fetch';
import { createFakeResponse } from './test_utils/create_fake_response';
import { ConfigService, DefaultConfigService } from './config_service';
import { createFakePartial } from './test_utils/create_fake_partial';
import { getLanguageServerVersion } from './utils/get_language_server_version';
import {
  RequestResponse,
  success,
  error,
  TestSimpleApiClient,
} from './test_utils/test_simple_api_client';
import { versionRequest } from './api_types';
import { FetchError } from './fetch_error';
import { connectToCable } from './action_cable';

jest.mock('./action_cable', () => ({
  connectToCable: jest.fn(),
}));

jest.mock('cross-fetch');
jest.mock('graphql-request');
jest.mock('./utils/get_language_server_version');

const GITLAB_LANGUAGE_SERVER_VERSION = 'v0.0.0';

const GITLAB_INSTANCE_VERSION = '17.0.0';

const TEST_CODE_SUGGESTION_REQUEST = {
  prompt_version: 1,
  project_path: '',
  project_id: -1,
  current_file: {
    content_above_cursor: FILE_INFO.prefix,
    content_below_cursor: FILE_INFO.suffix,
    file_name: FILE_INFO.fileRelativePath,
  },
};

const patRequest: GetRequest<PersonalAccessToken> = {
  type: 'rest',
  method: 'GET',
  path: '/api/v4/personal_access_tokens/self',
};

jest.useFakeTimers();

describe('GitLabAPI', () => {
  let lsFetch: LsFetch;
  let getSimpleClientSpy: jest.Spied<typeof GitLabAPI.prototype.getSimpleClient>;
  let simpleApiClient: TestSimpleApiClient;
  const token = 'glpat-1234';
  const gitlabBaseUrl = 'https://gitlab.com';
  const clientInfo = { name: 'MyClient', version: '1.0.0' };
  let configService: ConfigService;
  let api: GitLabAPI;

  const mockCancellationToken = {
    isCancellationRequested: false,
    onCancellationRequested: jest.fn(),
  };

  // adds configuration to the ApiClient
  const configureApi = (baseUrl = gitlabBaseUrl, localToken = token) => {
    configService.merge({
      client: {
        clientInfo,
        baseUrl,
        token: localToken,
      },
    });
    jest.runAllTicks();
  };

  const versionRequestResponse: RequestResponse = [
    versionRequest,
    success({ version: GITLAB_INSTANCE_VERSION }),
  ];

  const validTokenRequestResponse: RequestResponse = [patRequest, success({ scopes: ['api'] })];

  // adds configuration and also mocks the token check
  const prepareApi = ({ baseUrl = gitlabBaseUrl, localToken = token } = {}) => {
    simpleApiClient.responses = [versionRequestResponse, validTokenRequestResponse];
    getSimpleClientSpy.mockReturnValue(simpleApiClient);
    configureApi(baseUrl, localToken);
  };

  beforeEach(async () => {
    jest.resetAllMocks();
    jest.mocked(getLanguageServerVersion).mockReturnValue(GITLAB_LANGUAGE_SERVER_VERSION);
    lsFetch = new FetchBase();
    simpleApiClient = new TestSimpleApiClient();
    configService = new DefaultConfigService();
    api = new GitLabAPI(lsFetch, configService);
    getSimpleClientSpy = jest.spyOn(api, 'getSimpleClient');
  });

  describe('getCodeSuggestions', () => {
    describe('Error path', () => {
      it('should throw an error when no token provided', async () => {
        await expect(api.getCodeSuggestions(TEST_CODE_SUGGESTION_REQUEST)).rejects.toThrow(
          /Token needs to be provided to request Code Suggestions/,
        );
      });
    });

    describe('Success path', () => {
      let response: CodeSuggestionResponse | undefined;

      beforeEach(async () => {
        prepareApi();
      });

      it('should return code suggestions', async () => {
        const request: PostRequest<CodeSuggestionResponse> = {
          type: 'rest',
          method: 'POST',
          path: '/api/v4/code_suggestions/completions',
          body: TEST_CODE_SUGGESTION_REQUEST,
        };
        simpleApiClient.responses = [
          versionRequestResponse,
          [request, { success: CODE_SUGGESTIONS_RESPONSE }],
        ];

        response = await api.getCodeSuggestions(TEST_CODE_SUGGESTION_REQUEST);

        expect(response).toEqual({ ...CODE_SUGGESTIONS_RESPONSE, status: 200 });
      });
    });
  });

  describe('checkToken', () => {
    beforeEach(() => {
      simpleApiClient.responses = [versionRequestResponse];
      getSimpleClientSpy.mockReturnValue(simpleApiClient);
      configureApi();
    });

    const oauthRequest: GetRequest<OAuthTokenInfoResponse> = {
      type: 'rest',
      method: 'GET',
      path: '/oauth/token/info',
    };

    it('should use correct URL and token for validation', async () => {
      await api.checkToken(gitlabBaseUrl, token);

      expect(getSimpleClientSpy).toHaveBeenCalledWith(gitlabBaseUrl, token);
    });

    it.each`
      tokenType                  | requestResponses                                 | tokenType
      ${'Personal Access Token'} | ${[[patRequest, success({ scopes: ['api'] })]]}  | ${'pat'}
      ${'OAuth Access Token'}    | ${[[oauthRequest, success({ scope: ['api'] })]]} | ${'oauth'}
    `('should return valid response for $tokenType', async ({ requestResponses, tokenType }) => {
      simpleApiClient.responses = requestResponses;

      const testResult = await api.checkToken(gitlabBaseUrl, token);

      expect(testResult).toEqual({
        valid: true,
        tokenInfo: { type: tokenType, scopes: ['api'] },
      } satisfies ValidTokenCheckResponse);
    });

    it.each`
      tokenType                  | requestResponses
      ${'Personal Access Token'} | ${[[patRequest, success({ scopes: ['read-api'] })]]}
      ${'OAuth Access Token'}    | ${[[oauthRequest, success({ scope: ['read-api'] })]]}
    `('handles insufficient token scope for $tokenType', async ({ requestResponses }) => {
      simpleApiClient.responses = requestResponses;

      const testResult = await api.checkToken(gitlabBaseUrl, token);

      expect(testResult).toEqual({
        valid: false,
        reason: 'invalid_scopes',
        message: "Token has scope(s) 'read-api' (needs api).",
      } satisfies InvalidTokenCheckResponse);
    });

    it.each`
      tokenType                  | request
      ${'Personal Access Token'} | ${patRequest}
      ${'OAuth Access Token'}    | ${oauthRequest}
    `('handles invalid token for $tokenType', async ({ request }) => {
      const response = error(
        new FetchError(createFakeResponse({ status: 401 }), '{"error": "invalid_token"}'),
      );
      simpleApiClient.responses = [[request, response]];

      const testResult = await api.checkToken(gitlabBaseUrl, token);

      expect(testResult).toEqual({
        valid: false,
        reason: 'invalid_token',
        message: 'Token is invalid or expired.',
      } satisfies InvalidTokenCheckResponse);
    });
  });

  describe('getStreamingCodeSuggestions', () => {
    const originalFetch = global.fetch;
    let streamingAPI: GitLabAPI;
    let streamingLsFetch: LsFetch;

    beforeEach(() => {
      global.fetch = jest.fn();
      streamingLsFetch = new FetchBase();
      streamingAPI = new GitLabAPI(streamingLsFetch, configService);
    });

    afterAll(() => {
      global.fetch = originalFetch;
    });

    it('throws an error when token is not provided', async () => {
      const generator = streamingAPI.getStreamingCodeSuggestions(
        TEST_CODE_SUGGESTION_REQUEST,
        mockCancellationToken,
      );

      await expect(() => generator?.next()).rejects.toThrow(
        'Token needs to be provided to stream code suggestions',
      );
    });

    it('returns a generator', async () => {
      jest
        .spyOn(streamingLsFetch, 'fetch')
        .mockResolvedValue(createFakeResponse({ status: 200, text: 'hello' }));

      prepareApi();
      const generator = streamingAPI.getStreamingCodeSuggestions(
        TEST_CODE_SUGGESTION_REQUEST,
        mockCancellationToken,
      );
      await expect(() => generator?.next()).rejects.toThrow('Not implemented');
    });
    it('passes the streaming header to the server', async () => {
      const spy = jest
        .spyOn(streamingLsFetch, 'fetch')
        .mockResolvedValue(createFakeResponse({ status: 200, text: 'hello' }));

      prepareApi();
      const generator = streamingAPI.getStreamingCodeSuggestions(
        TEST_CODE_SUGGESTION_REQUEST,
        mockCancellationToken,
      );
      await expect(() => generator?.next()).rejects.toThrow('Not implemented');
      expect(spy).toHaveBeenCalledWith(
        expect.any(String),
        expect.objectContaining({
          headers: expect.objectContaining({
            'X-Supports-Sse-Streaming': 'true',
          }),
        }),
      );
    });
  });

  describe('fetchFromApi', () => {
    describe('when no token provided', () => {
      it('should not make a request', async () => {
        await expect(
          api.fetchFromApi({ type: 'rest', method: 'GET', path: '/test' }),
        ).rejects.toThrow('Token needs to be provided to authorise API request.');
        expect(fetch).not.toHaveBeenCalled();
      });
    });

    describe('when instance version is not supported', () => {
      beforeEach(() => {
        prepareApi();
      });

      it('should not make a request', async () => {
        const requestWithApiEndpointVersionSupport = createFakePartial<ApiRequest<Promise<string>>>(
          {
            type: 'rest',
            method: 'GET',
            path: '/api/v4/test',
            supportedSinceInstanceVersion: {
              version: '17.2.0',
              resourceName: 'do something',
            },
          },
        );
        await expect(api.fetchFromApi(requestWithApiEndpointVersionSupport)).rejects.toThrow(
          `Can't do something until your instance is upgraded to 17.2.0 or higher.`,
        );
        expect(fetch).not.toHaveBeenCalled();
      });
    });

    describe('when token is provided and instance supported', () => {
      const TEST_RESPONSE_JSON = [{ id: 1 }, { id: 2 }];

      beforeEach(() => {
        prepareApi();
      });

      it('forwards the request to simple API client', async () => {
        expect(fetch).not.toHaveBeenCalled();

        const mockGetRequest: ApiRequest<unknown> = {
          type: 'rest',
          method: 'GET',
          path: '/api/v4/test',
          searchParams: {
            param: '123',
            foo: 'bar',
          },
          headers: {
            'X-Test': '123',
          },
          signal: new AbortController().signal,
        };

        simpleApiClient.responses = [[mockGetRequest, { success: TEST_RESPONSE_JSON }]];

        const response = await api.fetchFromApi<unknown>(mockGetRequest);

        expect(response).toEqual(TEST_RESPONSE_JSON);
      });

      it('with failed request rejects', async () => {
        const response = api.fetchFromApi(
          createFakePartial<ApiRequest<unknown>>({
            type: 'rest',
            method: 'GET',
            path: '/api/v4/test',
          }),
        );

        await expect(response).rejects.toThrow(/Fetching Request .* failed/);
      });
    });
  });

  describe('onConfigChange', () => {
    let waitForLastReconfigureEvent: Promise<ApiReconfiguredData>;
    const validTokenCheckResponse = createFakeResponse({
      json: { active: true, scopes: ['api'] },
    });
    const invalidTokenCheckResponse = createFakeResponse({
      status: 401,
    });
    beforeEach(() => {
      configService = new DefaultConfigService();
      if (!configService.onConfigChange) {
        throw new Error('issue');
      }
      api = new GitLabAPI(lsFetch, configService);
      waitForLastReconfigureEvent = new Promise((resolve) => {
        api.onApiReconfigured((data) => {
          resolve(data);
        });
      });
      lsFetch.updateAgentOptions = jest.fn();
      lsFetch.get = jest.fn();
      jest.useFakeTimers();
    });
    afterEach(() => {
      jest.mocked(lsFetch.updateAgentOptions).mockReset();
    });

    it('initializes http proxy before doing a token check', async () => {
      configService.merge({ client: { token: 'hello' } });
      await waitForLastReconfigureEvent;

      const updateAgentOptionsOrder = jest.mocked(lsFetch.updateAgentOptions).mock
        .invocationCallOrder[0];
      const checkTokenOrder = jest.mocked(lsFetch.get).mock.invocationCallOrder[0];
      expect(updateAgentOptionsOrder).toBeLessThan(checkTokenOrder);
    });

    it('when updated with valid token and baseUrl, it sends a valid config event', async () => {
      jest.mocked(lsFetch.get).mockResolvedValue(validTokenCheckResponse);

      configService.merge({ client: { token: 'hello' } });
      jest.runAllTicks();

      expect(await waitForLastReconfigureEvent).toEqual({
        isInValidState: true,
        instanceInfo: { instanceUrl: 'https://gitlab.com' },
        tokenInfo: {
          type: 'pat',
          scopes: ['api'],
        },
      });

      expect(api.tokenInfo).toEqual({ type: 'pat', scopes: ['api'] });
    });

    it('when updated with invalid token, it sends a non-valid config event', async () => {
      jest.mocked(lsFetch.get).mockResolvedValue(invalidTokenCheckResponse);

      configService.merge({ client: { token: 'hello' } });
      jest.runAllTicks();

      expect(await waitForLastReconfigureEvent).toEqual(
        expect.objectContaining({
          isInValidState: false,
          validationMessage: expect.stringMatching(/Token is invalid/),
        }),
      );
    });
  });

  describe('Instance Information', () => {
    beforeEach(() => {
      prepareApi();
    });

    it('is set when api is configured ', () => {
      expect(api.instanceInfo).toEqual({
        instanceUrl: 'https://gitlab.com',
        instanceVersion: GITLAB_INSTANCE_VERSION,
      });
    });
  });

  describe('connectToCable', () => {
    beforeEach(() => {
      api = new GitLabAPI(lsFetch, configService);
      jest
        .mocked(connectToCable)
        .mockReset()
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .mockResolvedValue({} as any);
    });

    it('throws an error when token is not set', async () => {
      await expect(api.connectToCable()).rejects.toThrow(
        'Token is not set up. Cannot connect to cable without a token.',
      );
    });

    it.each([
      ['https://gitlab.com', 'https://gitlab.com'],
      ['https://gitlab.com/api/v4', 'https://gitlab.com'],
      ['http://gitlab.example.com:8080/foo/bar', 'http://gitlab.example.com:8080'],
      ['https://sub.domain.gitlab.com/path?query=123', 'https://sub.domain.gitlab.com'],
    ])('sets correct Origin header when baseURL is %s', async (baseUrl, expectedOrigin) => {
      configService.merge({ client: { baseUrl, token: 'test-token' } });
      jest.runAllTicks();

      await api.connectToCable();

      expect(connectToCable).toHaveBeenCalledWith(
        expect.any(String),
        expect.objectContaining({
          headers: expect.objectContaining({
            Origin: expectedOrigin,
          }),
        }),
      );
    });
  });
});
