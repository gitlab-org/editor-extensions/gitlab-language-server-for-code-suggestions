import { createViteConfigForWebview } from '@gitlab-org/vite-common-config';
import { VULN_DETAILS_WEBVIEW_ID } from './metadata';

// eslint-disable-next-line import/no-default-export
export default createViteConfigForWebview(VULN_DETAILS_WEBVIEW_ID);
